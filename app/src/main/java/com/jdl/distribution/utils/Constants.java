package com.jdl.distribution.utils;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.jdl.distribution.R;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class Constants {

    //  DESARROLLO
    public static final String URL_BASE = "http://192.168.0.7:8080/WSProyectoJDL/";//CASA
//    public static final String URL_BASE = "http://192.168.0.12:8080/WSProyectoJDL/";//LUIS
//    public static final String URL_BASE = "http://192.168.137.150:8080/WSProyectoJDL/";
//    public static final String URL_BASE = "http://localhost:8080/WSProyectoJDL/";
//    public static final String URL_BASE = "http://sprysales.hostper.com/cnx/";

    public static final String URL_BASE_MIGO = "https://api.migoperu.pe/api/v1/";

    public static final boolean DEBUG = true;

    //WS
    public static final String WS_LOGIN = "FuerzaVenta/Login";
    public static final String WS_PROGRAMADOS = "Cliente/listar-programados";
    public static final String WS_CLIENTES = "Cliente/listar-clientes";
    public static final String WS_INICIAR_VISITA = "Visita/iniciar-visita";
    public static final String WS_RECUPERAR_VISITA = "Visita/recuperar-visita";
    public static final String WS_PRODUCTOS_FRECUENTES = "Producto/lista-programada";
    public static final String WS_CERRAR_VISITA = "Visita/cerrar-visita";
    public static final String WS_REGISTRAR_PEDIDO = "Pedido/insertar";
    public static final String WS_EMITIR_COMPROBANTE = "Pedido/detalle";
    public static final String WS_SELECCIONA_CLIENTE = "Cliente/selecciona-cliente";
    public static final String WS_LISTAR_COMBO = "Cliente/listar-combo";
    public static final String WS_ACTUALIZAR_CLIENTE = "Cliente/actualizar-cliente";
    public static final String WS_AGREGAR_CLIENTE = "Cliente/insertar-cliente";
    public static final String WS_AGREGAR_INCIDENCIA = "Incidencia/insertar";
    public static final String WS_LISTAR_INCIDENCIA = "Incidencia/listar";
    public static final String WS_BUSCAR_PRODUCTO_NOMBRE = "Producto/buscar-producto-nombre";
    public static final String WS_BUSCAR_PRODUCTO_SCANNER = "Producto/buscar-producto-codbar";
    public static final String WS_PEDIDO_HISTORIAL = " Pedido/listar";
    public static final String WS_DESPACHO_HISTORIAL = " Entrega/listar";
    public static final String WS_CONSULTAR_DOCUMENTO = "Documento/buscar-documento";//no se usa
    public static final String WS_PRODUCTOS_LISTAR = "Producto/listar-todos";
    public static final String WS_ELIMINAR_INCIDENCIA = "Incidencia/eliminar";
    public static final String WS_ENTREGA_VALIDAR = "Entrega/validar";
    public static final String WS_ENTREGA_TOTAL = "Entrega/listar-venta";
    public static final String WS_FINALIZAR_ENTREGA = "Entrega/realizar-entrega";
    //FALTA WS
    public static final String WS_AGREGAR_IMAGEN_CLIENTE = "Cliente/imagen";//no se usa



    public static final int REQUEST_CODE_PERMISION_LOCATION = 100;
    public static final int REQUEST_CODE_PERMISION_CAMERA = 200;

    public static final String[] permissions = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA
    };
    public static final String[] permissionsCamera = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    //PROFILES
    public static final String PROFILE_ADMINISTRADOR = "1";
    public static final String PROFILE_PRE_VENTA = "2";
    public static final String PROFILE_REPARTO = "3";
    public static final String PROFILE_AUTO_VENTA = "4";
    public static final String PROFILE_SUPERVISOR = "5";

    public static final int VISIT_COMPLETED = 2;
    public static final int VISIT_DEFAULT = 0;
    public static final int VISIT_SIN_VENTA = 1;
    public static final int VISIT_COMPLETA_SIN_VENTA = 0;


    public static final String DISCOUNT_PORCENTAJE = "1";
    public static final String DISCOUNT_NETO = "2";


    public static final int TIPO_IMPRESION_PEDIDO = 1;
    public static final String OP_NOTIFY_LOCALES_CERRAR_VISITA = "PROGRAMADOS_CLOSE";
    public static final String OP_NOTIFY_CLIENT_UPDATE_DATA = "CLIENTE_UPDATE";
    public static final String OP_NOTIFY_LIST_CLIENTE = "CLIENTE_UPDATE_LIST";
    public static final String OP_NOTIFY_SCANNER = "OP_NOTIFY_SCANNER";

    public static final String FRECUENCIA_1 = "FRE-000001";
    public static final String FRECUENCIA_2 = "FRE-000002";
    public static final String FRECUENCIA_3 = "FRE-000003";
    public static final String FRECUENCIA_4 = "FRE-000004";
    public static final String FRECUENCIA_5 = "FRE-000005";
    public static final String FRECUENCIA_6 = "FRE-000006";
    public static final String FRECUENCIA_7 = "FRE-000007";

    public static final String BOLETA = "1";
    public static final String FACTURA = "2";
    public static final String DNI = "01";
    public static final String RUC = "02";
    public static final String TOKEN = "1d4a0b57-3d80-4aa2-853e-6ab9f0d3a32e-4e64f1e7-11c9-49cf-acb2-aab649b634d4";
    public static final String WS_CONSULTAR_DOCUMENTO_DNI = "dni";
    public static final String WS_CONSULTAR_DOCUMENTO_RUC = "ruc";


    public static int TIME_MAX_SEARCH = 1000;

    // CLICK BUTTONS VISITA
    public static final String PEDIDO = "1";
    public static final String VENTA = "2";

    public static final int REQUEST_CODE_SCAN = 101;

    public static boolean isInternetConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    public static boolean verifyInternetAndMqtt(Context context) {
        if (!Constants.isInternetConnected(context)) {
            Toast.makeText(context, context.getResources().getString(R.string.sin_internet), Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return false;
        }
    }

    public static double toDecimal(double descuento, int i) {

        return Math.round(descuento * Math.pow(10, i)) / Math.pow(10, i);

    }

    public static BitmapDescriptor bitmapImagenVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static void ocultarTeclado(Context context, View view) {

        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
            view.clearFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void ActionDown(View view) {
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(view, "scaleX", 0.95f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(view, "scaleY", 0.95f);
        scaleDownX.setDuration(300);
        scaleDownY.setDuration(300);

        AnimatorSet scaleDown = new AnimatorSet();
        scaleDown.play(scaleDownX).with(scaleDownY);

        scaleDown.start();
    }

    public static void ActionUp(View view) {
        ObjectAnimator scaleDownX2 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        ObjectAnimator scaleDownY2 = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        scaleDownX2.setDuration(300);
        scaleDownY2.setDuration(300);

        AnimatorSet scaleDown2 = new AnimatorSet();
        scaleDown2.play(scaleDownX2).with(scaleDownY2);

        scaleDown2.start();
    }

    public static void showSoftKeyboard(final Context context, final EditText editText) {
        try {
            editText.requestFocus();
            editText.postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            keyboard.showSoftInput(editText, 0);
                        }
                    }
                    , 200);
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
