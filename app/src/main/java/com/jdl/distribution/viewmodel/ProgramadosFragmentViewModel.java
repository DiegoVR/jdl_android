package com.jdl.distribution.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;


import com.jdl.distribution.mvp.model.Cliente;

import java.util.ArrayList;
import java.util.List;

public class ProgramadosFragmentViewModel extends BaseAndroidViewModel {

    private MediatorLiveData<List<Cliente>> observableProgramados;
    private ArrayList<Cliente> programadosList;

    public ProgramadosFragmentViewModel(@NonNull Application application) {
        super(application);
        observableProgramados  = new MediatorLiveData<>();
        programadosList = new ArrayList<>();
        observableProgramados.addSource(getRepository().getObservableProgramados(), observableProgramados::setValue);
    }

    public ArrayList<Cliente> getProgramados() {
        return programadosList;
    }

    public void setProgramados(@Nullable List<Cliente> programados) {
        programadosList.clear();
        programadosList.addAll(programados);
    }

    public void setObservableCliente(List<Cliente> programados) {
        getRepository().setObservableProgramados(programados);
    }

    public LiveData<List<Cliente>> getObservableProgramados() {
        return observableProgramados;
    }

}
