package com.jdl.distribution.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.jdl.distribution.data.db.repository.DataRepository;
import com.jdl.distribution.utils.application.MyApplication;

public class BaseAndroidViewModel extends AndroidViewModel {

    protected DataRepository repository;

    public BaseAndroidViewModel(@NonNull Application application) {
        super(application);
        repository = ((MyApplication)application).getRepository();
    }

    public DataRepository getRepository(){
        return repository;
    }


}
