package com.jdl.distribution.data.db.repository;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.jdl.distribution.data.db.AppDatabase;
import com.jdl.distribution.mvp.model.Cliente;
import java.util.Arrays;
import java.util.List;

public class DataRepository {

    private static final String LOG_TAG = DataRepository.class.getSimpleName();
    private MediatorLiveData<List<Cliente>> observableProgramados;



    private final AppDatabase db;
    private static DataRepository instance;

    public static DataRepository getInstance(AppDatabase db) {

        if (instance == null) {
            synchronized (DataRepository.class) {
                if (instance == null) {
                    instance = new DataRepository(db);
                }
            }
        }
        return instance;
    }

    private DataRepository(AppDatabase db) {
        this.db = db;
        observableProgramados = new MediatorLiveData<>();


//        observableProgramados.addSource(getDb().localDao().getAll(), locals -> {
//            if (getDb().getDatabaseCreated() != null) {
//                observableProgramados.postValue(locals);
//            }
//        });


    }

    public AppDatabase getDb() {
        return db;
    }


    //get

    public LiveData<List<Cliente>> getObservableProgramados() {
        return observableProgramados;
    }



    //set

    public void setObservableProgramados(@Nullable List<Cliente> programados) {

        new Thread(() -> {
            try {
//                getDb().clienteDao().deleteAllAndInsertAll(programados);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }).start();

    }
//
//    public void setObservableLoginMonedas(@Nullable List<Moneda> monedas) {
//
//        new Thread(() -> {
//            try {
////                getDb().monedasDao().deleteAndCreate(monedas);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }).start();
//
//    }

//    public void setObservableMonedas(@Nullable InitVisitResponse.Moneda moneda) {
//
//        if (moneda == null) {
//            new Thread(() -> {
//                try {
//                    getDb().initVisitDao().deleteAll();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }).start();
//            return;
//        }
//
//        List<InitVisitResponse.Moneda> visits = Arrays.asList(moneda);
//
//        try {
//
//            Completable.fromAction(() -> getDb().monedaDao().deleteAndCreate(visits))
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.single())
//                    .subscribe();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//    }


    //update


    //delete


    //logout

    public void logout() {

        new Thread(() -> {
//            getDb().localDao().deleteAll();

        }).start();

    }

}
