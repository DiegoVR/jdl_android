package com.jdl.distribution.ui.activity.bluetooth.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.jdl.distribution.R;
import com.jdl.distribution.ui.activity.bluetooth.model.BluetoothModel;

import java.util.ArrayList;

public class BluetoothAdapterAdd extends RecyclerView.Adapter<BluetoothAdapterAdd.ViewHolder> {

    private final Context context;
    private ArrayList<BluetoothModel> listadoBluetooth;
    AsynStatusListenerAdd asynStatusListener;

    public BluetoothAdapterAdd(Context context, ArrayList<BluetoothModel> listadoBluetooth, AsynStatusListenerAdd asynStatusListener){
        this.context = context;
        this.listadoBluetooth = listadoBluetooth;
        this.asynStatusListener = asynStatusListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_printer,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BluetoothModel model = listadoBluetooth.get(position);

        holder.txt_mac.setText(model.getIp6());
        holder.txt_nombre_impresora.setText(model.getNombre());

        holder.cv_impresora.setOnClickListener(i ->{
            asynStatusListener.onClickItem(model);
        });

    }

    @Override
    public int getItemCount() {
        return listadoBluetooth.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cv_impresora;
        TextView txt_nombre_impresora;
        TextView txt_mac;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_mac = itemView.findViewById(R.id.txt_mac);
            txt_nombre_impresora = itemView.findViewById(R.id.txt_nombre_impresora);
            cv_impresora = itemView.findViewById(R.id.cv_impresora);
        }
    }

    public interface AsynStatusListenerAdd{

        void onClickItem(BluetoothModel model);

    }

}
