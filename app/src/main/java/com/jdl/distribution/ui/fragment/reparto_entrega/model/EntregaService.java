package com.jdl.distribution.ui.fragment.reparto_entrega.model;

import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface EntregaService {
    @POST(Constants.WS_FINALIZAR_ENTREGA)
    Call<EntregaResponse> doEntrega(@Body String data);
}
