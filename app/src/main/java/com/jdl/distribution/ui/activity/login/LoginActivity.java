package com.jdl.distribution.ui.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.developer.appsupport.annotation.onetime.OneTime;
import com.developer.appsupport.annotation.onetime.OneTimer;
import com.developer.appsupport.ui.activity.BaseActivity;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.utils.JSONGenerator;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.activity.base.BaseMvpActivity;
import com.jdl.distribution.ui.activity.login.model.LoginResponse;
import com.jdl.distribution.ui.activity.login.presenter.LoginPresenter;
import com.jdl.distribution.ui.activity.login.view.LoginView;
import com.jdl.distribution.ui.activity.main.MainActivity;
import com.jdl.distribution.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseMvpActivity<LoginView, LoginPresenter> implements View.OnClickListener,LoginView {

    @OneTime
    @BindView(R.id.btn_acceder_login)
    ProgressButton btn_acceder_login;
    @BindView(R.id.icv_toogle_pass)
    ImageCardView icv_toogle_pass;
    @BindView(R.id.edt_password)
    EditText edt_password;
    @BindView(R.id.edt_usuario)
    EditText edt_usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this,this);
        init();
    }

    @NonNull
    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenter(this);
    }

    private void init(){
        checkPermisions();
        setListener();
    }

    private void setListener() {

        btn_acceder_login.setOnClickListener(this);
        btn_acceder_login.setOneTimerSupport(true);
        icv_toogle_pass.setOnClickListener(this);

        edt_password.setOnEditorActionListener((textView, i, keyEvent) -> {

            if (i == EditorInfo.IME_ACTION_DONE) {
                onClick(btn_acceder_login);
                return true;
            }

            return false;

        });

    }

    private void checkPermisions(){

        ActivityCompat.requestPermissions(this, Constants.permissions, Constants.REQUEST_CODE_PERMISION_LOCATION);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_acceder_login:

                JSONGenerator.getNewInstance(this)
                        .requireInternet(true)
                        .put("USU", edt_usuario, JSONGenerator.ValidateOptions.getDefault())
                        .put("PWD", edt_password, JSONGenerator.ValidateOptions.getDefault())
                        .operate(jsonObject -> {
                            OneTimer.subscribe(this,btn_acceder_login);
                            getPresenter().doLogin(jsonObject);
                        });

                break;
            case R.id.icv_toogle_pass:
                com.developer.appsupport.utils.Constants.tooglePassword(edt_password);
                break;
        }
    }

    @Override
    public void onLoginSuccess(LoginResponse loginResponse) {
        Toast.makeText(this, loginResponse.getMsg(), Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, MainActivity.class));
        finish();
        OneTimer.unsubscribe(this,btn_acceder_login);
    }

    @Override
    public void onLoginFail(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this,btn_acceder_login);
    }
}
