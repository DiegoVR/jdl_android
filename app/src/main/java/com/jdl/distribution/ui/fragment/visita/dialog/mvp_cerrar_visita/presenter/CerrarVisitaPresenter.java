package com.jdl.distribution.ui.fragment.visita.dialog.mvp_cerrar_visita.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.ui.fragment.visita.dialog.mvp_cerrar_visita.view.CerrarVisitaView;
import com.jdl.distribution.ui.fragment.visita.model.MenuVisitaService;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CerrarVisitaPresenter extends MvpBasePresenter<CerrarVisitaView> {

    private static final String LOG_TAG = CerrarVisitaPresenter.class.getSimpleName();
    private final Context context;

    public CerrarVisitaPresenter(Context context) {
        this.context = context;
    }

    public void doCloseVisitRequest(JSONObject jsonObject) {
        Log.d(LOG_TAG, "doCloseVisitRequest " + jsonObject.toString());
//        ifViewAttached(view -> view.showLoading(true));

        MenuVisitaService service = RetrofitClient.getRetrofitInstance().create(MenuVisitaService.class);
        Call<StatusMsgResponse> call = service.doCerrarVisita(jsonObject.toString());
        call.enqueue(new Callback<StatusMsgResponse>() {
            @Override
            public void onResponse(Call<StatusMsgResponse> call, Response<StatusMsgResponse> response) {

                StatusMsgResponse closeVisitResponse = response.body();

                if (closeVisitResponse == null) {
                    Log.d(LOG_TAG, "closeVisitResponse==null");
                    ifViewAttached(view -> {
                        view.onCloseVisitFail(context.getResources().getString(R.string.error));
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + closeVisitResponse.toString());

                switch (closeVisitResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.onCloseVisitSuccess(closeVisitResponse);
//                            view.showLoading(false);
                        });

                        break;

                    case FAIL:

                        ifViewAttached(view -> {
                            view.onCloseVisitFail(closeVisitResponse.getMsg());
//                            view.showLoading(false);
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<StatusMsgResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onCloseVisitFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onCloseVisitFail(context.getResources().getString(R.string.error));
                    }
//                    view.showLoading(false);
                });
            }
        });
    }

}
