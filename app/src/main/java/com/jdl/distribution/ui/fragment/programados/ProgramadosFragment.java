package com.jdl.distribution.ui.fragment.programados;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Destroyer;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.mvp.model.Message;
import com.developer.appsupport.utils.JSONGenerator;
import com.google.android.libraries.places.internal.c;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.AppPref;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.mvp.model.ClienteResponse;
import com.jdl.distribution.ui.fragment.base.BaseMvpFragment;
import com.jdl.distribution.ui.fragment.clientes.ClientesFragment;
import com.jdl.distribution.ui.fragment.programados.adapter.ProgramadosAdapter;
import com.jdl.distribution.ui.fragment.programados.model.InitVisitResponse;
import com.jdl.distribution.ui.fragment.programados.presenter.ProgramadosPresenter;
import com.jdl.distribution.ui.fragment.programados.view.ProgramadosView;
import com.jdl.distribution.ui.fragment.visita.VisitaFragment;
import com.jdl.distribution.ui.fragment.visita.VisitaFragmentBuilder;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.viewmodel.ProgramadosFragmentViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgramadosFragment extends BaseMvpFragment<ProgramadosView, ProgramadosPresenter> implements ProgramadosView, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    @Destroy(type = Type.SWIPE_REFRESH)
    @BindView(R.id.sw_programados)
    public SwipeRefreshLayout sw_programados;
    @BindView(R.id.rv_programados)
    RecyclerView rv_programados;
    @BindView(R.id.btn_no_programados)
    Button btn_no_programados;
    @BindView(R.id.edt_search_programados)
    EditText edt_search_programados;
    @BindView(R.id.txt_clientes_total)
    TextView txt_clientes_total;

    private ProgramadosFragmentViewModel viewModel;
    private ProgramadosAdapter adapter;

    private LoginPref loginPref;
    private AppPref appPref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_programados, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        if (getActivity() == null)
            return;

        viewModel = ViewModelProviders.of(this).get(ProgramadosFragmentViewModel.class);
        loginPref = new LoginPref(getContext());
        appPref = new AppPref(getContext());

        loadData("");
        setAdapter();
        setListener();
        search();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void setListener() {
        btn_no_programados.setOnClickListener(this);
        sw_programados.setOnRefreshListener(this);
    }

    private void search() {
        edt_search_programados.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filtrar(s.toString());
            }


        });
    }

    private void filtrar(String text) {

        ArrayList<Cliente> listFiltroCliente = new ArrayList<>();

        for (Cliente local : viewModel.getProgramados()) {
            if (local.getNombre().toLowerCase().contains(text.toLowerCase()) ||
                    local.getDireccion().toLowerCase().contains(text.toLowerCase()) ||
                    local.getNombre().toLowerCase().contains(text.toLowerCase()) ||
                    local.getContacto().toLowerCase().contains(text.toLowerCase())) {
                listFiltroCliente.add(local);
            }
        }

        adapter.filterList(listFiltroCliente);

    }

    private void setAdapter() {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        rv_programados.setLayoutManager(layoutManager);
        rv_programados.setItemAnimator(new DefaultItemAnimator());

        /*List<Cliente> lstCliente = new ArrayList<>();
        String nombre[] = {"Bodega Carlos","Bodega Juana"};
        String rs[] = {"Bodega Carlos S.A.C","Bodega Juana S.A.C"};
        String encargdo[] = {"Luis","Rodrigo"};
        String distancia[] = {"10km","5km"};
        String direccion[] = {"Av Gutierrez","Av Jose Galvez"};
        String color[] = {"0","0"};
        String hinicio[] = {"8","6"};
        String hfin[] = {"13","15"};
        for(int i = 0; i < nombre.length; i++){
            Cliente cliente = new Cliente();

            cliente.setNombre(nombre[i]);
            cliente.setContacto(encargdo[i]);
            cliente.setDistancia(distancia[i]);
            cliente.setDireccion(direccion[i]);
            cliente.setColorVisita(color[i]);
            cliente.setRazonSocial(rs[i]);
            cliente.setHoraInicio(hinicio[i]);
            cliente.setHoraFin(hfin[i]);

            lstCliente.add(cliente);
        }
        viewModel.setProgramados(lstCliente);*/

        adapter = new ProgramadosAdapter(getContext(), viewModel.getProgramados(), programados -> {

            JSONGenerator.getNewInstance(getActivity())
                    .requireInternet(true)
                    .put("id_cli", programados.getIdcli())
                    .put("id_perfil", new LoginPref(getContext()).getIdProfile())
                    .put("id_fv", loginPref.getUid())
                    .put("lat", appPref.getLatitude())
                    .put("lng", appPref.getLongitude())
                    .operate(jsonObject -> {
                        presenter.doInitVisit(jsonObject);
                    });

            /*Fragment signupFragment = new VisitaFragmentBuilder(programados, null, null).build();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack(null);
            transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
            transaction.add(R.id.main_content, signupFragment, VisitaFragment.class.getSimpleName());
            transaction.commitAllowingStateLoss();*/
        });

        rv_programados.setAdapter(adapter);

    }

    private void loadData(String search) {
        if (getActivity() == null)
            return;

        JSONGenerator.getNewInstance(getActivity())
                .requireInternet(true)
                .put("id_perfil", loginPref.getIdProfile())
                .put("id_usuario", loginPref.getUid())
                .put("lat", appPref.getLatitude())
                .put("lon", appPref.getLongitude())
                .operate(jsonObject -> getPresenter().doListProgramados(jsonObject));

    }

    @Override
    public ProgramadosPresenter createPresenter() {
        return new ProgramadosPresenter(getActivity());
    }

    @Override
    public void showLoadingDialog(boolean status) {
        if (status) {
            try {
                Destroyer.subscribe(this);
                sw_programados.setRefreshing(true);
            } catch (Exception e) {

            }
        } else {
            try {
                Destroyer.unsubscribe(this);
                sw_programados.setRefreshing(false);
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onProgramadosListSuccess(ClienteResponse response) {
        if (getContext() == null) return;

        if (!response.getIdvis().isEmpty()) {
            JSONGenerator.getNewInstance(getActivity())
                    .requireInternet(true)
                    .put("id_pf", loginPref.getIdProfile())
                    .put("id_fv", loginPref.getUid())
                    .put("id_vis", response.getIdvis())
                    .put("lat", appPref.getLatitude())
                    .put("lng", appPref.getLongitude())
                    .operate(jsonObject -> getPresenter().doRecuperarVisita(jsonObject));
            return;
        }

        txt_clientes_total.setText(String.valueOf(response.getListC().size()));
        viewModel.setProgramados(response.getListC());
        adapter.notifyDataSetChanged();
        Toast.makeText(getActivity(), response.getMsg(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProgramadosListFail(String mensaje, ClienteResponse response) {
        if (getContext() == null) return;
        Toast.makeText(getActivity(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onInitVisitSuccess(InitVisitResponse response) {
        if (getActivity() == null) return;
        VisitaFragment.OPEN = true;

        Fragment signupFragment = new VisitaFragmentBuilder(response.getCliente(), response.getMoneda(), response).build();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
        transaction.add(R.id.main_content, signupFragment, VisitaFragment.class.getSimpleName());
        transaction.commitAllowingStateLoss();

    }

    @Override
    public void onInitVisitFail(String message) {
        if (getActivity() == null) return;
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRecuperarVisitaSuccess(InitVisitResponse response) {
        if (getActivity() == null) return;

        Fragment signupFragment = new VisitaFragmentBuilder(response.getCliente(), response.getMoneda(), response).build();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
        transaction.add(R.id.main_content, signupFragment, VisitaFragment.class.getSimpleName());
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onRecuperarVisitaFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_no_programados:
                Fragment signupFragment = new ClientesFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack(ClientesFragment.class.getSimpleName());
                transaction.setCustomAnimations(R.anim.translate_in, R.anim.translate_out);
                transaction.replace(R.id.main_content, signupFragment, ClientesFragment.class.getSimpleName());
                transaction.commit();
                break;
        }
    }

    @Override
    public void onRefresh() {
        if (Constants.verifyInternetAndMqtt(getContext())) {
            sw_programados.setRefreshing(false);
            return;
        }
        loadData("");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnSubscribe(Message message) {
        if (message.getOpStr().equals(Constants.OP_NOTIFY_LOCALES_CERRAR_VISITA)) {
            loadData("");
        }
    }

}
