package com.jdl.distribution.ui.fragment.historial_pedidos.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import java.util.List;

public class PedidoHistorialResponse implements IStringNotNull<PedidoHistorialResponse>, StatusConverter {

    @Expose
    @SerializedName("listP")
    private List<DetallePedido> listP;
    @Expose
    @SerializedName("status")
    private int status;
    @Expose
    @SerializedName("msg")
    private String msg;

    public List<DetallePedido> getListP() {
        return listP;
    }

    public void setListP(List<DetallePedido> listP) {
        this.listP = listP;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "PedidoHistorialResponse{" +
                "listP=" + listP +
                ", status=" + status +
                ", msg='" + msg + '\'' +
                '}';
    }

    public static class DetallePedido implements IStringNotNull<DetallePedido> {
        @StringNotNull
        @Expose
        @SerializedName("idPed")
        private String idPed;
        @StringNotNull
        @Expose
        @SerializedName("nom")
        private String nom;
        @StringNotNull
        @Expose
        @SerializedName("ruc")
        private String ruc;
        @StringNotNull
        @Expose
        @SerializedName("rs")
        private String rs;
        @StringNotNull
        @Expose
        @SerializedName("total")
        private String total;
        @StringNotNull
        @Expose
        @SerializedName("moneda")
        private String moneda;
//        @StringNotNull
//        @Expose
//        @SerializedName("nomVen")
//        private String nomVen;
//        @StringNotNull
//        @Expose
//        @SerializedName("idfv")
//        private String idfv;
        @StringNotNull
        @Expose
        @SerializedName("estado")
        private String estado;

        public String getIdPed() {
            return idPed;
        }

        public void setIdPed(String idPed) {
            this.idPed = idPed;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public String getRuc() {
            return ruc;
        }

        public void setRuc(String ruc) {
            this.ruc = ruc;
        }

        public String getRs() {
            return rs;
        }

        public void setRs(String rs) {
            this.rs = rs;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getMoneda() {
            return moneda;
        }

        public void setMoneda(String moneda) {
            this.moneda = moneda;
        }

        public String getEstado() {
            return estado;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }


        @Override
        public String toString() {
            return "DetallePedido{" +
                    "idPed='" + idPed + '\'' +
                    ", nom='" + nom + '\'' +
                    ", ruc='" + ruc + '\'' +
                    ", rs='" + rs + '\'' +
                    ", total='" + total + '\'' +
                    ", moneda='" + moneda + '\'' +
                    ", estado='" + estado + '\'' +
                    '}';
        }

        @Override
        public DetallePedido getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

        @Override
    public PedidoHistorialResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }
}
