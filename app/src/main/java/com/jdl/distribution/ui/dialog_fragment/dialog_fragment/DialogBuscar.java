package com.jdl.distribution.ui.dialog_fragment.dialog_fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Destroyer;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.developer.appsupport.utils.JSONGenerator;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.ui.dialog_fragment.dialog_fragment.model.BuscarProductoResponse;
import com.jdl.distribution.ui.dialog_fragment.dialog_fragment.presenter.SearchProductPresenter;
import com.jdl.distribution.ui.dialog_fragment.dialog_fragment.view.SearchProductView;
import com.jdl.distribution.ui.fragment.base.BaseMvpDialogFragment;
import com.jdl.distribution.ui.fragment.visita.model.ProductoFrecuenteResponse;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.adapter.AdapterBinder;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogBuscar extends BaseMvpDialogFragment<SearchProductView, SearchProductPresenter>
        implements View.OnClickListener, SearchProductView {

    public static final String CLIENTE = "CLIENTE";
    private BusquedaAdapter adapter;
    private List<BuscarProductoResponse.Product> products;
    private Cliente cliente;

    @BindView(R.id.edt_query)
    EditText edtQuery;
    @BindView(R.id.ll_search)
    LinearLayout llSearch;
    @BindView(R.id.recycler_busqueda)
    RecyclerView recyclerBusqueda;
    @BindView(R.id.btn_regresar)
    Button btnRegresar;
    @BindView(R.id.btn_agregar)
    Button btnAgregar;
    @BindView(R.id.body)
    LinearLayout body;

    @Destroy(type = Type.PROGRESS_BAR)
    @BindView(R.id.pb_buscar_producto)
    public ProgressBar pb_buscar_producto;

    private CountDownTimer countDownTimerTextChangedOrigin;
    private OnSelectItemListener listener;
    String search;
    ArrayList<ProductoFrecuenteResponse.Product> listProduct;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_buscar, container, false);
        setCancelable(false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        if (getArguments() != null) {

            cliente = Parcels.unwrap(getArguments().getParcelable(CLIENTE));

        }

        setListener();
        cargarListadoBusqueda();
    }

    @Override
    public SearchProductPresenter createPresenter() {
        return new SearchProductPresenter(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() == null || getDialog().getWindow() == null)
            return;

        if (getActivity() == null)
            return;

        WindowManager.LayoutParams layoutParams = getDialog().getWindow().getAttributes();
        layoutParams.width = RelativeLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = RelativeLayout.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes(layoutParams);

        listProduct = new ArrayList<>();
        listProduct.clear();

        Constants.showSoftKeyboard(getContext(), edtQuery);
    }

    private void setListener() {
        btnRegresar.setOnClickListener(this);
        btnAgregar.setOnClickListener(this);
        body.setOnClickListener(this);

        edtQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                search = editable.toString();

                if (countDownTimerTextChangedOrigin != null) {
                    destroyCountDownTimerTextChanged();
                } else {

                    countDownTimerTextChangedOrigin = new CountDownTimer(Constants.TIME_MAX_SEARCH, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
//                                    Destroyer.subscribe(DialogBuscar.this);
//                                    pb_buscar_producto.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onFinish() {
//                                    Destroyer.unsubscribe(DialogBuscar.this);
//                                    pb_buscar_producto.setVisibility(View.GONE);
                            searchProducts(search);
                        }
                    }.start();
                }
            }
        });
    }

    private void searchProducts(String query) {

        if (getActivity() == null)
            return;

        if (cliente != null) {
            JSONGenerator.getNewInstance(getActivity())
                    .requireInternet(true)
//                    .put("idcli", cliente.getIdcli())
                    .put("id_perfil", new LoginPref(getContext()).getIdProfile())
                    .put("id_usuario", new LoginPref(getContext()).getUid())
                    .put("nom_pro", query)
                    .operate(getPresenter()::doSearchProduct);

        }
//        else {
//            JSONGenerator.getNewInstance(getActivity())
//                    .requireInternet(true)
//                    .put("idcli", codCliente)
//                    .put("idpf", new LoginPref(getContext()).getIdProfile())
//                    .put("idfv", new LoginPref(getContext()).getUid())
//                    .put("uuid", new LoginPref(getContext()).getToken())
//                    .put("cta", new LoginPref(getContext()).getCuenta())
//                    .put("mon", codMoneda)
//                    .put("tipo", tipo)
//                    .put("nompr", query)
//                    .operate(getPresenter()::doSearchProduct);
//        }


    }

    private void cargarListadoBusqueda() {

        products = new ArrayList<>();
        recyclerBusqueda.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerBusqueda.setItemAnimator(new DefaultItemAnimator());

        adapter = new BusquedaAdapter(getActivity(), products, new BusquedaAdapter.OnSearchListener() {
            @Override
            public void onCheckItem(int position, boolean isChecked, BuscarProductoResponse.Product search) {
                if (isChecked) {

                    ProductoFrecuenteResponse.Product product = new ProductoFrecuenteResponse.Product();

                    product.setIdPro(search.getIdPro());
                    product.setCodigo(search.getCodigo());
                    product.setCostoSinDescuentos(search.getCostoSinDescuentos());
                    product.setNombre(search.getNombre());
                    product.setStock(search.getStock());
                    product.setSigla(search.getSimboloMonedad());

                    listProduct.add(product);

                    if (!search.isChecked())
                        adapter.setEnableCantidad(position, true);

//                    Log.d("SEARCH_ADD",listProduct.size()+" - "+"Nombre: "+search.getNombre()+" - AGREGADO");
                } else {

                    for (int i = 0; i < listProduct.size(); i++) {
                        if (search.getIdPro().equals(listProduct.get(i).getIdPro())) {
                            listProduct.remove(i);
                        }
                    }

                    if (search.isChecked())
                        adapter.setEnableCantidad(position, false);

//                    Log.d("SEARCH_DELETE",listProduct.size()+" - "+"Nombre: "+search.getNombre()+" - ELIMINADO");
                }
            }
        });
        recyclerBusqueda.setAdapter(adapter);
    }

    private void destroyCountDownTimerTextChanged() {

        try {
            countDownTimerTextChangedOrigin.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            countDownTimerTextChangedOrigin.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {

        if (getActivity() == null)
            return;

        switch (view.getId()) {

            case R.id.btn_regresar:
                dismiss();
                break;
            case R.id.btn_agregar:

                listener.onSelectItem(listProduct);

                break;
            case R.id.body:
                dismiss();
                break;

        }

    }

    @Override
    public void showLoadingDialog(boolean status) {
        if (status) {
            try {
                Destroyer.subscribe(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            pb_buscar_producto.setVisibility(View.VISIBLE);
        } else {
            try {
                Destroyer.unsubscribe(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            pb_buscar_producto.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSearchProductSuccess(BuscarProductoResponse response) {
        products.clear();

        if (response.getListP() == null)
            return;

        products.addAll(response.getListP());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSearchProductFail(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public static class BusquedaAdapter extends RecyclerView.Adapter<BusquedaAdapter.ViewHolder> {

        Context context;
        List<BuscarProductoResponse.Product> listadoBuscar;
        OnSearchListener listener;

        public BusquedaAdapter(Context context, List<BuscarProductoResponse.Product> listadoBuscar, OnSearchListener listener) {
            this.context = context;
            this.listadoBuscar = listadoBuscar;
            this.listener = listener;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_buscar_producto, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHOlder, int i) {
            BuscarProductoResponse.Product model = listadoBuscar.get(i);
            viewHOlder.bindTo(model);
        }

        @Override
        public int getItemCount() {
            return listadoBuscar.size();
        }

        public void setEnableCantidad(int position, boolean checked) {
            listadoBuscar.get(position).setChecked(checked);
            notifyDataSetChanged();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements AdapterBinder<BuscarProductoResponse.Product>, CheckBox.OnCheckedChangeListener {
            @BindView(R.id.chb_add)
            CheckBox chb_add;
            @BindView(R.id.txt_nombre_pedido_busqueda_item)
            TextView pedido;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                chb_add.setOnCheckedChangeListener(this);
            }

            @Override
            public void bindTo(@Nullable BuscarProductoResponse.Product product) {
                if (product != null) {
                    pedido.setText(product.getNombre());
                    chb_add.setChecked(product.isChecked());
                }
            }

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                switch (buttonView.getId()) {
                    case R.id.chb_add:
                        if (getAdapterPosition() != -1) {
                            listener.onCheckItem(getAdapterPosition(), isChecked, listadoBuscar.get(getAdapterPosition()));
                        }
                        break;
                }
            }
        }

        private interface OnSearchListener {
            void onCheckItem(int position, boolean isChecked, BuscarProductoResponse.Product product);
        }

    }

    public void setOnSelectItemListener(OnSelectItemListener listener) {
        this.listener = listener;
    }

    public interface OnSelectItemListener {

        void onSelectItem(List<ProductoFrecuenteResponse.Product> product);


    }

}
