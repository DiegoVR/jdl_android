package com.jdl.distribution.ui.fragment.clientes;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Destroyer;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.mvp.model.Message;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.utils.JSONGenerator;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.AppPref;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.mvp.model.ClienteResponse;
import com.jdl.distribution.ui.fragment.agregar_cliente.AgregarClienteFragment;
import com.jdl.distribution.ui.fragment.agregar_cliente.AgregarClienteFragmentBuilder;
import com.jdl.distribution.ui.fragment.base.BaseMvpFragment;
import com.jdl.distribution.ui.fragment.clientes.model.ComboFrecuenciaResponse;
import com.jdl.distribution.ui.fragment.clientes.model.SeleccioneClienteResponse;
import com.jdl.distribution.ui.fragment.clientes.presenter.ClientePresenter;
import com.jdl.distribution.ui.fragment.clientes.view.ClientesView;
import com.jdl.distribution.ui.fragment.editar_cliente.EditarClienteFragment;
import com.jdl.distribution.ui.fragment.clientes.adapter.ClienteAdapter;
import com.jdl.distribution.ui.fragment.editar_cliente.EditarClienteFragmentBuilder;
import com.jdl.distribution.ui.fragment.visita.VisitaFragment;
import com.jdl.distribution.ui.fragment.visita.VisitaFragmentBuilder;
import com.jdl.distribution.utils.Constants;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClientesFragment extends BaseMvpFragment<ClientesView, ClientePresenter>
        implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, ClientesView {

    private static boolean AGREGAR = false;

    @BindView(R.id.btn_agregar_cliente)
    ProgressButton btn_agregar_cliente;
    @BindView(R.id.edt_search_clients)
    EditText edt_search_clients;
    @BindView(R.id.pb_search_products)
    ProgressBar pb_search_products;
    @Destroy(type = Type.SWIPE_REFRESH)
    @BindView(R.id.sw_clients)
    public SwipeRefreshLayout sw_clients;
    @BindView(R.id.rv_list_clients)
    RecyclerView rv_list_clients;

    private ClienteAdapter adapter;
    private ArrayList<Cliente> listCliente = new ArrayList<>();
    private LoginPref loginPref;
    private SeleccioneClienteResponse selClienteResponse;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_clientes, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        if (getActivity() == null)
            return;

        loginPref = new LoginPref(getContext());
        setListener();
        setAdapter();
        loadData();
        setData();
        search();
    }

    private void setData() {
        if (loginPref.getIdProfile().equals(Constants.PROFILE_REPARTO)) {
            btn_agregar_cliente.setVisibility(View.GONE);
        }
    }

    private void search() {
        edt_search_clients.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filtrar(s.toString());
            }


        });
    }

    private void filtrar(String text) {

        ArrayList<Cliente> listFiltroPedido = new ArrayList<>();

        for (Cliente local : listCliente) {
            if (local.getNombre().toLowerCase().contains(text.toLowerCase()) ||
                    local.getRazonSocial().toLowerCase().contains(text.toLowerCase()) ||
                    local.getContacto().toLowerCase().contains(text.toLowerCase()) ||
                    local.getDireccion().toLowerCase().contains(text.toLowerCase()) ||
                    local.getRuc().toLowerCase().contains(text.toLowerCase())) {
                listFiltroPedido.add(local);
            }
        }

        adapter.filterList(listFiltroPedido);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void setAdapter() {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        rv_list_clients.setLayoutManager(layoutManager);
        rv_list_clients.setItemAnimator(new DefaultItemAnimator());

        adapter = new ClienteAdapter(getContext(), listCliente, new ClienteAdapter.ClienteListener() {
            @Override
            public void onItemClickListener(Cliente clientes, int i) {
//                Toast.makeText(getContext(), clientes.getNombre(), Toast.LENGTH_SHORT).show();

                JSONGenerator.getNewInstance(getActivity())
                        .requireInternet(true)
                        .put("id_perfil", loginPref.getIdProfile())
                        .put("id_usuario", loginPref.getUid())
                        .put("id_cliente", clientes.getIdcli())
                        .operate(jsonObject -> {
                            getPresenter().doSeleccionaCliente(jsonObject);
                        });

            }

            @Override
            public void onItemClickInitVisitListener(Cliente clientes, int i) {
                Toast.makeText(getContext(), clientes.getNombre(), Toast.LENGTH_SHORT).show();

                Fragment signupFragment = new VisitaFragmentBuilder(clientes, null, null).build();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack(null);
                transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
                transaction.add(R.id.main_content, signupFragment, VisitaFragment.class.getSimpleName());
                transaction.commit();

            }
        });
        rv_list_clients.setAdapter(adapter);
    }

    private void setListener() {
        sw_clients.setOnRefreshListener(this);
        btn_agregar_cliente.setOnClickListener(this);
    }

    @Override
    public void onRefresh() {
        if (Constants.verifyInternetAndMqtt(getContext())) {
            sw_clients.setRefreshing(false);
            return;
        }
        loadData();
    }

    private void loadData() {
        if (getActivity() == null)
            return;

        JSONGenerator.getNewInstance(getActivity())
                .requireInternet(true)
                .put("id_perfil", loginPref.getIdProfile())
                .put("id_usuario", loginPref.getUid())
                .put("lat", new AppPref(getContext()).getLatitude())
                .put("lon", new AppPref(getContext()).getLongitude())
                .put("mapa_val", "prueba")
                .operate(jsonObject -> {
                    getPresenter().doClientsList(jsonObject);
                });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_agregar_cliente:
                AGREGAR = true;
                JSONGenerator.getNewInstance(getActivity())
                        .requireInternet(true)
                        .put("id_perfil", loginPref.getIdProfile())
                        .put("id_usuario", loginPref.getUid())
                        .operate(jsonObject -> {
                            getPresenter().doListarFrecuencia(jsonObject);
                        });

                break;
        }
    }

    @Override
    public ClientePresenter createPresenter() {
        return new ClientePresenter(getActivity());
    }

    @Override
    public void showLoadingDialog(boolean status) {
        if (status) {
            try {
                Destroyer.subscribe(this);
                sw_clients.setRefreshing(true);
            } catch (Exception e) {

            }
        } else {
            try {
                Destroyer.unsubscribe(this);
                sw_clients.setRefreshing(false);
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onListClientsSuccess(ClienteResponse response) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
        if (response.getListC() != null) {
            listCliente.clear();
            listCliente.addAll(response.getListC());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onListClientsFail(String response) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSeleccionaClienteSuccess(SeleccioneClienteResponse response) {
        if (getActivity() == null) return;
        selClienteResponse = response;
        JSONGenerator.getNewInstance(getActivity())
                .requireInternet(true)
                .put("id_perfil", loginPref.getIdProfile())
                .put("id_usuario", loginPref.getUid())
                .operate(jsonObject -> {
                    getPresenter().doListarFrecuencia(jsonObject);
                });


    }

    @Override
    public void onSeleccionaClienteFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onListarComboFrecuenciaSuccess(ComboFrecuenciaResponse response) {
        if (getActivity() == null) return;

        if (AGREGAR) {
            AGREGAR = false;
            Fragment agregar = new AgregarClienteFragmentBuilder(response.getListFrecu(), response.getListMP(), response.getListZona()).build();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.addToBackStack(null);
            transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
            transaction.add(R.id.main_content, agregar, AgregarClienteFragment.class.getSimpleName());
            transaction.commit();
        } else {
            Fragment signupFragment = new EditarClienteFragmentBuilder(selClienteResponse.getCliente(), response.getListFrecu(), response.getListMP(), response.getListZona()).build();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack(ClientesFragment.class.getSimpleName());
            transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
            transaction.add(R.id.main_content, signupFragment, EditarClienteFragment.class.getSimpleName());
            transaction.commit();
        }

    }

    @Override
    public void onListarComboFrecuenciaFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message message) {
        if (message.getOpStr().equals(Constants.OP_NOTIFY_LIST_CLIENTE)) {
            loadData();
        }
    }

}
