package com.jdl.distribution.ui.fragment.historial_eventos;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.mvp.model.Message;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.fragment.historial_eventos.adapter.HistorialEventoAdapter;
import com.jdl.distribution.ui.fragment.historial_eventos.model.HistorialEventoResponse;
import com.jdl.distribution.utils.Constants;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistorialEventoFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @Destroy(type = Type.SWIPE_REFRESH)
    @BindView(R.id.sw_eventos_historial)
    public SwipeRefreshLayout sw_eventos_historial;
    @BindView(R.id.rv_eventos_historial)
    RecyclerView rv_eventos_historial;
    @BindView(R.id.edt_evento)
    EditText edt_evento;
    @BindView(R.id.txt_cantidad)
    TextView txt_cantidad;

    private LoginPref loginPref;
    private HistorialEventoAdapter eventoAdapter;
    private ArrayList<HistorialEventoResponse.DetalleEvento> eventoList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_historial_evento, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        if (getActivity() == null)
            return;

        loginPref = new LoginPref(getContext());

        setAdapters();
        setListeners();
        search();

    }

    private void setAdapters() {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        rv_eventos_historial.setLayoutManager(layoutManager);
        rv_eventos_historial.setItemAnimator(new DefaultItemAnimator());

       /* eventoList.clear();

        String nomEvento [] = {
                "Pedido Generado",
                "Venta Generada",
                "Entrega Relalizada",
                "Pedido Generado",
                "Pedido Generado",
                "Venta Generada",
                "Pedido Generado",
                "Pedido Generado",
                "Entrega Realizada",
                "Pedido Generado"};

        String hora [] = {"22:50","12:20","13:50","5:40","18:53","10:25","8:20","6:45","7:30","13:55"};
        String nomCliente [] = {"Carlos","Juan","Manuel","Oscar","Luis","Sergio","Cristian","Antony","Eduardo","Raul"};
        String ruc [] = {"52145236215","15245124785","","12523654128","45212365895","12354785412","12546985236","52136202123","","52032012018"};
        String rs [] = {"Bodega Juan S.A.C","Bodega Carlos S.A.C","","Bodega Oscar S.A.C","Bodega Luis S.A.C","Bodega Rodriguez S.A.C","Bodega Kiara S.A.C","Bodega Juan S.A.C","","Bodega Juan S.A.C"};
        String nomVendedor [] = {"Diego","Diego","Carlos","Diego","Diego","Diego","Diego","Diego","Carlos","Diego"};
        String tipoComprobante [] = {"1","2","","1","1","2","1","1","","1"};
        String idComprobante [] = {"1","2","","3","4","5","6","7","","8"};
        String nombreDoc [] = {"F000-000001","F000-000002","","F000-000003","F000-000004","F000-000005","F000-000006","F000-000007","","F000-000008"};
        String tipoDOc [] = {"PEDIDO","VENTA","","PEDIDO","PEDIDO","VENTA","PEDIDO","PEDIDO","","PEDIDO"};


        for (int i = 0; i < hora.length; i++){
            HistorialEventoResponse.DetalleEvento evento = new HistorialEventoResponse.DetalleEvento();
            evento.setNombreEvento(nomEvento[i]);
            evento.setHora(hora[i]);
            evento.setNombreCliente(nomCliente[i]);
            evento.setRuc(ruc[i]);
            evento.setRs(rs[i]);
            evento.setNomVen(nomVendedor[i]);
            evento.setTipoComprobante(tipoComprobante[i]);
            evento.setIdComprobante(idComprobante[i]);
            evento.setNombreDocumento(nombreDoc[i]);
            evento.setTipoDocumento(tipoDOc[i]);
            eventoList.add(evento);
        }
*/
        eventoAdapter = new HistorialEventoAdapter(getContext(), eventoList, new HistorialEventoAdapter.EventoHistorialListener() {
            @Override
            public void onItemClickListener(HistorialEventoResponse.DetalleEvento evento) {

            }

            @Override
            public void onItemClickListenerEditar(HistorialEventoResponse.DetalleEvento evento) {

            }

            @Override
            public void onItemClickListenerImprimir(HistorialEventoResponse.DetalleEvento evento) {

            }
        });

        rv_eventos_historial.setAdapter(eventoAdapter);
        eventoAdapter.notifyDataSetChanged();
        sw_eventos_historial.setRefreshing(false);
        txt_cantidad.setText(eventoList.size() + "");
    }

    private void search() {
        edt_evento.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filtrar(s.toString());
            }

        });
    }

    private void filtrar(String text) {

        ArrayList<HistorialEventoResponse.DetalleEvento> listFiltroCliente = new ArrayList<>();

        for (HistorialEventoResponse.DetalleEvento local : eventoList) {
            if (local.getNombreEvento().toLowerCase().contains(text.toLowerCase()) ||
                    local.getNombreCliente().toLowerCase().contains(text.toLowerCase()) ||
                    local.getNombreDocumento().toLowerCase().contains(text.toLowerCase()) ||
                    local.getRs().toLowerCase().contains(text.toLowerCase()) ||
                    local.getRuc().toLowerCase().contains(text.toLowerCase()) ||
                    local.getNomVen().toLowerCase().contains(text.toLowerCase())) {
                listFiltroCliente.add(local);
            }
        }

        eventoAdapter.filterList(listFiltroCliente);
        txt_cantidad.setText(eventoAdapter.filterList(listFiltroCliente) + "");

    }

    private void setListeners() {
        sw_eventos_historial.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        if (Constants.verifyInternetAndMqtt(getContext())) {
            sw_eventos_historial.setRefreshing(false);
            return;
        }
        setAdapters();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMesage(Message message) {
        if (message.getOpStr().equals(HistorialEventoFragment.class.getSimpleName())) {
//            Toast.makeText(getContext(), message.getMessage(), Toast.LENGTH_SHORT).show();
//            fecha = message.getMessage();
//            search = message.getMessage2();
            setAdapters();

        }
//        if (message.getOpStr().equals(HistorialEventoFragment.class.getSimpleName()+"anulado")){
//            loadData(fecha,search);
//        }
    }
}
