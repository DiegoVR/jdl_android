package com.jdl.distribution.ui.fragment.clientes.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import org.parceler.Parcel;

import java.util.List;

public class ComboFrecuenciaResponse implements IStringNotNull<ComboFrecuenciaResponse>, StatusConverter {

    @Expose
    @StringNotNull
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("listFre")
    private List<Frecuencia> listFrecu;
    @Expose
    @SerializedName("listZona")
    private List<Zona> listZona;
    @Expose
    @SerializedName("listMP")
    private List<MedioPago> listMP;
    @Expose
    @SerializedName("status")
    private int status;

    public String getMsg() {
        return msg;
    }

    public List<Frecuencia> getListFrecu() {
        return listFrecu;
    }

    public List<MedioPago> getListMP() {
        return listMP;
    }

    public List<Zona> getListZona() {
        return listZona;
    }

    public int getStatus() {
        return status;
    }



    @Override
    public String toString() {
        return "ComboFrecuenciaResponse{" +
                "msg='" + msg + '\'' +
                ", listFrecu=" + listFrecu +
                ", listZona=" + listZona +
                ", listMP=" + listMP +
                ", status=" + status +
                '}';
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class MedioPago implements IStringNotNull<MedioPago>{
        @Expose
        @StringNotNull
        @SerializedName("idMediop")
        private String idMedio;
        @Expose
        @StringNotNull
        @SerializedName("mepDescripcion")
        private String nombre;
        @Expose
        @StringNotNull
        @SerializedName("estado")
        private int estado;

        public String getIdMedio() {
            return idMedio;
        }

        public String getNombre() {
            return nombre;
        }

        public int getEstado() {
            return estado;
        }


        @Override
        public String toString() {
            return "MedioPago{" +
                    "idMedio='" + idMedio + '\'' +
                    ", nombre='" + nombre + '\'' +
                    ", estado=" + estado +
                    '}';
        }

        @Override
        public MedioPago getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Zona implements IStringNotNull<Zona>{
        @Expose
        @StringNotNull
        @SerializedName("idZona")
        private String idZona;
        @Expose
        @StringNotNull
        @SerializedName("zonNombre")
        private String nombre;
        @Expose
        @StringNotNull
        @SerializedName("estado")
        private int estado;

        public String getIdZona() {
            return idZona;
        }

        public String getNombre() {
            return nombre;
        }

        public int getEstado() {
            return estado;
        }

        @Override
        public String toString() {
            return "Zona{" +
                    "idZona='" + idZona + '\'' +
                    ", nombre='" + nombre + '\'' +
                    ", estado=" + estado +
                    '}';
        }

        @Override
        public Zona getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Frecuencia implements IStringNotNull<Frecuencia>{

        @Expose
        @StringNotNull
        @SerializedName("id")
        private String id;
        @Expose
        @StringNotNull
        @SerializedName("nombre")
        private String nombre;
        @Expose
        @StringNotNull
        @SerializedName("dias")
        private String dias;

        public String getId() {
            return id;
        }

        public String getDias() {
            return dias;
        }

        public String getNombre() {
            return nombre;
        }

        @Override
        public String toString() {
            return "Frecuencia{" +
                    "id='" + id + '\'' +
                    ", dias='" + dias + '\'' +
                    ", nombre='" + nombre + '\'' +
                    '}';
        }

        @Override
        public Frecuencia getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Override
    public ComboFrecuenciaResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }
}
