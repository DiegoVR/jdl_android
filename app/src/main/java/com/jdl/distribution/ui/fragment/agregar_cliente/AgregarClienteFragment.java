package com.jdl.distribution.ui.fragment.agregar_cliente;

import android.Manifest;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import butterknife.BindView;
import butterknife.ButterKnife;

import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.developer.appsupport.annotation.onetime.OneTime;
import com.developer.appsupport.annotation.onetime.OneTimer;
import com.developer.appsupport.mvp.model.Message;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.ui.dialog.select_photo.SelectPhotoDialog;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.developer.appsupport.utils.JSONGenerator;
import com.developer.appsupport.utils.files.ImageListener;
import com.developer.appsupport.utils.files.ImagePresenter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.bundler.ParcelerArgsBundler;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.AppPref;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.ui.fragment.agregar_cliente.model.MigoDocumentoResponse;
import com.jdl.distribution.ui.fragment.agregar_cliente.presenter.AgregarClientePresenter;
import com.jdl.distribution.ui.fragment.agregar_cliente.view.AgregarClienteView;
import com.jdl.distribution.ui.fragment.base.BaseMvpFragment;
import com.jdl.distribution.ui.fragment.clientes.model.ComboFrecuenciaResponse;
import com.jdl.distribution.utils.Constants;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AgregarClienteFragment extends BaseMvpFragment<AgregarClienteView, AgregarClientePresenter>
        implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener, ImageListener,
        SelectPhotoDialog.OnTypeSelectedListener,
        CheckBox.OnCheckedChangeListener, View.OnClickListener, AgregarClienteView {

    private static final String LOG_TAG = AgregarClienteFragment.class.getSimpleName();

    @Arg(bundler = ParcelerArgsBundler.class)
    List<ComboFrecuenciaResponse.Frecuencia> listComboFrecuencia;
    @Arg(bundler = ParcelerArgsBundler.class)
    List<ComboFrecuenciaResponse.Zona> listComboZona;
    @Arg(bundler = ParcelerArgsBundler.class)
    List<ComboFrecuenciaResponse.MedioPago> listComboMedioPago;
    private ImagePresenter imagePresenter;
    private SelectPhotoDialog selectPhotoDialog;

    private GoogleMap mMap;
    //    private GeoApiContext geoApiContext;
    private AppPref appPref;
    private LoginPref loginPref;

    @BindView(R.id.txt_direccion_mapa)
    TextView txt_direccion_mapa;
    @BindView(R.id.spinner_frecuencia)
    Spinner spinner_frecuencia;
    @BindView(R.id.sp_zonas)
    Spinner sp_zonas;
    @BindView(R.id.sp_medio_pago)
    Spinner sp_medio_pago;

    @BindView(R.id.rb_ruc)
    RadioButton rb_ruc;
    @BindView(R.id.rb_dni)
    RadioButton rb_dni;

    @BindView(R.id.edt_nombre_local)
    TextView edtNombreLocal;
    @BindView(R.id.edt_nombre_cliente)
    TextView edtNombreCliente;
    @OneTime
    @BindView(R.id.btn_guardar_cliente)
    ProgressButton btn_guardar_cliente;
    @BindView(R.id.ll_horario_inicial)
    LinearLayout ll_horario_inicial;
    @BindView(R.id.edt_horario_inicial)
    TextView edt_horario_inicial;
    @BindView(R.id.ll_horario_final)
    LinearLayout ll_horario_final;
    @BindView(R.id.edt_horario_final)
    TextView edt_horario_final;

    @BindView(R.id.edt_telefono_local)
    EditText edt_telefono_local;
    @BindView(R.id.edt_correo_local)
    EditText edt_correo_local;

    @BindView(R.id.edt_telefono_empresa)
    EditText edt_telefono_empresa;
    @BindView(R.id.edt_ruc_empresa)
    EditText edt_ruc_empresa;
    @BindView(R.id.edt_razon_social_empresa)
    EditText edt_razon_social_empresa;
    @BindView(R.id.edt_correo_empresa)
    EditText edt_correo_empresa;
    @BindView(R.id.edt_direccion_empresa)
    EditText edt_direccion_empresa;
    @BindView(R.id.img_buscar)
    ImageView img_buscar;

    @BindView(R.id.chk_lun)
    CheckBox chk_lun;
    @BindView(R.id.chk_mar)
    CheckBox chk_mar;
    @BindView(R.id.chk_mie)
    CheckBox chk_mie;
    @BindView(R.id.chk_jue)
    CheckBox chk_jue;
    @BindView(R.id.chk_vie)
    CheckBox chk_vie;
    @BindView(R.id.chk_sab)
    CheckBox chk_sab;
    @BindView(R.id.chk_dom)
    CheckBox chk_dom;

    @BindView(R.id.chk_lunE)
    CheckBox chk_lunE;
    @BindView(R.id.chk_marE)
    CheckBox chk_marE;
    @BindView(R.id.chk_mieE)
    CheckBox chk_mieE;
    @BindView(R.id.chk_jueE)
    CheckBox chk_jueE;
    @BindView(R.id.chk_vieE)
    CheckBox chk_vieE;
    @BindView(R.id.chk_sabE)
    CheckBox chk_sabE;
    @BindView(R.id.chk_domE)
    CheckBox chk_domE;
    @BindView(R.id.img_agregar)
    ImageView img_agregar;

    @BindView(R.id.img_block_habilitar)
    ImageCardView img_block_habilitar;
    @BindView(R.id.img_block_deshabilitar)
    ImageCardView img_block_deshabilitar;
    @BindView(R.id.ll_block)
    LinearLayout ll_block;
    @BindView(R.id.view_block)
    View view_block;

    // LIST CHECK
    ArrayList<DiasSemana> diasListSelect = new ArrayList<>();
    ArrayList<DiasSemana> entregaListSelect = new ArrayList<>();

    private String frecuenciaSelected = "";
    private String medioPagoSelected = "";
    private String zonaSelected = "";
    private String diasvalidos = "0";
    private String tipoComprobante = "";
    private String tipoDocumento = "";
    private int documentoSeleccionado;
    private File imgFile;

    private int click = -1;
    private int previousExpandedPosition = -1;
    private String horaSeleccionadaInicial = "";
    private String horaSeleccionadaFinal = "";

    private static final String CERO = "0";
    private static final String DOS_PUNTOS = ":";
    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la hora hora
    final int hora = c.get(Calendar.HOUR_OF_DAY);
    final int minuto = c.get(Calendar.MINUTE);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agregar_cliente, container, false);
        ButterKnife.bind(this, view);
        FragmentArgs.inject(this);
        init();
        return view;
    }

    private boolean validateCampos() {

        if (edt_ruc_empresa.getText().toString().isEmpty()) {
            edt_ruc_empresa.setError("Complete campo");
            return false;
        }
        if (edt_razon_social_empresa.getText().toString().isEmpty()) {
            edt_razon_social_empresa.setError("Complete campo");
            return false;
        }
        if (edt_direccion_empresa.getText().toString().isEmpty()) {
            edt_direccion_empresa.setError("Complete campo");
            return false;
        }
        if (edtNombreLocal.getText().toString().isEmpty()) {
            edtNombreLocal.setError("Complete campo");
            return false;
        }
        if (edt_horario_inicial.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Seleccione horario inicial", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (edt_horario_final.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Seleccione horario final", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (medioPagoSelected.isEmpty()) {
            Toast.makeText(getContext(), "Seleccione medio de pago", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (zonaSelected.isEmpty()) {
            Toast.makeText(getContext(), "Seleccione zona", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public AgregarClientePresenter createPresenter() {
        return new AgregarClientePresenter(getContext());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        appPref = new AppPref(getContext());
        loginPref = new LoginPref(getContext());
        initGoogleMap();
    }

    private void initGoogleMap() {
        if (getActivity() == null)
            return;

        FragmentManager fm = getActivity().getSupportFragmentManager();/// getChildFragmentManager();
        SupportMapFragment supportMapFragment = SupportMapFragment.newInstance();
        fm.beginTransaction().replace(R.id.map_ubicacion_agregar_cliente, supportMapFragment).commit();
        supportMapFragment.getMapAsync(this);
    }

    private void init() {
        if (getActivity() == null)
            return;

        rb_dni.setChecked(true);
        tipoDocumento = Constants.DNI;
        tipoComprobante = Constants.BOLETA;
        documentoSeleccionado = 1;

        setAdapterSpinnerFrecuencia();
        setAdapterSpinnerZona();
        setAdapterSpinnerTipoPago();
        setListener();
        clickExpandible();

    }

    private void setAdapterSpinnerTipoPago() {
        List<String> tipoListStr = new ArrayList<>();
        for (int i = 0; i < listComboMedioPago.size(); i++) {
            tipoListStr.add(listComboMedioPago.get(i).getNombre());
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, tipoListStr);
        sp_medio_pago.setAdapter(arrayAdapter);
        sp_medio_pago.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                medioPagoSelected = listComboMedioPago.get(i).getIdMedio();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setAdapterSpinnerZona() {
        List<String> zonaListStr = new ArrayList<>();
        for (int i = 0; i < listComboZona.size(); i++) {
            zonaListStr.add(listComboZona.get(i).getNombre());
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, zonaListStr);
        sp_zonas.setAdapter(arrayAdapter);
        sp_zonas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                zonaSelected = listComboZona.get(i).getIdZona();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void clickExpandible() {

        final boolean isExpanded = 1 == click;
        view_block.setVisibility(isExpanded ? View.GONE : View.VISIBLE);

        ll_block.setActivated(isExpanded);

        if (isExpanded) previousExpandedPosition = 1;

        ll_block.setOnClickListener(view -> {
            if (view_block.getVisibility() != View.VISIBLE) {
                ll_block.setActivated(true);
                view_block.setVisibility(View.VISIBLE);
                img_block_deshabilitar.setVisibility(View.GONE);
                img_block_habilitar.setVisibility(View.VISIBLE);
                mMap.getUiSettings().setScrollGesturesEnabled(false);
                mMap.getUiSettings().setZoomGesturesEnabled(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                }
                mMap.setMyLocationEnabled(false);

//                CameraPosition cameraPosition = new CameraPosition.Builder().target(cliente.getLatLng()).zoom(14).build();
//                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


            } else {
                img_block_habilitar.setVisibility(View.GONE);
                img_block_deshabilitar.setVisibility(View.VISIBLE);
                ll_block.setActivated(false);
                view_block.setVisibility(View.GONE);
                mMap.getUiSettings().setScrollGesturesEnabled(true);
                mMap.getUiSettings().setZoomGesturesEnabled(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                }
                mMap.setMyLocationEnabled(true);
            }
        });

    }

    private void setListener() {
        chk_lun.setOnCheckedChangeListener(this);
        chk_mar.setOnCheckedChangeListener(this);
        chk_mie.setOnCheckedChangeListener(this);
        chk_jue.setOnCheckedChangeListener(this);
        chk_vie.setOnCheckedChangeListener(this);
        chk_sab.setOnCheckedChangeListener(this);
        chk_dom.setOnCheckedChangeListener(this);

        chk_lunE.setOnCheckedChangeListener(this);
        chk_marE.setOnCheckedChangeListener(this);
        chk_mieE.setOnCheckedChangeListener(this);
        chk_jueE.setOnCheckedChangeListener(this);
        chk_vieE.setOnCheckedChangeListener(this);
        chk_sabE.setOnCheckedChangeListener(this);
        chk_domE.setOnCheckedChangeListener(this);

        btn_guardar_cliente.setOnClickListener(this);
        rb_ruc.setOnClickListener(this);
        rb_dni.setOnClickListener(this);
        img_buscar.setOnClickListener(this);
        ll_horario_inicial.setOnClickListener(this);
        ll_horario_final.setOnClickListener(this);

        btn_guardar_cliente.setOneTimerSupport(true);

    }

    private void setAdapterSpinnerFrecuencia() {
        List<String> frecuenciaListStr = new ArrayList<>();
        for (int i = 0; i < listComboFrecuencia.size(); i++) {
            frecuenciaListStr.add(listComboFrecuencia.get(i).getNombre());
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, frecuenciaListStr);
        spinner_frecuencia.setAdapter(arrayAdapter);
        spinner_frecuencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                frecuenciaSelected = listComboFrecuencia.get(i).getId();
                diasvalidos = listComboFrecuencia.get(i).getDias();

                clearCheck();
                validateCheckVisita();
                validateCheckEntrega();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void clearCheck() {
        int cantidad = 0;
        if (frecuenciaSelected.equals(Constants.FRECUENCIA_1)) {
            cantidad = 2;
        } else if (frecuenciaSelected.equals(Constants.FRECUENCIA_2) ||
                frecuenciaSelected.equals(Constants.FRECUENCIA_3) ||
                frecuenciaSelected.equals(Constants.FRECUENCIA_4)) {
            cantidad = 1;
        }

        if (diasListSelect.size() > cantidad) {
            diasListSelect.clear();

            chk_lun.setChecked(false);
            chk_mar.setChecked(false);
            chk_mie.setChecked(false);
            chk_jue.setChecked(false);
            chk_vie.setChecked(false);
            chk_sab.setChecked(false);
            chk_dom.setChecked(false);
        }
        if (entregaListSelect.size() > cantidad) {
            entregaListSelect.clear();

            chk_lunE.setChecked(false);
            chk_marE.setChecked(false);
            chk_mieE.setChecked(false);
            chk_jueE.setChecked(false);
            chk_vieE.setChecked(false);
            chk_sabE.setChecked(false);
            chk_domE.setChecked(false);
        }
    }

    private void validateCheckVisita() {
        if (!frecuenciaSelected.isEmpty() || frecuenciaSelected != null) {

            View[] botones = {chk_dom, chk_lun, chk_mar, chk_mie, chk_jue, chk_vie, chk_sab};
            String[] dias = {"1", "2", "3", "4", "5", "6", "7"};
            if (diasvalidos.equals("0")) {
                for (int k = 0; k < dias.length; k++) {
                    botones[k].setEnabled(false);
                }
                return;
            }
            if (diasListSelect.size() == Integer.parseInt(diasvalidos)) {

                for (int k = 0; k < dias.length; k++) {
                    for (int j = 0; j < diasListSelect.size(); j++) {
                        boolean result = diasListSelect.get(j).getIdDias() != dias[k];
                        if (!result) {
                            botones[k].setEnabled(true);
                            break;
                        } else {
                            botones[k].setEnabled(false);
                        }
                    }
                }

            } else if (diasListSelect.size() < Integer.parseInt(diasvalidos)) {
                for (int k = 0; k < botones.length; k++) {
                    botones[k].setEnabled(true);
                }
            }
        }
    }

    private void validateCheckEntrega() {

        if (!frecuenciaSelected.isEmpty() || frecuenciaSelected != null) {

            View[] botones = {chk_domE, chk_lunE, chk_marE, chk_mieE, chk_jueE, chk_vieE, chk_sabE};
            String[] dias = {"1", "2", "3", "4", "5", "6", "7"};

            if (diasvalidos.equals("0")) {
                for (int k = 0; k < dias.length; k++) {
                    botones[k].setEnabled(false);
                }
                return;
            }

            if (entregaListSelect.size() == Integer.parseInt(diasvalidos)) {

                for (int k = 0; k < dias.length; k++) {
                    for (int j = 0; j < entregaListSelect.size(); j++) {
                        Log.d("ENTREGA", entregaListSelect.toString());
                        boolean result = entregaListSelect.get(j).getIdDias() != dias[k];
                        if (!result) {
                            botones[k].setEnabled(true);
                            break;
                        } else {
                            botones[k].setEnabled(false);
                        }
                    }
                }

            } else if (entregaListSelect.size() < Integer.parseInt(diasvalidos)) {
                for (int k = 0; k < botones.length; k++) {
                    botones[k].setEnabled(true);
                }
            }

        }

    }

    private void agregarDiasVisita(String codigo, String nombre, boolean isChecked) {
        if (!frecuenciaSelected.isEmpty() || frecuenciaSelected != null) {
            if (isChecked) {
                DiasSemana dias = new DiasSemana();
                dias.setIdDias(codigo);
                dias.setNombre(nombre);
                diasListSelect.add(dias);
            } else {
                for (int i = 0; i < diasListSelect.size(); i++) {
                    if (codigo.equals(diasListSelect.get(i).getIdDias())) {
                        diasListSelect.remove(i);
                    }
                }
            }
        }
    }

    private void agregarDiasEntrega(String dia, String nombre, boolean isChecked) {
        if (!frecuenciaSelected.isEmpty() | frecuenciaSelected != null) {
            if (isChecked) {
                DiasSemana dias = new DiasSemana();
                dias.setIdDias(dia);
                dias.setNombre(nombre);
                entregaListSelect.add(dias);
            } else {
                for (int i = 0; i < entregaListSelect.size(); i++) {
                    if (dia.equals(entregaListSelect.get(i).getIdDias())) {
                        entregaListSelect.remove(i);
                    }
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        imagePresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_guardar_cliente:

                if (!validateCampos())
                    return;

                JSONGenerator.getNewInstance(getActivity())
                        .requireInternet(true)
                        .put("id_perfil", loginPref.getIdProfile())
                        .put("id_usuario", loginPref.getUid())
                        .put("latitud", mMap.getCameraPosition().target.latitude)
                        .put("longitud", mMap.getCameraPosition().target.longitude)
                        .put("nombre_local", edtNombreLocal)
                        .put("encargado_local", edtNombreCliente)
                        .put("telefono_local", edt_telefono_local)
                        .put("email_local", edt_correo_local)
                        .put("direccion_local", txt_direccion_mapa)
                        .put("zona", zonaSelected)
                        .put("tipo_comprobante", tipoComprobante)
                        .put("frecuencia_visita", frecuenciaSelected)
                        .put("hora_inicio", horaSeleccionadaInicial)
                        .put("hora_fin", horaSeleccionadaFinal)
                        .put("tipo_pago", 1)
                        .put("medio_pago", medioPagoSelected)
                        .put("tipo_documento", tipoDocumento)
                        .put("numero_documento", edt_ruc_empresa)
                        .put("tipo_documento_nombre", tipoDocumento.equals(Constants.RUC) ? "RUC" : "DNI")
                        .put("telefono_empresa", edt_telefono_empresa)
                        .put("nombre_empresa", edt_razon_social_empresa)
                        .put("direccion_empresa", edt_direccion_empresa)
                        .put("correo_empresa", edt_correo_empresa)
                        .put("lst_visita", diasListSelect)
                        .put("lst_entrega", entregaListSelect)

                        .operate(jsonObject -> {
                            OneTimer.subscribe(this, btn_guardar_cliente);
                            presenter.doAgregarCliente(jsonObject);
                        });
                break;
            case R.id.rb_ruc:
                if (documentoSeleccionado != 6)
                    clearDNI();

                documentoSeleccionado = 6;
                edt_ruc_empresa.setHint(R.string.ruc);
                edt_ruc_empresa.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});

                tipoComprobante = Constants.FACTURA;
                tipoDocumento = Constants.RUC;
                break;
            case R.id.rb_dni:
                if (documentoSeleccionado != 1)
                    clearDNI();

                edt_ruc_empresa.setHint(R.string.dni);
                edt_ruc_empresa.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8)});
                documentoSeleccionado = 1;

                tipoComprobante = Constants.BOLETA;
                tipoDocumento = Constants.DNI;
                break;
            case R.id.img_buscar:

                if (edt_ruc_empresa.getText().toString().isEmpty()) {
                    edt_ruc_empresa.setError("Complete campo");
                    return;
                }

                if (tipoDocumento.equals(Constants.RUC)) {
                    /*JSONGenerator.getNewInstance(getActivity())
                            .requireInternet(true)
                            .put("tipo_doc", Constants.RUC)
                            .put("numero", edt_ruc_empresa)
                            .put("id_fv", new LoginPref(getContext()).getUid())
                            .operate(jsonObject -> {

                               // getPresenter().doConsultarDocumento(jsonObject);
                            });*/
                    getPresenter().doConsultarDocumentoRuc(Constants.TOKEN, edt_ruc_empresa.getText().toString());
                } else if (tipoDocumento.equals(Constants.DNI)) {
                    getPresenter().doConsultarDocumentoDni(Constants.TOKEN, edt_ruc_empresa.getText().toString());
                }
                break;
            case R.id.ll_horario_inicial:
                obtenerHoraInicial();
                break;
            case R.id.ll_horario_final:
                obtenerHoraFinal();
                break;
            case R.id.img_agregar:

                selectPhotoDialog.show(getChildFragmentManager(), SelectPhotoDialog.class.getSimpleName());

                break;
        }
    }

    private void obtenerHoraInicial() {
        TimePickerDialog recogerHora = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada = (hourOfDay < 10) ? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10) ? String.valueOf(CERO + minute) : String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if (hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }

                horaSeleccionadaInicial = horaFormateada;

                //Muestro la hora con el formato deseado
//                edt_horario_inicial.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
                edt_horario_inicial.setText(horaFormateada + DOS_PUNTOS + "00" + " " + AM_PM);
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }

    private void obtenerHoraFinal() {
        TimePickerDialog recogerHora = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada = (hourOfDay < 10) ? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10) ? String.valueOf(CERO + minute) : String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if (hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }

                horaSeleccionadaFinal = horaFormateada;

                //Muestro la hora con el formato deseado
//                edt_horario_final.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
                edt_horario_final.setText(horaFormateada + DOS_PUNTOS + "00" + " " + AM_PM);
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }


    private void clearDNI() {
        edt_ruc_empresa.setText("");
        edt_telefono_empresa.setText("");
        edt_razon_social_empresa.setText("");
        edt_direccion_empresa.setText("");
        edt_correo_empresa.setText("");
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            // ******************VISITA****************** //
            case R.id.chk_lun:
                agregarDiasVisita("2", "Lunes", isChecked);
                validateCheckVisita();
                break;
            case R.id.chk_mar:
                agregarDiasVisita("3", "Martes", isChecked);
                validateCheckVisita();
                break;
            case R.id.chk_mie:
                agregarDiasVisita("4", "Miercoles", isChecked);
                validateCheckVisita();
                break;
            case R.id.chk_jue:
                agregarDiasVisita("5", "Jueves", isChecked);
                validateCheckVisita();
                break;
            case R.id.chk_vie:
                agregarDiasVisita("6", "Viernes", isChecked);
                validateCheckVisita();
                break;
            case R.id.chk_sab:
                agregarDiasVisita("7", "Sabado", isChecked);
                validateCheckVisita();
                break;
            case R.id.chk_dom:
                agregarDiasVisita("1", "Domingo", isChecked);
                validateCheckVisita();
                break;
            // ******************VISITA****************** //
            // ******************ENTREGA****************** //
            case R.id.chk_lunE:
                agregarDiasEntrega("2", "Lunes", isChecked);
                validateCheckEntrega();
                break;
            case R.id.chk_marE:
                agregarDiasEntrega("3", "Martes", isChecked);
                validateCheckEntrega();
                break;
            case R.id.chk_mieE:
                agregarDiasEntrega("4", "Miercoles", isChecked);
                validateCheckEntrega();
                break;
            case R.id.chk_jueE:
                agregarDiasEntrega("5", "Jueves", isChecked);
                validateCheckEntrega();
                break;
            case R.id.chk_vieE:
                agregarDiasEntrega("6", "Viernes", isChecked);
                validateCheckEntrega();
                break;
            case R.id.chk_sabE:
                agregarDiasEntrega("7", "Sabado", isChecked);
                validateCheckEntrega();
                break;
            case R.id.chk_domE:
                agregarDiasEntrega("1", "Domingo", isChecked);
                validateCheckEntrega();
                break;
        }
    }

    @Override
    public void onCameraIdle() {
        Log.i(LOG_TAG, "onCameraIdle");
//        Log.i(LOG_TAG,"DIRECCION_ACTUAL: "+mMap.getCameraPosition().target.latitude+","+mMap.getCameraPosition().target.longitude);
        setLocation(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);

    }

    public void setLocation(double latitud, double longitud) {
        //Obtener la direccion de la calle a partir de la latitud y la longitud
        if (latitud != 0.0 && longitud != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(latitud, longitud, 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);
//                    Log.i(LOG_TAG,"DIRECCION_COORDENADAS: "+DirCalle.getAddressLine(0));
                    txt_direccion_mapa.setText(DirCalle.getAddressLine(0));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        this.mMap.setOnCameraIdleListener(this);

        mMap.getUiSettings().setCompassEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        try {

            //https://developers.google.com/maps/documentation/android-sdk/styling

            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.mapstyle));

            if (!success) {
                Log.e(LOG_TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(LOG_TAG, "Can't find style. Error: ", e);
        }

        this.mMap.setOnMapLoadedCallback(() -> {
            // Para inclinar el mapa
            //.tilt(67.5f).bearing(314)
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(appPref.getLatitude()), Double.parseDouble(appPref.getLongitude()))).zoom(14).build();
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.getUiSettings().setScrollGesturesEnabled(false);
            mMap.getUiSettings().setRotateGesturesEnabled(false);
            mMap.getUiSettings().setZoomGesturesEnabled(false);
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
        }
        mMap.setMyLocationEnabled(true);

    }

    @Override
    public void onAgregarClienteSuccess(StatusMsgResponse response) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), response.getMsg(), Toast.LENGTH_SHORT).show();

        if (imgFile != null) {
            JSONGenerator.getNewInstance(getActivity())
                    .requireInternet(true)
                    .put("id_perfil", new LoginPref(getContext()).getIdProfile())
                    .put("id_usuario", new LoginPref(getContext()).getUid())
                    .put("id_cliente", loginPref.getUid())
                    .operate(jsonObject -> {
                        presenter.doAgregarImagen(jsonObject,imgFile);
                    });
            return;
        }

        EventBus.getDefault().post(new Message<>(Constants.OP_NOTIFY_LIST_CLIENTE,""));
        getActivity().onBackPressed();
        OneTimer.unsubscribe(this, btn_guardar_cliente);

    }

    @Override
    public void onAgregarClienteFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_guardar_cliente);
    }

    @Override
    public void onAgregarImagenSuccess(StatusMsgResponse response) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_guardar_cliente);
    }

    @Override
    public void onAgregarImagenFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_guardar_cliente);
    }

    @Override
    public void onConsultarDocumentoSuccess(MigoDocumentoResponse response) {
        if (getActivity() == null) return;
        if (response.getNombreDni() != null) {
            edt_razon_social_empresa.setText(response.getNombreDni());
        }
        if (response.getRazonSocial() != null) {
            edt_razon_social_empresa.setText(response.getRazonSocial());
        }

        edt_direccion_empresa.setText(response.getDireccion());

    }

    @Override
    public void onConsultarDocumentoFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTypeSelected(SelectPhotoDialog.ImageProvider imageProvider) {
        switch (imageProvider) {

            case GALLERY:
                imagePresenter.doGetImageGallery();
                break;

            case CAMERA:
                imagePresenter.doGetImageCamera();
                break;
        }
    }

    @Override
    public void onImageLoading(boolean loading, String message) {

    }

    @Override
    public void onImageSuccess(File file) {
        imgFile = file;
    }

    @Override
    public void onImageViewSuccess(Bitmap bitmap) {
        Glide.with(getContext())
                .load(bitmap)
                .centerCrop()
                .into(img_agregar);
    }

    @Override
    public void onImageFail(String message) {

    }

    private class DiasSemana {
        public String idDias;
        public String nombre;

        public String getIdDias() {
            return idDias;
        }

        public void setIdDias(String idDias) {
            this.idDias = idDias;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }
    }

}
