package com.jdl.distribution.ui.fragment.productos_frecuentes.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

public class RegistrarPedidoResponse implements IStringNotNull<RegistrarPedidoResponse>, StatusConverter {

    @StringNotNull
    @Expose
    @SerializedName("id_pedido")
    private String idPed;
//    @StringNotNull
//    @Expose
//    @SerializedName("motivo")
//    private String error;
    @Expose
    @SerializedName("status")
    private int status;
    @StringNotNull
    @Expose
    @SerializedName("msg")
    private String msg;

    public String getIdPed() {
        return idPed;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "RegistrarPedidoResponse{" +
                "idPed='" + idPed + '\'' +
                ", status=" + status +
                ", msg='" + msg + '\'' +
                '}';
    }

    @Override
    public RegistrarPedidoResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }
}
