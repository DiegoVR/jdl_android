package com.jdl.distribution.ui.fragment.historial_despacho.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.ui.fragment.historial_despacho.model.HistorialDespachoResponse;

public interface EntregaHistorialView extends MvpView {
    void showLoadingDialog(boolean status);

    void onEntregaSuccess(HistorialDespachoResponse response);
    void onEntregaFail(String mensaje);

    void onEmitirComprobanteSuccess(EmitirComprobanteResponse response);
    void onEmitirComprobanteSFail(String mensaje);
}
