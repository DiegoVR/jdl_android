package com.jdl.distribution.ui.fragment.mapa.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.model.ClienteResponse;
import com.jdl.distribution.ui.fragment.clientes.model.ClienteService;
import com.jdl.distribution.ui.fragment.clientes.presenter.ClientePresenter;
import com.jdl.distribution.ui.fragment.mapa.view.MapaView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapaPresenter extends MvpBasePresenter<MapaView> {

    private static final String LOG_TAG = "[" + ClientePresenter.class.getSimpleName() + "]";
    private final Context context;

    public MapaPresenter(Context context) {
        this.context = context;
    }

    public void doClientsListMap(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        ifViewAttached(view -> view.showLoadingDialog(true));


        ClienteService service = RetrofitClient.getRetrofitInstance().create(ClienteService.class);
        Call<ClienteResponse> call = service.doListarClientes(jsonObject.toString());


        call.enqueue(new Callback<ClienteResponse>() {
            @Override
            public void onResponse(Call<ClienteResponse> call, Response<ClienteResponse> response) {
                ClienteResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> view.showLoadingDialog(false));
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onListarClientesMapaFail("clientListResponse==null");
                        } else {
                            view.onListarClientesMapaFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onListarClientesMapaSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onListarClientesMapaFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<ClienteResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.showLoadingDialog(false);
                        view.onListarClientesMapaFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.showLoadingDialog(false);
                        view.onListarClientesMapaFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

}
