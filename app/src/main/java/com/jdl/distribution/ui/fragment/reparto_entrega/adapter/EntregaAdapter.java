package com.jdl.distribution.ui.fragment.reparto_entrega.adapter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.utils.Constants;
import com.developer.appsupport.utils.MyOutlineProvider;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.visita.model.EntregaTotalResponse;
import com.jdl.distribution.utils.adapter.AdapterBinder;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EntregaAdapter  extends RecyclerView.Adapter<EntregaAdapter.ViewHolder> {

    //    public static String isSelectedAll = null;
    Context context;
    List<EntregaTotalResponse.Entrega> listEntrega;
    //EntregaListener listener;

    public EntregaAdapter(Context context, List<EntregaTotalResponse.Entrega> listEntrega/*EntregaListener listener*/) {
        this.context = context;
        this.listEntrega = listEntrega;
        //this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_entrega,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EntregaTotalResponse.Entrega entrega = listEntrega.get(position);
        holder.bindTo(entrega);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

   /* public void setNuevaCantidad(int position, String cantidad,boolean checked,String motivo){
        listEntrega.get(position).setNuevaCantidad(cantidad);
        listEntrega.get(position).setCkecked(checked);
        listEntrega.get(position).setMotivo(motivo);

        notifyDataSetChanged();
    }*/

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listEntrega.size();
    }

    public void selectAll(){
//        isSelectedAll="1";
//        notifyDataSetChanged();

       /* for (EntregaTotalResponse.Entrega entrega : listEntrega) {
            entrega.setCkecked(true);
            entrega.setNuevaCantidad(entrega.getCantidad());
            entrega.setMotivo(entrega.getMotivo());
        }*/
        notifyDataSetChanged();

    }
    public void unselectall(){
//        isSelectedAll="0";
//        notifyDataSetChanged();
       /* for (EntregaResponse.Entrega entrega : listEntrega) {
            entrega.setCkecked(false);
            entrega.setNuevaCantidad("0");
            entrega.setMotivo("");
        }*/
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterBinder<EntregaTotalResponse.Entrega>, CheckBox.OnCheckedChangeListener,View.OnClickListener {
        @BindView(R.id.txt_cantidad_entrega)
        TextView txt_cantidad_entrega;
        @BindView(R.id.txt_codigo_entrega)
        TextView txt_codigo_entrega;
        @BindView(R.id.txt_nombre_entrega)
        TextView txt_nombre_entrega;
        @BindView(R.id.chb_add)
        CheckBox chb_add;
        @BindView(R.id.body)
        LinearLayout body;
        @BindView(R.id.img_config)
        ImageCardView img_config;
        @BindView(R.id.ll_motivo)
        LinearLayout ll_motivo;
        @BindView(R.id.txt_motivo_entrega)
        TextView txt_motivo_entrega;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                body.setOutlineProvider(new MyOutlineProvider((int) Constants.convertDpiToPx(context, 10), 0.25f));
            }

            chb_add.setOnCheckedChangeListener(this);
            img_config.setOnClickListener(this);
        }

        @Override
        public void bindTo(@Nullable EntregaTotalResponse.Entrega entrega) {
            if (entrega!=null){
                txt_nombre_entrega.setText(entrega.getNomPro());
                txt_codigo_entrega.setText(entrega.getCodPro());
                //Log.d("CANTIDAD",entrega.getNuevaCantidad());
                txt_cantidad_entrega.setText(entrega.getCantidad());
                //chb_add.setChecked(entrega.isCkecked());
                //txt_motivo_entrega.setText(entrega.getMotivo());

                /*if (entrega.getMotivo().isEmpty()){
                    ll_motivo.setVisibility(View.GONE);
                }else{
                    ll_motivo.setVisibility(View.VISIBLE);
                }*/
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()){
                case R.id.chb_add:
//                    if (buttonView.isPressed()){
                    if(getAdapterPosition()!=-1) {
                        //listener.onItemClickAddProducto(getAdapterPosition(),isChecked, listEntrega.get(getAdapterPosition()));
                    }
//                    }
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.img_config:
//                    isSelectedAll = null;
                    if(getAdapterPosition()!=-1) {
//                        Log.d("CANTIDAD",listEntrega.get(getAdapterPosition()).getNuevaCantidad());
                        //listener.onItemClickConfig(getAdapterPosition(),listEntrega.get(getAdapterPosition()));
                    }
                    break;
            }
        }
    }

/*
    public interface EntregaListener {
        void onItemClickAddProducto(int position,boolean isChecked, EntregaTotalResponse.Entrega entrega);
        void onItemClickConfig(int position,EntregaTotalResponse.Entrega entrega);
    }
*/
}
