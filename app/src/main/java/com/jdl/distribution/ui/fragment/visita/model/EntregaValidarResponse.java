package com.jdl.distribution.ui.fragment.visita.model;


import android.annotation.SuppressLint;
import android.os.Parcelable;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

@Parcel(Parcel.Serialization.BEAN)
public class EntregaValidarResponse implements IStringNotNull<EntregaValidarResponse>, StatusConverter {

    @StringNotNull
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("listAsig")
    private List<Factura> listFactura;
    @Expose
    @SerializedName("count")
    private int count;
    @Expose
    @SerializedName("status")
    private int status;

    public String getMsg() {
        return msg;
    }

    public int getCount() {
        return count;
    }

    public List<Factura> getListFactura() {
        return listFactura;
    }

    public void setListFactura(List<Factura> listFactura) {
        this.listFactura = listFactura;
    }

    public int getStatus() {
        return status;
    }

    @SuppressLint("ParcelCreator")
    @Parcel(Parcel.Serialization.BEAN)
    public static class Factura implements IStringNotNull<Factura>, Parcelable {

        @Expose
        @SerializedName("id_asig")
        private String idAsignacion;
        @Expose
        @SerializedName("id_fv_rep")
        private String idReparto;

        public String getIdAsignacion() {
            return idAsignacion;
        }

        public String getIdReparto() {
            return idReparto;
        }

        @Override
        public String toString() {
            return "Factura{" +
                    "idAsignacion='" + idAsignacion + '\'' +
                    ", idReoarto='" + idReparto + '\'' +
                    '}';
        }

        @Override
        public Factura getWithStringNotNull() {
            return getWithStringNotNull(this);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(android.os.Parcel dest, int flags) {

        }
    }

    @Override
    public EntregaValidarResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }
}
