package com.jdl.distribution.ui.fragment.mapa.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.mvp.model.ClienteResponse;

public interface MapaView extends MvpView {
    void showLoadingDialog(boolean status);
    void onListarClientesMapaSuccess(ClienteResponse response);
    void onListarClientesMapaFail(String mensaje);
}
