package com.jdl.distribution.ui.fragment.reparto_entrega.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.ui.fragment.reparto_entrega.model.EntregaResponse;

public interface EntregaView extends MvpView {
    void onEntregarSuccess(EntregaResponse response);
    void onEntregarFail(String mensaje);

    void onEmitirComprobanteSuccess(EmitirComprobanteResponse response);
    void onEmitirComprobanteSFail(String mensaje);
}
