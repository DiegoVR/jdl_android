package com.jdl.distribution.ui.fragment.incidencias.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.ViewTarget;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.utils.Constants;
import com.developer.appsupport.utils.MyOutlineProvider;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.incidencias.model.IncidenciasResponse;
import com.jdl.distribution.utils.adapter.AdapterBinder;

import java.util.ArrayList;
import java.util.Base64;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IncidenciasAdapter extends RecyclerView.Adapter<IncidenciasAdapter.ViewHolder> {

    Context context;
    ArrayList<IncidenciasResponse.ImagenIncidencia> listIncidencia;
    IncidenciaListener listener;

    public IncidenciasAdapter(Context context, ArrayList<IncidenciasResponse.ImagenIncidencia> listIncidencia, IncidenciaListener listener) {
        this.context = context;
        this.listIncidencia = listIncidencia;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_incidencias_gallery,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        IncidenciasResponse.ImagenIncidencia model = listIncidencia.get(position);
        holder.bindTo(model);
    }

    @Override
    public int getItemCount() {
        return listIncidencia==null ? 0 : listIncidencia.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterBinder<IncidenciasResponse.ImagenIncidencia>, View.OnClickListener {

        @BindView(R.id.cv_incidencias)
        CardView cv_incidencias;
        @BindView(R.id.img_incidencias)
        ImageView img_incidencias;
        @BindView(R.id.img_delete)
        ImageCardView img_delete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cv_incidencias.setOutlineProvider(new MyOutlineProvider((int) Constants.convertDpiToPx(context, 10), 0.25f));
            }
            img_delete.setOnClickListener(this);
            cv_incidencias.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.img_delete:
                    listener.onItemClickDeleteListener(listIncidencia.get(getAdapterPosition()));
                    break;
                case R.id.cv_incidencias:
                    listener.onItemClick(listIncidencia.get(getAdapterPosition()));
                    break;
            }
        }

        @Override
        public void bindTo(@Nullable IncidenciasResponse.ImagenIncidencia imagenIncidencia) {
            if (imagenIncidencia!=null) {

                byte[] base64Decoded = android.util.Base64.decode(imagenIncidencia.getImage(), android.util.Base64.DEFAULT);

                Glide.with(context)
                        .load(base64Decoded)
                        .centerCrop()
                        .into(img_incidencias);
            }
        }
    }

    public interface IncidenciaListener {
        void onItemClickDeleteListener(IncidenciasResponse.ImagenIncidencia listIncidencia);
        void onItemClick(IncidenciasResponse.ImagenIncidencia listIncidencia);
    }

}
