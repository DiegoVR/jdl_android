package com.jdl.distribution.ui.fragment.agregar_cliente.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MigoDocumentoResponse {

    @StringNotNull
    @Expose
    @SerializedName("nombre_o_razon_social")
    private String razonSocial;
    @StringNotNull
    @Expose
    @SerializedName("nombre")
    private String nombreDni;
    @StringNotNull
    @Expose
    @SerializedName("direccion")
    private String direccion;
    @Expose
    @SerializedName("success")
    private boolean status;

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getNombreDni() {
        return nombreDni;
    }

    public String getDireccion() {
        return direccion;
    }

    public boolean getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "MigoDocumentoResponse{" +
                "razonSocial='" + razonSocial + '\'' +
                ", nombreDni='" + nombreDni + '\'' +
                ", direccion='" + direccion + '\'' +
                ", status=" + status +
                '}';
    }

}
