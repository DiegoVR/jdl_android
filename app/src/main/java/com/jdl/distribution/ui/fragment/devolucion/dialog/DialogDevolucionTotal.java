package com.jdl.distribution.ui.fragment.devolucion.dialog;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.jdl.distribution.R;

public class DialogDevolucionTotal extends BaseDialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_devolucion_total, container, false);

        return view;
    }

}
