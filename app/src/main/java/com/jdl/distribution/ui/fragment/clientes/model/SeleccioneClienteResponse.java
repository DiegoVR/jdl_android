package com.jdl.distribution.ui.fragment.clientes.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;
import com.jdl.distribution.mvp.model.Cliente;

public class SeleccioneClienteResponse implements IStringNotNull<SeleccioneClienteResponse>, StatusConverter {

    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("cliente")
    private Cliente cliente;
    @Expose
    @SerializedName("status")
    private int status;

    public String getMsg() {
        return msg;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public SeleccioneClienteResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    @Override
    public String toString() {
        return "SeleccioneClienteResponse{" +
                "msg='" + msg + '\'' +
                ", cliente=" + cliente +
                ", status=" + status +
                '}';
    }
}
