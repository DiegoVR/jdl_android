package com.jdl.distribution.ui.fragment.clientes.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.jdl.distribution.R;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.utils.adapter.AdapterBinder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClienteAdapter extends RecyclerView.Adapter<ClienteAdapter.ViewHolder> {

    Context context;
    ArrayList<Cliente> listClientes;
    ClienteListener listener;

    public ClienteAdapter(Context context, ArrayList<Cliente> listClientes, ClienteListener listener) {
        this.context = context;
        this.listClientes = listClientes;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cliente, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(listClientes.get(position));
    }

    @Override
    public int getItemCount() {
        return listClientes == null ? 0 : listClientes.size();
    }

    public void filterList(ArrayList<Cliente> listFiltroPedido) {
        listClientes = listFiltroPedido;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterBinder<Cliente>, View.OnClickListener {
        @BindView(R.id.txt_nombre_cliente_item)
        TextView txt_nombre_cliente_item;
        @BindView(R.id.txt_rs_cliente_item)
        TextView txt_rs_cliente_item;
        @BindView(R.id.txt_ruc_cliente_item)
        TextView txt_ruc_cliente_item;
        @BindView(R.id.txt_encargado_cliente_item)
        TextView txt_encargado_cliente_item;
        @BindView(R.id.txt_direccion_cliente_item)
        TextView txt_direccion_cliente_item;
        @BindView(R.id.cv_list_clients)
        CardView cv_list_clients;
        @BindView(R.id.cv_visita)
        CardView cv_visita;
        @BindView(R.id.cv_editar)
        CardView cv_editar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            cv_editar.setOnClickListener(this);
            cv_visita.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.cv_editar:
                    if (getAdapterPosition() != -1) {
                        listener.onItemClickListener(listClientes.get(getAdapterPosition()), getAdapterPosition());
                    }
                    break;
                case R.id.cv_visita:
                    if (getAdapterPosition() != -1) {
                        listener.onItemClickInitVisitListener(listClientes.get(getAdapterPosition()), getAdapterPosition());
                    }
                    break;
            }
        }

        @Override
        public void bindTo(@Nullable Cliente clientes) {
            if (clientes != null) {
                txt_nombre_cliente_item.setText(clientes.getNombre());
                txt_rs_cliente_item.setText(clientes.getRazonSocial());
                txt_ruc_cliente_item.setText(clientes.getRuc());
                txt_encargado_cliente_item.setText(clientes.getContacto());
                txt_direccion_cliente_item.setText(clientes.getDireccion());
            }
        }
    }

    public interface ClienteListener {
        void onItemClickListener(Cliente clientes, int i);

        void onItemClickInitVisitListener(Cliente clientes, int position);
    }

}
