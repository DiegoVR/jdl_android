package com.jdl.distribution.ui.fragment.devolucion.devolucion_parcial;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.fragmentargs.bundler.ParcelerArgsBundler;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.ui.fragment.devolucion.devolucion_parcial.adapter.ListaEditarDevolucionAdapter;
import com.jdl.distribution.ui.fragment.devolucion.devolucion_parcial.model.EditarListaDevolucionResponse;
import com.jdl.distribution.ui.fragment.devolucion.dialog.DialogDevolucionParcial;
import com.jdl.distribution.ui.fragment.devolucion.lista_devolucion.model.ListaDevolucionResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@FragmentWithArgs
public class DevolucionParcialFragment extends BaseFragment implements View.OnClickListener {

    private boolean checkFalse = true;

    @BindView(R.id.rv_edit_devolucion)
    RecyclerView rv_edit_devolucion;
    @BindView(R.id.btn_aceptar)
    ProgressButton btn_aceptar;
    @BindView(R.id.img_back)
    ImageCardView img_back;
    @BindView(R.id.txt_nombre)
    TextView txt_nombre;

    private DialogFragment dialogDevolucionParcial;

    @Arg(bundler = ParcelerArgsBundler.class)
    List<EditarListaDevolucionResponse.Detalle> listDetalle;
    @Arg(bundler = ParcelerArgsBundler.class)
    Cliente cliente;
    @Arg(bundler = ParcelerArgsBundler.class)
    ListaDevolucionResponse.Documento documento;
    @Arg
    String idVisita;

    private ArrayList<EditarListaDevolucionResponse.Detalle> listDetalleAdd;
    private ListaEditarDevolucionAdapter adapter;

    ArrayList<Producto> listSeleccionadosFinal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_devolucion_parcial, container, false);
        FragmentArgs.inject(this);
        ButterKnife.bind(this,view);
        init(savedInstanceState);
        return view;
    }

    private void init(Bundle savedInstanceState) {

        listDetalleAdd = new ArrayList<>();
        listDetalleAdd.clear();

        initDialog(savedInstanceState);
        setData();
        setListener();
        setAdapter();

    }

    private void setListener() {
        btn_aceptar.setOnClickListener(this);
    }

    private void initDialog(Bundle savedInstanceState) {
        if (savedInstanceState!=null){

            dialogDevolucionParcial = (DialogDevolucionParcial)getChildFragmentManager().findFragmentByTag(DialogDevolucionParcial.class.getSimpleName());

            if (dialogDevolucionParcial==null)
                dialogDevolucionParcial = new DialogDevolucionParcial();

        }else {

            dialogDevolucionParcial = new DialogDevolucionParcial();

        }
    }

    private void setData() {
        txt_nombre.setText(documento.getNombre());
    }

    private void setAdapter() {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        rv_edit_devolucion.setLayoutManager(layoutManager);
        rv_edit_devolucion.setItemAnimator(new DefaultItemAnimator());

        adapter = new ListaEditarDevolucionAdapter(getContext(), listDetalle, (position, isChecked, detalle) -> {
            if (isChecked) {

                listDetalleAdd.add(detalle);

                if (!detalle.isCkecked())
                    adapter.setEnableCantidad(position, true, detalle.getCantidad());

//                    if (listDetalle.size() == listDetalleAdd.size()) {
//                        chk_todos.setChecked(true);
//                    }

//                    Log.d("EntregaReparto",listDetalleAdd.size()+" - "+"Nombre: "+detalle.getNombre()+" - AGREGADO");
            } else {
                checkFalse = false;

                for (int i = 0; i < listDetalleAdd.size(); i++) {
                    if (detalle.getIdPro().equals(listDetalleAdd.get(i).getIdPro())) {
                        listDetalleAdd.remove(i);
                    }
                }

                if (detalle.isCkecked())
                    adapter.setEnableCantidad(position, false, "");

//                    chk_todos.setChecked(false);
//                    Log.d("EntregaReparto",listDetalleAdd.size()+" - "+"Nombre: "+detalle.getNombre()+" - ELIMINADO");
            }
        });

        rv_edit_devolucion.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_aceptar:

                listSeleccionadosFinal = new ArrayList<>();
                listSeleccionadosFinal.clear();

                int count = 0;

                if (listDetalleAdd.size() == 0) {
                    Toast.makeText(getContext(), "Seleccione al menos un item para devolución", Toast.LENGTH_SHORT).show();
                    return;
                }

                for (EditarListaDevolucionResponse.Detalle entrega : listDetalleAdd) {
                    if (Integer.parseInt(entrega.getNuevaCantidad().isEmpty() ? "0" : entrega.getNuevaCantidad()) > 0) {
                        listSeleccionadosFinal.add(new Producto(entrega.getIdPro(), entrega.getNuevaCantidad()));
                    } else {
                        count++;
                    }
                }

                if (count > 0) {
                    Toast.makeText(getContext(), "Debe asignar cantidad a sus items", Toast.LENGTH_SHORT).show();
                    return;
                }

                dialogDevolucionParcial.showNow(getChildFragmentManager(),DialogDevolucionParcial.class.getSimpleName());

                break;
        }
    }


    public static class Producto implements Parcelable {
        private String idPro;
        private String cant;

        public Producto(String idPro, String cant) {
            this.idPro = idPro;
            this.cant = cant;
        }

        protected Producto(Parcel in) {
            idPro = in.readString();
            cant = in.readString();
        }

        public static final Creator<Producto> CREATOR = new Creator<Producto>() {
            @Override
            public Producto createFromParcel(Parcel in) {
                return new Producto(in);
            }

            @Override
            public Producto[] newArray(int size) {
                return new Producto[size];
            }
        };

        public String getIdPro() {
            return idPro;
        }

        public void setIdPro(String idPro) {
            this.idPro = idPro;
        }

        public String getCantE() {
            return cant;
        }

        public void setCantE(String cantE) {
            this.cant = cantE;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(idPro);
            dest.writeString(cant);
        }
    }


}
