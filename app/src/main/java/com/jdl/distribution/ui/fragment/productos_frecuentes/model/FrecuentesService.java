package com.jdl.distribution.ui.fragment.productos_frecuentes.model;

import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface FrecuentesService {
    @POST(Constants.WS_REGISTRAR_PEDIDO)
    Call<RegistrarPedidoResponse> doRegistrarPedido(@Body String data);

    @POST(Constants.WS_EMITIR_COMPROBANTE)
    Call<EmitirComprobanteResponse> emitirComprobante(@Body String data);
}
