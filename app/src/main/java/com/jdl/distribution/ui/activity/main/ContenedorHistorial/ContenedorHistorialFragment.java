package com.jdl.distribution.ui.activity.main.ContenedorHistorial;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.developer.appsupport.mvp.model.Message;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.developer.appsupport.utils.CalendarUtil;
import com.google.android.material.tabs.TabLayout;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.adapter.adapter_viewpager.TableLayoutAdapter;
import com.jdl.distribution.ui.fragment.historial_despacho.HistorialDespachoFragment;
import com.jdl.distribution.ui.fragment.historial_eventos.HistorialEventoFragment;
import com.jdl.distribution.ui.fragment.historial_pedidos.HistorialPedidoFragment;
import com.jdl.distribution.utils.Constants;

import org.apache.commons.lang3.time.DateUtils;
import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContenedorHistorialFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.vp_historial)
    ViewPager vp_historial;
    @BindView(R.id.tablayoutHistorial)
    TabLayout tablayoutHistorial;

    private TableLayoutAdapter adapter;
    private List<Fragment> listFragment;
    private LoginPref loginPref;

    private String fecha = "";
    private Calendar c = Calendar.getInstance();
    private static final String CERO = "0";
    private int dia_actual = c.get(Calendar.DAY_OF_MONTH);
    private final int mes_actual = c.get(Calendar.MONTH);
    private final int anio_actual = c.get(Calendar.YEAR);

    TextView txt_dia_actual;
    TextView txt_mes_actual;
    TextView txt_año_actual;
    LinearLayout ll_fecha;
    ImageCardView img_left_day;
    ImageCardView img_right_day;
    LinearLayout ll_fecha_historial;

    Date today = new Date();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contenedor_historial, container, false);
        ButterKnife.bind(this,view);

        loginPref = new LoginPref(getContext());

        tablayoutHistorial.setTabGravity(TabLayout.GRAVITY_FILL);

        adapter = new TableLayoutAdapter(((AppCompatActivity) getContext()).getSupportFragmentManager());

        vp_historial.setAdapter(adapter);

        init();

        agregarFragment(adapter);

//        vp_historial.setOffscreenPageLimit(4);
        vp_historial.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayoutHistorial));
        tablayoutHistorial.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp_historial.setCurrentItem(tab.getPosition());

//                Toast.makeText(getContext(), "Tab: "+ tab.getPosition()+ " ViewPager: "+vp_historial.getCurrentItem(), Toast.LENGTH_SHORT).show();

                listener(tablayoutHistorial);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        if (tablayoutHistorial.getTabCount() <= 4 ){
            tablayoutHistorial.setTabMode(TabLayout.MODE_FIXED);
        } else {
            tablayoutHistorial.setTabMode(TabLayout.MODE_SCROLLABLE);
        }


        if (vp_historial.getCurrentItem() == 0){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d("LISTENER_ASD",adapter.getmFragmentList().get(vp_historial.getCurrentItem()).getClass().getSimpleName());

//                    new AppPref(getContext()).setFragment(adapter.getmFragmentList().get(vp_historial.getCurrentItem()).getClass().getSimpleName()+"anulado");

                    EventBus.getDefault().post(new Message<>(adapter.getmFragmentList().get(vp_historial.getCurrentItem()).getClass().getSimpleName(),fecha));
                }
            },500);
        }


        return view;
    }

    private void init(){
        instanceMain();
        setListener();
    }

    private void setListener() {
        ll_fecha.setOnClickListener(this);
        img_left_day.setOnClickListener(this);
        img_right_day.setOnClickListener(this);

        MyDate(today);
    }

    private void MyDate(Date date){
        SimpleDateFormat formatFecha = new SimpleDateFormat("dd-MM-yyyy");

        Date dateHoy = new Date();
        String hoy = formatFecha.format(dateHoy);

        String fecha_seleccionada = formatFecha.format(date);

        Log.d("HOY",hoy+"  SELECCIONADA: "+fecha_seleccionada);

        if (fecha_seleccionada.equals(hoy)){
            img_right_day.setVisibility(View.GONE);
        }

        toddMMyy(date);
    }

    public void toddMMyy(Date day){
//        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
//        String date = formatter.format(day);

        String añoFormatter="yyyy";
        SimpleDateFormat añoFormat = new SimpleDateFormat(añoFormatter);
        int año = Integer.parseInt(añoFormat.format(day));

        String mesFormatter="MM";
        SimpleDateFormat mesFormat = new SimpleDateFormat(mesFormatter);
        int mes = Integer.parseInt(mesFormat.format(day));

        String dayFormatter="dd";
        SimpleDateFormat dayFormat = new SimpleDateFormat(dayFormatter);
        int dia = Integer.parseInt(dayFormat.format(day));

        //Formateo el día obtenido: antepone el 0 si son menores de 10
        String diaFormateado = (dia < 10) ? CERO + String.valueOf(dia) : String.valueOf(dia);
        //Formateo el mes obtenido: antepone el 0 si son menores de 10
        String mesFormateado = (mes < 10) ? CERO + String.valueOf(mes) : String.valueOf(mes);

        fecha = año+"-"+mesFormateado+"-"+diaFormateado;

        txt_dia_actual.setText(diaFormateado);
        txt_mes_actual.setText(CalendarUtil.nombreMes112Corto[mes]);
        txt_año_actual.setText(String.valueOf(año).substring(2));

        Log.d("out: ", fecha);

    }

    @Override
    public void onResume() {
//        if (!EventBus.getDefault().isRegistered(this)){
//            EventBus.getDefault().register(this);
//        }
        ll_fecha_historial.setVisibility(View.VISIBLE);
        for (int i =0 ; i < adapter.getmFragmentList().size(); i++){

            Log.d("REGISTER_EVENT",EventBus.getDefault().isRegistered(adapter.getmFragmentList().get(i))+" Fragment: "+adapter.getmFragmentList().get(i).getClass().getSimpleName());

            if (!EventBus.getDefault().isRegistered(adapter.getmFragmentList().get(i))){
                EventBus.getDefault().register(adapter.getmFragmentList().get(i));
            }
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        ll_fecha_historial.setVisibility(View.GONE);
//        EventBus.getDefault().unregister(this);
        for (int i =0 ; i < adapter.getmFragmentList().size(); i++){

            Log.d("REGISTER_EVENT",EventBus.getDefault().isRegistered(adapter.getmFragmentList().get(i))+" Fragment: "+adapter.getmFragmentList().get(i).getClass().getSimpleName());

            if (EventBus.getDefault().isRegistered(adapter.getmFragmentList().get(i))){
                EventBus.getDefault().unregister(adapter.getmFragmentList().get(i));
            }
        }
    }

    @Override
    public void onDestroy() {
//        EventBus.getDefault().unregister(this);
        ll_fecha_historial.setVisibility(View.GONE);
        for (int i =0 ; i < adapter.getmFragmentList().size(); i++){

            Log.d("REGISTER_EVENT",EventBus.getDefault().isRegistered(adapter.getmFragmentList().get(i))+" Fragment: "+adapter.getmFragmentList().get(i).getClass().getSimpleName());

            if (EventBus.getDefault().isRegistered(adapter.getmFragmentList().get(i))){
                EventBus.getDefault().unregister(adapter.getmFragmentList().get(i));
            }
        }
        super.onDestroy();
    }

    private void listener(TabLayout tab){
//        edt_search.setText("");

        if (adapter.getmFragmentList().get(tab.getSelectedTabPosition()).getClass().getSimpleName().equals(HistorialEventoFragment.class.getSimpleName())){
            Log.d("LISTENER",adapter.getmFragmentList().get(tab.getSelectedTabPosition()).getClass().getSimpleName());
//            new AppPref(getContext()).setFragment(adapter.getmFragmentList().get(vp_historial.getCurrentItem()).getClass().getSimpleName()+"anulado");
            EventBus.getDefault().post(new Message<>(adapter.getmFragmentList().get(tab.getSelectedTabPosition()).getClass().getSimpleName(),fecha));
            return;
        }
        if (adapter.getmFragmentList().get(tab.getSelectedTabPosition()).getClass().getSimpleName().equals(HistorialPedidoFragment.class.getSimpleName())){
            Log.d("LISTENER",adapter.getmFragmentList().get(tab.getSelectedTabPosition()).getClass().getSimpleName());
//            new AppPref(getContext()).setFragment(adapter.getmFragmentList().get(vp_historial.getCurrentItem()).getClass().getSimpleName()+"anulado");
            EventBus.getDefault().post(new Message<>(adapter.getmFragmentList().get(tab.getSelectedTabPosition()).getClass().getSimpleName(),fecha));
            return;
        }
        if (adapter.getmFragmentList().get(tab.getSelectedTabPosition()).getClass().getSimpleName().equals(HistorialDespachoFragment.class.getSimpleName())){
            Log.d("LISTENER",adapter.getmFragmentList().get(tab.getSelectedTabPosition()).getClass().getSimpleName());
//            new AppPref(getContext()).setFragment(adapter.getmFragmentList().get(vp_historial.getCurrentItem()).getClass().getSimpleName()+"anulado");
            EventBus.getDefault().post(new Message<>(adapter.getmFragmentList().get(tab.getSelectedTabPosition()).getClass().getSimpleName(),fecha));
            return;
        }
    }

    private void agregarFragment(TableLayoutAdapter adapter){
        listFragment = new ArrayList<>();
        listFragment.clear();

        if (loginPref.getIdProfile().equals(Constants.PROFILE_PRE_VENTA)){
            //tablayoutHistorial.addTab(tablayoutHistorial.newTab().setText("Eventos"));
            tablayoutHistorial.addTab(tablayoutHistorial.newTab().setText("Pedidos"));

            //listFragment.add(new HistorialEventoFragment());
            listFragment.add(new HistorialPedidoFragment());
            adapter.addFragment(listFragment);
            adapter.notifyDataSetChanged();
        }
//        if (loginPref.getIdProfile().equals(Constants.PROFILE_ADMINISTRADOR)){
//
//            tablayoutHistorial.addTab(tablayoutHistorial.newTab().setText("Eventos"));
//            tablayoutHistorial.addTab(tablayoutHistorial.newTab().setText("Pedidos"));
////            tablayoutHistorial.addTab(tablayoutHistorial.newTab().setText("Despacho"));
//
//            listFragment.add(new HistorialEventoFragment());
//            listFragment.add(new HistorialPedidoFragment());
////            listFragment.add(new DespachoHistorialFragment());
//            adapter.addFragment(listFragment);
//            adapter.notifyDataSetChanged();
//        }

        if (loginPref.getIdProfile().equals(Constants.PROFILE_REPARTO)){
            //tablayoutHistorial.addTab(tablayoutHistorial.newTab().setText("Eventos"));
            tablayoutHistorial.addTab(tablayoutHistorial.newTab().setText("Despacho"));

            //listFragment.add(new HistorialEventoFragment());
            listFragment.add(new HistorialDespachoFragment());
            adapter.addFragment(listFragment);
            adapter.notifyDataSetChanged();
        }

    }

    private void instanceMain() {
        txt_dia_actual = getActivity().findViewById(R.id.txt_dia_actual);
        txt_mes_actual = getActivity().findViewById(R.id.txt_mes_actual);
        txt_año_actual = getActivity().findViewById(R.id.txt_año_actual);
        ll_fecha = getActivity().findViewById(R.id.ll_fecha);
        img_left_day = getActivity().findViewById(R.id.img_left_day);
        img_right_day = getActivity().findViewById(R.id.img_right_day);
        ll_fecha_historial = getActivity().findViewById(R.id.ll_fecha_historial);
    }

    private void getDate() {

        if (getActivity() == null)
            return;

        DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(), (view, year, month, dayOfMonth) -> {

            //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
            final int mesActual = month + 1;

            //Formateo el día obtenido: antepone el 0 si son menores de 10
            String diaFormateado = (dayOfMonth < 10) ? CERO + String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
            //Formateo el mes obtenido: antepone el 0 si son menores de 10
            String mesFormateado = (mesActual < 10) ? CERO + String.valueOf(mesActual) : String.valueOf(mesActual);

            //enterActivityProfileDataViewModel.setDate(diaFormateado+"/"+mesFormateado+"/"+year);
            //enterActivityProfileDataViewModel.setDateVisible(diaFormateado+" "+ CalendarUtil.nombreMes011[month]+" "+year);

            fecha = year+"-"+mesFormateado+"-"+diaFormateado;

            txt_dia_actual.setText(diaFormateado);
            txt_mes_actual.setText(CalendarUtil.nombreMes011Corto[month]);
            txt_año_actual.setText(String.valueOf(year).substring(2));

            SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();

            try {
                date = formatFecha.parse(fecha);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                today = formatFecha.parse(fecha);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            MyDate(date);


            listener(tablayoutHistorial);

//            txt_fecha_actual.setText(diaFormateado + " de " + CalendarUtil.nombreMes011[month] + " del " + year);

//            loadData(fecha);

        }, anio_actual, mes_actual, dia_actual);

        datepickerdialog.show();

    }

    @Override
    public void onClick(View v) {
        Date date = new Date();
        SimpleDateFormat formatFecha = new SimpleDateFormat("dd-MM-yyyy");
        String hoy = formatFecha.format(date);
        String calendario = null;
        switch (v.getId()) {
            case R.id.ll_fecha:
                getDate();
                break;
            case R.id.img_left_day:

//                edt_search.setText("");

                Date decrement = DateUtils.addDays(today, -1);
                today = decrement;
                toddMMyy(decrement);
                calendario = formatFecha.format(today);
                if (calendario.equals(hoy)){
                    img_right_day.setVisibility(View.GONE);
                    listener(tablayoutHistorial);
                }else{
                    img_right_day.setVisibility(View.VISIBLE);
                    listener(tablayoutHistorial);
                }
                break;

            case R.id.img_right_day:
//                edt_search.setText("");
                Date increment = DateUtils.addDays(today, 1);
                today = increment;
                toddMMyy(increment);
                calendario = formatFecha.format(today);
                if (calendario.equals(hoy)){
                    img_right_day.setVisibility(View.GONE);
                    listener(tablayoutHistorial);
                }else{
                    img_right_day.setVisibility(View.VISIBLE);
                    listener(tablayoutHistorial);
                }

                break;
        }
    }
}
