package com.jdl.distribution.ui.fragment.clientes.model;

import com.jdl.distribution.mvp.model.ClienteResponse;
import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ClienteService {
    @POST(Constants.WS_CLIENTES)
    Call<ClienteResponse> doListarClientes(@Body String data);

    @POST(Constants.WS_SELECCIONA_CLIENTE)
    Call<SeleccioneClienteResponse> doSeleccionaCliente(@Body String data);

    @POST(Constants.WS_LISTAR_COMBO)
    Call<ComboFrecuenciaResponse> doListarComboFrecuencia(@Body String data);
}
