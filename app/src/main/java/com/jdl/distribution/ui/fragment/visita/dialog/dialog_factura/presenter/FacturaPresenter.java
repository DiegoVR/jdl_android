package com.jdl.distribution.ui.fragment.visita.dialog.dialog_factura.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.visita.dialog.dialog_factura.view.FacturaView;
import com.jdl.distribution.ui.fragment.visita.model.EntregaTotalResponse;
import com.jdl.distribution.ui.fragment.visita.model.MenuVisitaService;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FacturaPresenter extends MvpBasePresenter<FacturaView> {

    private static final String LOG_TAG = FacturaPresenter.class.getSimpleName();
    private final Context context;

    public FacturaPresenter(Context context) {
        this.context = context;
    }

    public void doEntregaTotalRequest(JSONObject jsonObject) {
        Log.d(LOG_TAG, "enviando: "+jsonObject.toString());

        MenuVisitaService service = RetrofitClient.getRetrofitInstance().create(MenuVisitaService.class);
        Call<EntregaTotalResponse> call = service.getEntregaTotal(jsonObject.toString());
        call.enqueue(new Callback<EntregaTotalResponse>() {
            @Override
            public void onResponse(Call<EntregaTotalResponse> call, Response<EntregaTotalResponse> response) {

                EntregaTotalResponse entregaResponse = response.body();

                if (entregaResponse==null){
                    Log.d(LOG_TAG, "doEntregaRequest==null");
                    if (Constants.DEBUG) {
                        ifViewAttached(view -> view.onEntregaTotalFail("doEntregaRequest==null"));
                    }else {
                        ifViewAttached(view -> view.onEntregaTotalFail(context.getResources().getString(R.string.error)));
                    }
                    return;
                }

                Log.d(LOG_TAG, "onResponse: "+entregaResponse.toString());

                switch (entregaResponse.getWithStringNotNull().getStatusEnum()){

                    case SUCCESS:

                        ifViewAttached(view -> view.onEntregaTotalSuccess(entregaResponse));

                        break;

                    case FAIL:

                        ifViewAttached(view -> view.onEntregaTotalFail(entregaResponse.getMsg()));

                        break;

                }

            }

            @Override
            public void onFailure(Call<EntregaTotalResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: "+t==null?"":t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG){
                        view.onEntregaTotalFail("onFailure: "+t==null?"":t.toString());
                    }else {
                        view.onEntregaTotalFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });
    }

}
