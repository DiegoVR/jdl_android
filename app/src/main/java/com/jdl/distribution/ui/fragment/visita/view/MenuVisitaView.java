package com.jdl.distribution.ui.fragment.visita.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.ui.fragment.clientes.model.ComboFrecuenciaResponse;
import com.jdl.distribution.ui.fragment.clientes.model.SeleccioneClienteResponse;
import com.jdl.distribution.ui.fragment.visita.model.EntregaTotalResponse;
import com.jdl.distribution.ui.fragment.visita.model.EntregaValidarResponse;
import com.jdl.distribution.ui.fragment.visita.model.EventoResponse;
import com.jdl.distribution.ui.fragment.visita.model.ProductoFrecuenteResponse;

public interface MenuVisitaView extends MvpView {
    void onProductosFrecuentesSuccess(ProductoFrecuenteResponse response);
    void onProductosFrecuentesFail(String message);

//    void onValidarEventoSuccess(EventoResponse response);
//    void onValidarEventoFail(String mensaje);

    void onCloseVisitSuccess(StatusMsgResponse response);
    void onCloseVisitFail(String message);

    //EDITAR//

    void onSeleccionaClienteSuccess(SeleccioneClienteResponse response);
    void onSeleccionaClienteFail(String mensaje);

    void onListarComboFrecuenciaSuccess(ComboFrecuenciaResponse response);
    void onListarComboFrecuenciaFail(String mensaje);

    //EDITAR//

    void onEntregaValidarSuccess(EntregaValidarResponse response);
    void onEntregaValidarFail(String mensaje);

    void onEntregaTotalSuccess(EntregaTotalResponse response);
    void onEntregaTotalFail(String message);


}
