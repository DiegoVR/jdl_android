package com.jdl.distribution.ui.activity.login.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.activity.login.model.LoginResponse;
import com.jdl.distribution.ui.activity.login.model.LoginService;
import com.jdl.distribution.ui.activity.login.view.LoginView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter extends MvpBasePresenter<LoginView> {

    private static final String LOG_TAG = LoginPresenter.class.getSimpleName();
    private Context context;

    public LoginPresenter(Context context) {
        this.context = context;
    }

    public void doLogin(JSONObject jsonObject){
        Log.d(LOG_TAG,"enviando: "+jsonObject.toString());

        LoginService service = RetrofitClient.getRetrofitInstance().create(LoginService.class);

        Call<LoginResponse> call = service.login(jsonObject.toString());

        call.enqueue(new Callback<LoginResponse>() {

            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                LoginResponse loginResponse=response.body();

                if (loginResponse==null){
                    Log.d(LOG_TAG, "onResponse: loginResponse==null");
                    if (Constants.DEBUG){
                        ifViewAttached(view -> view.onLoginFail("onResponse: loginResponse==null"));
                    }
                    return;
                }

                Log.d(LOG_TAG, "onResponse: "+loginResponse.toString());

                if (Constants.DEBUG){
                    ifViewAttached(view -> {
//                        Toast.makeText(context, loginResponse.toString(), Toast.LENGTH_LONG).show();
                    });
                }

                switch (loginResponse.getWithStringNotNull().getStatusEnum()){

                    case SUCCESS:

                        if (!new LoginPref(context).login(loginResponse)){
                            return;
                        }

                        ifViewAttached(view -> view.onLoginSuccess(loginResponse));

                        break;

                    case FAIL:
                        ifViewAttached(view -> view.onLoginFail(loginResponse.getMsg()));
                        break;
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

                Log.d(LOG_TAG, "onFailure: "+t==null?"":t.toString());

                ifViewAttached(view -> {
                    if (Constants.DEBUG){
                        view.onLoginFail(t==null?"":t.toString());
                    }else {
                        view.onLoginFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });
    }
}
