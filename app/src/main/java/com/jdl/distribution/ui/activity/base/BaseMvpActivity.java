package com.jdl.distribution.ui.activity.base;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.developer.appsupport.ui.activity.IBaseActivity;
import com.developer.appsupport.ui.listener.MyLifecycleObserver;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public abstract class BaseMvpActivity<V extends MvpView, P extends MvpPresenter<V>> extends MvpActivity<V, P> implements IBaseActivity {


    public MyLifecycleObserver observer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        if (observer!=null)
            observer.onCreate();

        super.onCreate(savedInstanceState);
    }


    @Override
    protected void onStart() {

        if (observer!=null)
            observer.onStart();

        super.onStart();
    }


    @Override
    protected void onResume() {

        if (observer!=null)
            observer.onResume();

        super.onResume();
    }


    @Override
    protected void onPause() {

        if (observer!=null)
            observer.onPause();

        super.onPause();
    }


    @Override
    protected void onStop() {

        if (observer!=null)
            observer.onStop();

        super.onStop();
    }


    @Override
    protected void onDestroy() {

        if (observer!=null)
            observer.onDestroy();

        super.onDestroy();
    }


    @Override
    public void setLifecycleObserver(MyLifecycleObserver observer){
        this.observer = observer;
    }

}
