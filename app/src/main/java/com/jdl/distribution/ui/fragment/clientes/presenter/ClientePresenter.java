package com.jdl.distribution.ui.fragment.clientes.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.model.ClienteResponse;
import com.jdl.distribution.ui.fragment.clientes.model.ClienteService;
import com.jdl.distribution.ui.fragment.clientes.model.ComboFrecuenciaResponse;
import com.jdl.distribution.ui.fragment.clientes.model.SeleccioneClienteResponse;
import com.jdl.distribution.ui.fragment.clientes.view.ClientesView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientePresenter extends MvpBasePresenter<ClientesView> {
    private static final String LOG_TAG = "[" + ClientePresenter.class.getSimpleName() + "]";
    private final Context context;

    public ClientePresenter(Context context) {
        this.context = context;
    }

    public void doClientsList(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        ifViewAttached(view -> view.showLoadingDialog(true));


        ClienteService service = RetrofitClient.getRetrofitInstance().create(ClienteService.class);
        Call<ClienteResponse> call = service.doListarClientes(jsonObject.toString());


        call.enqueue(new Callback<ClienteResponse>() {
            @Override
            public void onResponse(Call<ClienteResponse> call, Response<ClienteResponse> response) {
                ClienteResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> view.showLoadingDialog(false));
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onListClientsFail("clientListResponse==null");
                        } else {
                            view.onListClientsFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onListClientsSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onListClientsFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<ClienteResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.showLoadingDialog(false);
                        view.onListClientsFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.showLoadingDialog(false);
                        view.onListClientsFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

    public void doSeleccionaCliente(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        ifViewAttached(view -> view.showLoadingDialog(true));


        ClienteService service = RetrofitClient.getRetrofitInstance().create(ClienteService.class);
        Call<SeleccioneClienteResponse> call = service.doSeleccionaCliente(jsonObject.toString());


        call.enqueue(new Callback<SeleccioneClienteResponse>() {
            @Override
            public void onResponse(Call<SeleccioneClienteResponse> call, Response<SeleccioneClienteResponse> response) {
                SeleccioneClienteResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> view.showLoadingDialog(false));
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onSeleccionaClienteFail("clientListResponse==null");
                        } else {
                            view.onSeleccionaClienteFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onSeleccionaClienteSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onSeleccionaClienteFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<SeleccioneClienteResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.showLoadingDialog(false);
                        view.onSeleccionaClienteFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.showLoadingDialog(false);
                        view.onSeleccionaClienteFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

    public void doListarFrecuencia(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        ifViewAttached(view -> view.showLoadingDialog(true));


        ClienteService service = RetrofitClient.getRetrofitInstance().create(ClienteService.class);
        Call<ComboFrecuenciaResponse> call = service.doListarComboFrecuencia(jsonObject.toString());


        call.enqueue(new Callback<ComboFrecuenciaResponse>() {
            @Override
            public void onResponse(Call<ComboFrecuenciaResponse> call, Response<ComboFrecuenciaResponse> response) {
                ComboFrecuenciaResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> view.showLoadingDialog(false));
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onListarComboFrecuenciaFail("clientListResponse==null");
                        } else {
                            view.onListarComboFrecuenciaFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onListarComboFrecuenciaSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onListarComboFrecuenciaFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<ComboFrecuenciaResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.showLoadingDialog(false);
                        view.onListarComboFrecuenciaFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.showLoadingDialog(false);
                        view.onListarComboFrecuenciaFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

}
