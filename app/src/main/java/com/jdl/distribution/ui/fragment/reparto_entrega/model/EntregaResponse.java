package com.jdl.distribution.ui.fragment.reparto_entrega.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

public class EntregaResponse implements IStringNotNull<EntregaResponse>, StatusConverter {

    @StringNotNull
    @Expose
    @SerializedName("msg")
    private String msg;
    @StringNotNull
    @Expose
    @SerializedName("idEntrega")
    private String idEntrega;
    @Expose
    @SerializedName("status")
    private int status;

    public String getMsg() {
        return msg;
    }

    public String getIdEntrega() {
        return idEntrega;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "EntregaResponse{" +
                "msg='" + msg + '\'' +
                ", idEntrega='" + idEntrega + '\'' +
                ", status=" + status +
                '}';
    }

    @Override
    public EntregaResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }
}
