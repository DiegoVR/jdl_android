package com.jdl.distribution.ui.fragment.historial_pedidos.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.utils.Constants;
import com.developer.appsupport.utils.MyOutlineProvider;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.historial_pedidos.model.PedidoHistorialResponse;
import com.jdl.distribution.utils.adapter.AdapterBinder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PedidoHistorialAdapter extends RecyclerView.Adapter<PedidoHistorialAdapter.ViewHolder> {

    private final Context context;
    private ArrayList<PedidoHistorialResponse.DetallePedido> listPedido;
    private PedidoHistorialListener listener;

    public PedidoHistorialAdapter(Context context, ArrayList<PedidoHistorialResponse.DetallePedido> listPedido, PedidoHistorialListener listener) {
        this.context = context;
        this.listPedido = listPedido;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pedido_historial, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(listPedido.get(position));
    }

    @Override
    public int getItemCount() {
        return listPedido == null ? 0 : listPedido.size();
    }

    public void filterList(ArrayList<PedidoHistorialResponse.DetallePedido> listFiltroPedido) {
        listPedido = listFiltroPedido;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterBinder<PedidoHistorialResponse.DetallePedido>, View.OnClickListener {
        @BindView(R.id.txt_nombre_pedido_historial)
        TextView txt_nombre_pedido_historial;
        @BindView(R.id.txt_rs_pedido_historial)
        TextView txt_rs_pedido_historial;
        @BindView(R.id.txt_ruc_pedido_historial)
        TextView txt_ruc_pedido_historial;
        @BindView(R.id.txt_total_pedido_historial)
        TextView txt_total_pedido_historial;
        @BindView(R.id.txt_encargado_pedido_historial)
        TextView txt_encargado_pedido_historial;
        @BindView(R.id.ll_nombre_vendedor)
        LinearLayout ll_nombre_vendedor;
        @BindView(R.id.cv_pedido_historial)
        CardView cv_pedido_historial;
        @BindView(R.id.txt_anulado)
        TextView txt_anulado;
        @BindView(R.id.img_imp)
        ImageCardView img_imp;
        @BindView(R.id.img_edit)
        ImageCardView img_edit;
        //        @BindView(R.id.img_ojo)
//        ImageCardView img_ojo;
        @BindView(R.id.img_anular)
        ImageCardView img_anular;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cv_pedido_historial.setOutlineProvider(new MyOutlineProvider((int) Constants.convertDpiToPx(context, 10), 0.25f));
            }

//            img_ojo.setOnClickListener(this);
            img_edit.setOnClickListener(this);
            img_imp.setOnClickListener(this);
            img_anular.setOnClickListener(this);
            cv_pedido_historial.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_imp:
                    if (getAdapterPosition() != -1) {
                        listener.onItemClickListenerImprimir(listPedido.get(getAdapterPosition()));
                    }
                    break;
                case R.id.img_edit:
                    if (getAdapterPosition() != -1) {
                        listener.onItemClickListenerEditar(listPedido.get(getAdapterPosition()));
                    }
                    break;
//                case R.id.img_ojo:
//                    listener.onItemClickListener(getAdapterPosition(),getCurrentList());
//                    break;
                case R.id.cv_pedido_historial:
                    if (getAdapterPosition() != -1) {
                        listener.onItemClickListener(listPedido.get(getAdapterPosition()));
                    }
                    break;
            }
        }

        @Override
        public void bindTo(@Nullable PedidoHistorialResponse.DetallePedido productos) {
            if (productos != null) {
                txt_nombre_pedido_historial.setText(productos.getNom());
                txt_rs_pedido_historial.setText(productos.getRs());
                txt_ruc_pedido_historial.setText(productos.getRuc());
                txt_total_pedido_historial.setText(productos.getMoneda().concat(productos.getTotal()));

               /* if (productos.getEstado().equals("1")) {
                    txt_anulado.setVisibility(View.VISIBLE);

                    img_edit.setVisibility(View.GONE);
                    img_imp.setVisibility(View.GONE);
                    img_anular.setVisibility(View.GONE);
//                    img_ojo.setVisibility(View.VISIBLE);
                } else if (productos.getEstado().equals("0")) {
                    txt_anulado.setVisibility(View.GONE);

//                    img_ojo.setVisibility(View.VISIBLE);
                    img_edit.setVisibility(View.VISIBLE);
                    img_imp.setVisibility(View.VISIBLE);
                    img_anular.setVisibility(View.VISIBLE);

                }*/

//                if (new LoginPref(context).getIdProfile().equals(com.spry.sales.utils.Constants.PROFILE_SUPERVISOR)||
//                        new LoginPref(context).getIdProfile().equals(com.spry.sales.utils.Constants.PROFILE_ADMINISTRADOR)){
//                    ll_nombre_vendedor.setVisibility(View.VISIBLE);
//                }else{
//                    ll_nombre_vendedor.setVisibility(View.GONE);
//                }

            }
        }
    }

    public interface PedidoHistorialListener {
        void onItemClickListener(PedidoHistorialResponse.DetallePedido detallePedido);

        void onItemClickListenerEditar(PedidoHistorialResponse.DetallePedido detallePedido);

        void onItemClickListenerImprimir(PedidoHistorialResponse.DetallePedido detallePedido);
    }


}
