package com.jdl.distribution.ui.fragment.devolucion.devolucion_parcial.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.developer.appsupport.utils.Constants;
import com.developer.appsupport.utils.MyOutlineProvider;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.devolucion.devolucion_parcial.model.EditarListaDevolucionResponse;
import com.jdl.distribution.utils.adapter.AdapterBinder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListaEditarDevolucionAdapter extends RecyclerView.Adapter<ListaEditarDevolucionAdapter.VieweHolder> {

    Context context;
    List<EditarListaDevolucionResponse.Detalle> listDetalle;
    EditDeolucionListener listener;

    public ListaEditarDevolucionAdapter(Context context, List<EditarListaDevolucionResponse.Detalle> listDetalle,EditDeolucionListener listener) {
        this.context = context;
        this.listDetalle = listDetalle;
        this.listener = listener;
    }

    @NonNull
    @Override
    public VieweHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_edit_devolucion,parent,false);
        return new VieweHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VieweHolder holder, int position) {
        EditarListaDevolucionResponse.Detalle detalle = listDetalle.get(position);
        holder.bindTo(detalle);
    }

    @Override
    public int getItemCount() {
        return listDetalle.size();
    }

    public void selectAll(){
        for (EditarListaDevolucionResponse.Detalle entrega : listDetalle) {
            if(!entrega.isCkecked()) {
                entrega.setNuevaCantidad(entrega.getCantidad());
                entrega.setCkecked(true);
            }
        }
        notifyDataSetChanged();

    }
    public void unselectall(){
        for (EditarListaDevolucionResponse.Detalle entrega : listDetalle) {
            if(entrega.isCkecked()) {
                entrega.setNuevaCantidad("");
                entrega.setCkecked(false);
            }
        }
        notifyDataSetChanged();
    }

    public void setEnableCantidad(int position, boolean checked, String cantidad) {
        listDetalle.get(position).setNuevaCantidad(cantidad);
        listDetalle.get(position).setCkecked(checked);
        notifyItemChanged(position);
    }

    public class VieweHolder extends RecyclerView.ViewHolder implements AdapterBinder<EditarListaDevolucionResponse.Detalle>, CheckBox.OnCheckedChangeListener {
        @BindView(R.id.edt_cantidad)
        EditText edt_cantidad;
        @BindView(R.id.txt_nombre_edit_devolucion)
        TextView txt_nombre_edit_devolucion;
        @BindView(R.id.txt_codigo)
        TextView txt_codigo;
        @BindView(R.id.txt_cantidad)
        TextView txt_cantidad;
        @BindView(R.id.chb_add)
        CheckBox chb_add;
        @BindView(R.id.body)
        CardView body;
        public VieweHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                body.setOutlineProvider(new MyOutlineProvider((int) Constants.convertDpiToPx(context, 10), 0.25f));
            }
            chb_add.setOnCheckedChangeListener(this);

            edt_cantidad.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    if (editable.toString().isEmpty()) {
                        listDetalle.get(getAdapterPosition()).setNuevaCantidad("");
                    } else {
                        if (Integer.parseInt(editable.toString()) <=0 ) {

                            Log.d("EDIT_NUM",editable.toString());
                            Toast.makeText(context, "La cantidad mayor a 0", Toast.LENGTH_SHORT).show();
                            edt_cantidad.setText("");
                            listDetalle.get(getAdapterPosition() < 0 ? 0 : getAdapterPosition()).setNuevaCantidad("");
                            edt_cantidad.requestFocus();
                            return;
                        }

                        if (Integer.parseInt(editable.toString()) > Integer.parseInt(listDetalle.get(getAdapterPosition()).getCantidad())) {
                            Log.d("EDIT_MAYOR",editable.toString());
                            edt_cantidad.setText("");
                            Toast.makeText(context, "Cantidad no aceptada", Toast.LENGTH_SHORT).show();
                            listDetalle.get(getAdapterPosition()).setNuevaCantidad("");
                            edt_cantidad.requestFocus();
                            return;
                        }

                        listDetalle.get(getAdapterPosition()<0?0:getAdapterPosition()).setNuevaCantidad(TextUtils.isEmpty(editable.toString()) ? "0" : editable.toString());
                    }
                }

            });

            edt_cantidad.setOnEditorActionListener((v, actionId, event) -> {


                if (actionId == EditorInfo.IME_ACTION_NEXT){

                    listDetalle.get(getAdapterPosition()+1).setHasFocus(true);
                    notifyItemChanged(getAdapterPosition()+1);

                    return true;
                }

                return true;

            });

        }

        @Override
        public void bindTo(@Nullable EditarListaDevolucionResponse.Detalle detalle) {
            if (detalle!=null){
                txt_nombre_edit_devolucion.setText(detalle.getNombre());
                txt_cantidad.setText(detalle.getCantidad());
                txt_codigo.setText(detalle.getCodigo());
                chb_add.setChecked(detalle.isCkecked());

                if (detalle.isCkecked()){
                    edt_cantidad.setText(detalle.getNuevaCantidad());
                    edt_cantidad.setVisibility(View.VISIBLE);
                    edt_cantidad.requestFocus();
                }else{
                    edt_cantidad.setText(detalle.getNuevaCantidad());
                    edt_cantidad.setVisibility(View.GONE);
                }


                if (detalle.isHasFocus()){
                    edt_cantidad.requestFocus();
                    Constants.showKeyboard(context, edt_cantidad);
                }else {

                }

            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()){
                case R.id.chb_add:
                    if(getAdapterPosition()!=-1) {
                        if (chb_add.isPressed()) {
                            listener.onItemClickAddProducto(getAdapterPosition(), isChecked, listDetalle.get(getAdapterPosition()));
                        }
                    }
                    break;
            }
        }
    }

    public interface EditDeolucionListener {
        void onItemClickAddProducto(int position,boolean isChecked, EditarListaDevolucionResponse.Detalle detalle);
    }
}
