package com.jdl.distribution.ui.fragment.agregar_cliente.model;

import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.utils.Constants;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface AgregarClienteService {
    @POST(Constants.WS_AGREGAR_CLIENTE)
    Call<StatusMsgResponse> doAgregarCliente(@Body String data);

    @Multipart
    @POST(Constants.WS_CONSULTAR_DOCUMENTO_DNI)
    Call<MigoDocumentoResponse> doConsultarDocumentoDni(@Part("token") RequestBody token, @Part("dni") RequestBody dni);

    @Multipart
    @POST(Constants.WS_CONSULTAR_DOCUMENTO_RUC)
    Call<MigoDocumentoResponse> doConsultarDocumentoRuc(@Part("token") RequestBody token, @Part("ruc") RequestBody dni);

    @Multipart
    @POST(Constants.WS_AGREGAR_IMAGEN_CLIENTE)
    Call<StatusMsgResponse> doAgregarImagen(@Part("data") RequestBody data,@Part MultipartBody.Part file);
}
