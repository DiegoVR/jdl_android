package com.jdl.distribution.ui.fragment.productos.dialog;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.developer.appsupport.ui.dialog.select_photo.SelectPhotoDialog;
import com.developer.appsupport.utils.files.ImageListener;
import com.developer.appsupport.utils.files.ImagePresenter;
import com.developer.appsupport.utils.files.ImagePresenterImpl;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.productos.blur.RSBlurProcessor;
import com.jdl.distribution.ui.fragment.productos.model.ProductoResponse;

import org.parceler.Parcels;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogImagenProducto extends BaseDialogFragment implements SelectPhotoDialog.OnTypeSelectedListener, ImageListener,View.OnClickListener {

    private String ROOT_DIRECTORY = Environment.getExternalStorageDirectory() + "/"+"AppSupport"+"/";
    private String ROOT_DIRECTORY_FOTOS = ROOT_DIRECTORY.concat("photos/");
    public static final String PRODUCTO = "PRODUCTO";

    @BindView(R.id.ll_reemplazar)
    LinearLayout ll_reemplazar;
    @BindView(R.id.img_producto)
    ImageView img_producto;
    @BindView(R.id.txt_nombre_producto)
    TextView txt_nombre_producto;
    @Destroy(type = Type.PROGRESS_BAR)
    @BindView(R.id.pb_img_producto)
    public ProgressBar pb_img_producto;

    private ImagePresenter imagePresenter;
    private SelectPhotoDialog selectPhotoDialog;
    private ProductoResponse.Productos productos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_imagen_producto, container, false);
        ButterKnife.bind(this,view);
        init(savedInstanceState);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() == null) {
            return;
        }
//        setStyle(DialogFragment.STYLE_NORMAL, R.style.MyCustomTheme);
    }

    private void init(Bundle savedInstanceState) {
        imagePresenter = new ImagePresenterImpl(getActivity(), this);

        try {
            productos = Parcels.unwrap(getArguments().getParcelable(PRODUCTO));
        }catch (Exception e){
            e.printStackTrace();
        }

        setData();
        initDialog(savedInstanceState);
        setListener();
    }

    private void setData() {

        Glide.with(getActivity())
                .load(productos.getImagen())
                .thumbnail(0.1f)
                .into(img_producto);

        txt_nombre_producto.setText(productos.getNom());

    }

    private void setListener() {


        ll_reemplazar.setOnClickListener(this);
    }

    public static Bitmap getBitmapFromView(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        view.draw(c);
        return bitmap;
    }

    private void initDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) {

            selectPhotoDialog = (SelectPhotoDialog) getChildFragmentManager().findFragmentByTag(SelectPhotoDialog.class.getSimpleName());

            if (selectPhotoDialog==null){
                selectPhotoDialog = new SelectPhotoDialog();
                selectPhotoDialog.setOnTypeSelectedListener(this);
            }

        }else {
            selectPhotoDialog = new SelectPhotoDialog();
            selectPhotoDialog.setOnTypeSelectedListener(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        imagePresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePresenter.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void onTypeSelected(SelectPhotoDialog.ImageProvider imageProvider) {
        switch (imageProvider){

            case GALLERY:
                imagePresenter.doGetImageGallery();
                break;

            case CAMERA:
                imagePresenter.doGetImageCamera();
                break;
        }
    }

    @Override
    public void onImageLoading(boolean loading, String message) {

    }

    @Override
    public void onImageSuccess(File file) {

    }

    @Override
    public void onImageViewSuccess(Bitmap bitmap) {

    }

    @Override
    public void onImageFail(String message) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_reemplazar:
                selectPhotoDialog.show(getChildFragmentManager(), SelectPhotoDialog.class.getSimpleName());
                break;
        }
    }
}
