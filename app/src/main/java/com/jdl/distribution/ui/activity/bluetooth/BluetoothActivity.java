package com.jdl.distribution.ui.activity.bluetooth;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

import com.developer.appsupport.ui.activity.BaseActivity;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.utils.JSONGenerator;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.activity.bluetooth.adapter.BluetoothAdapterAdd;
import com.jdl.distribution.ui.activity.bluetooth.model.BluetoothModel;
import com.jdl.distribution.utils.Constants;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BluetoothActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    @BindView(R.id.rv_bluetooth)
    RecyclerView rv_bluetooth;
    @BindView(R.id.img_back)
    ImageCardView img_back;
    @BindView(R.id.sw_bluetooth)
    SwipeRefreshLayout sw_bluetooth;

    public static final int REQUEST_BLUETOOTH = 1;
    private static BluetoothAdapter BTAdapter;
    private ArrayList<BluetoothModel> listadoBluetooth = new ArrayList<>();
    private BluetoothAdapterAdd bluetoohAdapter;
    static private BluetoothSocket mbtSocket = null;

    private Dialog dialog;
    int format = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        ButterKnife.bind(this, this);
//        printerPresenter = new BluetoothPresenterImpl(getContext(), this);

        BTAdapter = BluetoothAdapter.getDefaultAdapter();

        permisos();

        init();

    }

    private void init() {
        sw_bluetooth.setOnRefreshListener(this);
        EsCompatible();
        listarDireccionesBluetooth();
        flushData();
        setListener();
    }

    private void permisos(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    Constants.REQUEST_CODE_PERMISION_LOCATION);
        } else {
            proceedDiscovery();
        }
    }

    private void setListener() {
        img_back.setOnClickListener(this);
    }

    private void EsCompatible() {
        // Phone does not support Bluetooth so let the user know and exit.
        if (BTAdapter == null) {
            new AlertDialog.Builder(this)
                    .setTitle("No es compatible")
                    .setMessage("Su celular no soporta Bluetooth")
                    .setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

//        if (!BTAdapter.isEnabled()) {
//            Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            startActivityForResult(enableBT, REQUEST_BLUETOOTH);
//        }
    }

    private void flushData() {
        try {
            if (mbtSocket != null) {
                mbtSocket.close();
                mbtSocket = null;
            }

//            if (mBluetoothAdapter != null) {
//                mBluetoothAdapter.cancelDiscovery();
//            }


            //finalize();
        } catch (Exception ex) {
//            Log.e(TAG, ex.getMessage());
        }

    }

    private void listarDireccionesBluetooth() {
        final GridLayoutManager layoutManager = new GridLayoutManager(this,1);
        rv_bluetooth.setLayoutManager(layoutManager);
        rv_bluetooth.setItemAnimator(new DefaultItemAnimator());

        listadoBluetooth.clear();

        Set<BluetoothDevice> pairedDevices = BTAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                BluetoothModel newDevice= new BluetoothModel(device.getName(),device.getAddress());
                listadoBluetooth.add(newDevice);
            }
        }

//        if(listadoBluetooth.size() == 0) {
//            listadoBluetooth.add(new BluetoothModel("No hay dispositivos", ""));
//        }

        bluetoohAdapter = new BluetoothAdapterAdd(this, listadoBluetooth, new BluetoothAdapterAdd.AsynStatusListenerAdd(){
            @Override
            public void onClickItem(BluetoothModel model) {
                openDialogSelectFormat(model);
            }
        });
        rv_bluetooth.setAdapter(bluetoohAdapter);
    }

    private void openDialogSelectFormat(BluetoothModel model){

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_format_print);
        dialog.show();

        RadioButton rb_57mm = dialog.findViewById(R.id.rb_57mm);
        RadioButton rb_80mm = dialog.findViewById(R.id.rb_80mm);
        ProgressButton btn_seleccionar_formato = dialog.findViewById(R.id.btn_seleccionar_formato);

        rb_57mm.setOnClickListener(i ->{
            format = 1;
        });

        rb_80mm.setOnClickListener(i ->{
            format = 2;
        });

        btn_seleccionar_formato.setOnClickListener(i ->{
            onBackPressed();
//            sendDataSavePrint(model,format);
        });

    }

    private void sendDataSavePrint(BluetoothModel model, int format){

//        JSONGenerator.getNewInstance(false, this)
//                .requireInternet(true)
//                .put("idfv", new LoginPref(this).getUid())
//                .put("nom", model.getNombre())
//                .put("mac", model.getIp6())
//                .put("predet", "1")
//                .put("cta", new LoginPref(this).getCuenta())
//                .put("format", format)
//                .operate(new JSONGenerator.OnOperateListener() {
//                    @Override
//                    public void onOperateError() {
////                                showContent();
//                    }
//
//                    @Override
//                    public void onOperateSuccess(JSONObject jsonObject) {
//                        printerPresenter.doAddImpresoraRequest( jsonObject);
//                    }
//                });
    }

    protected void proceedDiscovery() {
//        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
//        filter.addAction(BluetoothDevice.ACTION_NAME_CHANGED);
////        registerReceiver(mBTReceiver, filter);
//
//        BTAdapter.startDiscovery();
        if (!BTAdapter.isEnabled()) {
            Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBT, REQUEST_BLUETOOTH);
            sw_bluetooth.setRefreshing(false);
        }else{
            sw_bluetooth.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        if (Constants.verifyInternetAndMqtt(this)){
            sw_bluetooth.setRefreshing(false);
            return;
        }
        permisos();
        listarDireccionesBluetooth();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
}
