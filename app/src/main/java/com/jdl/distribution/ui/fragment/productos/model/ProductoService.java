package com.jdl.distribution.ui.fragment.productos.model;

import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ProductoService {
    @POST(Constants.WS_PRODUCTOS_LISTAR)
    Call<ProductoResponse> doProductosListar(@Body String data);
}
