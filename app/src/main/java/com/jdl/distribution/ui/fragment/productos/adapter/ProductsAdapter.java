package com.jdl.distribution.ui.fragment.productos.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.utils.Constants;
import com.developer.appsupport.utils.MyOutlineProvider;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.fragment.productos.model.ProductoResponse;
import com.jdl.distribution.utils.adapter.AdapterBinder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {
    private final Context context;
    private ProductsListener listener;
    private ArrayList<ProductoResponse.Productos> listProductos;

    public ProductsAdapter(Context context, ArrayList<ProductoResponse.Productos> listProductos, ProductsListener listener) {
        this.context = context;
        this.listProductos = listProductos;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_products, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProductoResponse.Productos productos = listProductos.get(position);
        holder.bindTo(productos);
    }

    @Override
    public int getItemCount() {
        return listProductos.size();
    }

    public void filterList(ArrayList<ProductoResponse.Productos> listFiltroPedido) {
        listProductos = listFiltroPedido;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterBinder<ProductoResponse.Productos>, View.OnClickListener {
        @BindView(R.id.txt_nombre_producto)
        TextView txt_nombre_producto;
        @BindView(R.id.txt_stock_producto)
        TextView txt_stock_producto;
        @BindView(R.id.txt_precio_producto)
        TextView txt_precio_producto;
        @BindView(R.id.cv_products)
        CardView cv_products;
        @BindView(R.id.txt_nombre_stock)
        TextView txt_nombre_stock;
        @BindView(R.id.txt_precio_descuento)
        TextView txt_precio_descuento;
        @BindView(R.id.txt_stock_total)
        TextView txt_stock_total;
        @BindView(R.id.img_editar)
        ImageCardView img_editar;
        //        @BindView(R.id.img_stock)
//        ImageCardView img_stock;
        @BindView(R.id.ll_precio)
        LinearLayout ll_precio;
        /*@BindView(R.id.ll_descuento)
        LinearLayout ll_descuento;*/
        @BindView(R.id.img_producto)
        ImageView img_producto;
        @BindView(R.id.cv_imagen)
        CardView cv_imagen;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cv_products.setOutlineProvider(new MyOutlineProvider((int) Constants.convertDpiToPx(context, 10), 0.25f));
            }
//            img_stock.setOnClickListener(this);
            img_editar.setOnClickListener(this);
            cv_imagen.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_editar:
                    if (getAdapterPosition() != -1) {
                        listener.onItemClickEditListener(listProductos.get(getAdapterPosition()));
                    }
                    break;
                case R.id.cv_imagen:
                    if (getAdapterPosition() != -1) {
                        listener.onItemClickImageListener(listProductos.get(getAdapterPosition()));
                    }
                    break;
            }
        }

        @Override
        public void bindTo(@Nullable ProductoResponse.Productos productos) {
            if (productos != null) {
                txt_nombre_producto.setText(productos.getNom());
                txt_stock_producto.setText(productos.getStock());
                txt_precio_producto.setText(productos.getCost());
                txt_stock_total.setText(productos.getStockT());
                txt_precio_descuento.setText(productos.getDesP() + "%");
//
//                Glide.with(context)
//                        .load(productos.getImagen())
////                        .diskCacheStrategy(DiskCacheStrategy.NONE)
////                        .skipMemoryCache(true)
//                        .thumbnail(0.1f)
//                        .centerCrop()
//                        .error(R.drawable.ic_no_gallery)
//                        .into(img_producto);

               if (productos.getImagen().isEmpty()){
                   img_producto.setPadding(30,30,30,30);
                   Glide.with(context)
                           .load(R.drawable.ic_no_gallery)
//                        .diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .skipMemoryCache(true)
                           .thumbnail(0.1f)
                           .centerCrop()
                           .into(img_producto);
               }else{
                   byte[] base64Decoded = android.util.Base64.decode(productos.getImagen(), android.util.Base64.DEFAULT);
                   img_producto.setPadding(0,0,0,0);
                   Glide.with(context)
                           .load(base64Decoded)
//                        .diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .skipMemoryCache(true)
                           .thumbnail(0.1f)
                           .centerCrop()
                           .into(img_producto);
               }

                if (new LoginPref(context).getIdProfile().equals(com.jdl.distribution.utils.Constants.PROFILE_PRE_VENTA)) {
                    txt_nombre_stock.setText("Planta:");
                } else if (new LoginPref(context).getIdProfile().equals(com.jdl.distribution.utils.Constants.PROFILE_AUTO_VENTA)) {
                    txt_nombre_stock.setText("Carga:");
                } else if (new LoginPref(context).getIdProfile().equals(com.jdl.distribution.utils.Constants.PROFILE_ADMINISTRADOR)) {
                    txt_nombre_stock.setText("Stock:");
                    img_editar.setVisibility(View.VISIBLE);
                } else if (new LoginPref(context).getIdProfile().equals(com.jdl.distribution.utils.Constants.PROFILE_SUPERVISOR)) {
                    txt_nombre_stock.setText("Stock:");
                } else if (new LoginPref(context).getIdProfile().equals(com.jdl.distribution.utils.Constants.PROFILE_REPARTO)) {
                    txt_nombre_stock.setText("Carga:");
                    ll_precio.setVisibility(View.GONE);
                }

                if (Integer.parseInt(productos.getStock()) <= 0) {
                    txt_nombre_stock.setTextColor(context.getResources().getColor(R.color.rojo));
                } else {
                    txt_nombre_stock.setTextColor(context.getResources().getColor(R.color.celeste));
                }

            }
        }
    }

    public interface ProductsListener {
        void onItemClickEditListener(ProductoResponse.Productos productos);

        void onItemClickImageListener(ProductoResponse.Productos productos);
//        void onItemClickStockListener(int codigo);
    }

//    private static final DiffUtil.ItemCallback<ProductsResponse.Productos> DIFF = new DiffUtil.ItemCallback<ProductsResponse.Productos>() {
//        @Override
//        public boolean areItemsTheSame(@NonNull ProductsResponse.Productos oldItem, @NonNull ProductsResponse.Productos newItem) {
//            return oldItem.getIdPro() == newItem.getIdPro();
//        }
//
//        @Override
//        public boolean areContentsTheSame(@NonNull ProductsResponse.Productos oldItem, @NonNull ProductsResponse.Productos newItem) {
//            return true;
//        }
//    };

}
