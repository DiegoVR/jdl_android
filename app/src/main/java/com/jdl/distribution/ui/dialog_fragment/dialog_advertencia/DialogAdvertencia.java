package com.jdl.distribution.ui.dialog_fragment.dialog_advertencia;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.jdl.distribution.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogAdvertencia extends BaseDialogFragment implements View.OnClickListener {

    @BindView(R.id.btn_si)
    Button btn_si;
    @BindView(R.id.btn_no)
    Button btn_no;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_advertencia, container, false);
        setCancelable(false);
        ButterKnife.bind(this,view);

        init();
        return view;
    }

    private void init(){
        setListener();
    }

    private void setListener() {
        btn_si.setOnClickListener(this);
        btn_no.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_no:
                dismiss();
                break;
            case R.id.btn_si:
                getActivity().moveTaskToBack(true);
                dismiss();
                break;
        }
    }
}
