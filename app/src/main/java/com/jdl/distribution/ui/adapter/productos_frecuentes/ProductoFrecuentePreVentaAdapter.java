package com.jdl.distribution.ui.adapter.productos_frecuentes;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.developer.appsupport.ui.custom.ImageCardView;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.visita.model.ProductoFrecuenteResponse;
import com.jdl.distribution.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductoFrecuentePreVentaAdapter extends RecyclerView.Adapter<ProductoFrecuentePreVentaAdapter.ViewHolder> {

    private Context context;
    private List<ProductoFrecuenteResponse.Product> items;
    private Listener listener;

    public ProductoFrecuentePreVentaAdapter(Context context, List<ProductoFrecuenteResponse.Product> items, Listener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_producto_frecuente,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ProductoFrecuenteResponse.Product product = items.get(i);

        if((i % 2) == 0)
            viewHolder.ll_item_body.setBackgroundColor(context.getResources().getColor(R.color.windowBackground));
        else
            viewHolder.ll_item_body.setBackgroundColor(context.getResources().getColor(R.color.white));

        viewHolder.nombre.setText(product.getNombre());

        viewHolder.txtStock.setText(product.getStock());
        viewHolder.txtDescuento.setText(product.getValorDescuentoFormateado());
        viewHolder.txtPunitario.setText("P.Unit ".concat(product.getSigla()).concat(String.valueOf(product.getCostoSinDescuentos())));

//        String total = product.getTotal().isEmpty() ? String.valueOf(product.getCostoSinDescuentos()) : product.getTotal();

        viewHolder.txt_total_item.setText(String.valueOf(product.getTotal()));

        if (product.getCantidad()>0)
            viewHolder.edt_venta.setText(String.valueOf(product.getCantidad()));
        else
            viewHolder.edt_venta.setText(product.getCant());

        if (product.isHasFocus()){
            viewHolder.edt_venta.requestFocus();
            com.developer.appsupport.utils.Constants.showKeyboard(context, viewHolder.edt_venta);
        }else {

        }

    }

    public void setNuevoPrecio(int position, String discountValue){

        items.get(position).setCostoSinDescuentos(Double.parseDouble(discountValue));

        notifyItemChanged(position);

    }

    public void setDiscount(int position, String discountType, String discountValue, String precio){

        items.get(position).setValorDescuento(discountValue);
        items.get(position).setTipoDescuento(discountType);
        items.get(position).setCostoSinDescuentos(Double.parseDouble(precio));

        if (discountType.equals(Constants.DISCOUNT_PORCENTAJE)){

            items.get(position).setValorDescuentoFormateado("dsct: ".concat(discountValue).concat("%"));
            double importe = items.get(position).getCantidad() * items.get(position).getCostoSinDescuentos();
            double dsct = (Double.parseDouble(discountValue)*importe)/100;
            items.get(position).setTotal(String.valueOf(dsct));

        } else if (discountType.equals(Constants.DISCOUNT_NETO)) {

            items.get(position).setValorDescuentoFormateado("dsct: ".concat(items.get(position).getSigla()).concat(discountValue));
            double valor = Double.parseDouble(discountValue);
            items.get(position).setTotal(String.valueOf(valor));

        }

        notifyItemChanged(position);

    }

    @Override
    public int getItemCount() {
        return items ==null?0: items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.edt_venta)
        EditText edt_venta;
        @BindView(R.id.txt_nombre_pedido_detalle_item)
        TextView nombre;
        @BindView(R.id.txt_stock)
        TextView txtStock;
        @BindView(R.id.img_eye)
        ImageCardView imgEye;
        @BindView(R.id.txt_descuento)
        TextView txtDescuento;
        @BindView(R.id.txt_p_unitario)
        TextView txtPunitario;
        @BindView(R.id.txt_total_item)
        TextView txt_total_item;
        @BindView(R.id.ll_item_body)
        LinearLayout ll_item_body;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);


            edt_venta.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    items.get(getAdapterPosition()<0?0:getAdapterPosition()).setCantidad(TextUtils.isEmpty(editable.toString()) ? 0 : Integer.parseInt(editable.toString()));

                    double importe = (items.get(getAdapterPosition()).getCantidad() * items.get(getAdapterPosition()).getCostoSinDescuentos())-Double.parseDouble(items.get(getAdapterPosition()).getTotal().isEmpty() ? "0.0" : items.get(getAdapterPosition()).getTotal());
//                    items.get(getAdapterPosition()).setTotal(String.valueOf(importe));

                    txt_total_item.setText("Total: ".concat(items.get(getAdapterPosition()).getSigla()).concat(String.valueOf(Constants.toDecimal(importe,2))));


                    listener.onTextChangeListener(getAdapterPosition(), edt_venta);

                }
            });

            edt_venta.setOnEditorActionListener((v, actionId, event) -> {


                if (actionId == EditorInfo.IME_ACTION_NEXT){

                    if(items.size() > getAdapterPosition()+1){
                        items.get(getAdapterPosition()+1).setHasFocus(true);
                        notifyItemChanged(getAdapterPosition()+1);

                        return true;
                    }
                }

                return true;

            });

            imgEye.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){

                case R.id.img_eye:
                    if (getAdapterPosition() != -1) {
                        listener.onEyeClickListener(getAdapterPosition());
                    }
                    break;
            }
        }
    }

    public interface Listener{

        void onEyeClickListener(int i);
        void onTextChangeListener(int i, EditText editText);

    }

}
