package com.jdl.distribution.ui.adapter.adapter_viewpager;

import android.os.Parcelable;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TableLayoutAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();


    public List<Fragment> getmFragmentList() {
        return mFragmentList;
    }

    //    ViewFragment listener;
    public TableLayoutAdapter(FragmentManager manager) {
        super(manager);
//        this.listener = listener;
    }

    @Override
    public Fragment getItem(int position) {

        return mFragmentList.get(position);
    }


    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(List<Fragment> list) {
        mFragmentList.clear();
        mFragmentList.addAll(list);
    }

    public void removeFragment(int position) {
        mFragmentList.remove(position);
        notifyDataSetChanged();
    }

    public interface ViewFragment{

        void onScrollView(int vista);

    }

    @Override
    public Parcelable saveState() {
        return null;
    }

//    @Override
//    public void restoreState(Parcelable state, ClassLoader loader) {
//        try {
//            super.restoreState(state, loader);
//        } catch (Exception e) {
//            Log.e("TAG", "Error Restore State of Fragment : " + e.getMessage(), e);
//        }
//    }

}
