package com.jdl.distribution.ui.fragment.editar_cliente.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.ui.fragment.editar_cliente.model.CrudClienteService;
import com.jdl.distribution.ui.fragment.editar_cliente.view.CrudClienteView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrudClientePresenter extends MvpBasePresenter<CrudClienteView> {

    private static final String LOG_TAG = "[" + CrudClientePresenter.class.getSimpleName() + "]";
    private final Context context;

    public CrudClientePresenter(Context context) {
        this.context = context;
    }

    public void doActualizarCliente(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        CrudClienteService service = RetrofitClient.getRetrofitInstance().create(CrudClienteService.class);
        Call<StatusMsgResponse> call = service.doActualizarCliente(jsonObject.toString());


        call.enqueue(new Callback<StatusMsgResponse>() {
            @Override
            public void onResponse(Call<StatusMsgResponse> call, Response<StatusMsgResponse> response) {
                StatusMsgResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onActualizarFail("clientListResponse==null");
                        } else {
                            view.onActualizarFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.onActualizarSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.onActualizarFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<StatusMsgResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onActualizarFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onActualizarFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

}
