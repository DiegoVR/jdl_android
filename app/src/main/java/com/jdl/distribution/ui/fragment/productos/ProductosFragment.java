package com.jdl.distribution.ui.fragment.productos;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Destroyer;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.developer.appsupport.utils.JSONGenerator;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.fragment.base.BaseMvpFragment;
import com.jdl.distribution.ui.fragment.productos.adapter.ProductsAdapter;
import com.jdl.distribution.ui.fragment.productos.dialog.DialogImagenProducto;
import com.jdl.distribution.ui.fragment.productos.model.ProductoResponse;
import com.jdl.distribution.ui.fragment.productos.presenter.ProductoPresenter;
import com.jdl.distribution.ui.fragment.productos.view.ProductosView;
import com.jdl.distribution.utils.Constants;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductosFragment extends BaseMvpFragment<ProductosView, ProductoPresenter > implements ProductosView, SwipeRefreshLayout.OnRefreshListener {

    public static boolean CLICK = true;

    LoginPref loginPref;
    @Destroy(type = Type.SWIPE_REFRESH)
    @BindView(R.id.sw_products)
    public SwipeRefreshLayout sw_products;
    @BindView(R.id.rv_products)
    RecyclerView rv_products;
    @BindView(R.id.edt_search_products)
    EditText edt_search_products;
    @BindView(R.id.icv_search_products)
    ImageCardView icv_search_products;

    private DialogFragment dialogImagen;
    private ProductsAdapter adapter;
    ArrayList<ProductoResponse.Productos> listProducto = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_productos, container, false);
        ButterKnife.bind(this, view);

        init(savedInstanceState);
        return view;
    }

    private void init(Bundle savedInstanceState) {
        if (getActivity() == null)
            return;

        initDialog(savedInstanceState);

        loginPref = new LoginPref(getContext());

        loadData();
        setListeners();
        setAdapters();
        search();

    }

    private void setListeners() {
        sw_products.setOnRefreshListener(this);
    }

    private void search() {
        edt_search_products.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filtrar(s.toString());
            }


        });
    }

    private void filtrar(String text) {

        ArrayList<ProductoResponse.Productos> listFiltroPedido = new ArrayList<>();

        for (ProductoResponse.Productos local : listProducto) {
            if (local.getNom().toLowerCase().contains(text.toLowerCase())) {
                listFiltroPedido.add(local);
            }
        }

        adapter.filterList(listFiltroPedido);

    }

    private void setAdapters() {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        rv_products.setLayoutManager(layoutManager);
        rv_products.setItemAnimator(new DefaultItemAnimator());

        adapter = new ProductsAdapter(getContext(), listProducto, new ProductsAdapter.ProductsListener() {
            @Override
            public void onItemClickEditListener(ProductoResponse.Productos productos) {


            }

            @Override
            public void onItemClickImageListener(ProductoResponse.Productos productos) {

                Bundle bundle = new Bundle();
                bundle.putParcelable(DialogImagenProducto.PRODUCTO, Parcels.wrap(productos));
                dialogImagen.setArguments(bundle);
                dialogImagen.showNow(getChildFragmentManager(), DialogImagenProducto.class.getSimpleName());

            }
        });

        rv_products.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void loadData(){
        JSONGenerator.getNewInstance(getActivity())
                .requireInternet(true)
                .put("id_perfil", new LoginPref(getContext()).getIdProfile())
                .put("id_usuario", loginPref.getUid())
                .operate(jsonObject -> {
                    getPresenter().doListProgramados(jsonObject);
                });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) {

            dialogImagen = (DialogImagenProducto) getChildFragmentManager().findFragmentByTag(DialogImagenProducto.class.getSimpleName());

            if (dialogImagen != null)
                dialogImagen = new DialogImagenProducto();

        } else {
            dialogImagen = new DialogImagenProducto();
        }
    }

    @Override
    public ProductoPresenter createPresenter() {
        return new ProductoPresenter(getContext());
    }

    @Override
    public void showLoadingDialog(boolean status) {
        if (status){
            try {
                Destroyer.subscribe(this);
            }catch (Exception e){
                e.printStackTrace();
            }
            sw_products.setRefreshing(true);
        }else {
            try {
                Destroyer.unsubscribe(this);
            }catch (Exception e){
                e.printStackTrace();
            }
            sw_products.setRefreshing(false);
        }
    }

    @Override
    public void onListarProductoSuccess(ProductoResponse response) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
        if (response.getListP() != null){
            listProducto.clear();
            listProducto.addAll(response.getListP());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onListarProductoFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        if (Constants.verifyInternetAndMqtt(getContext())){
            sw_products.setRefreshing(false);
            return;
        }

        loadData();

    }
}
