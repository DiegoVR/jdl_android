package com.jdl.distribution.ui.dialog_fragment.dialog_fragment.model;

import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SearchProductService {

    @POST(Constants.WS_BUSCAR_PRODUCTO_NOMBRE)
    Call<BuscarProductoResponse> searchProduct(@Body String data);

    @POST(Constants.WS_BUSCAR_PRODUCTO_SCANNER)
    Call<BuscarProductoResponse> searchProductScanner(@Body String data);

}
