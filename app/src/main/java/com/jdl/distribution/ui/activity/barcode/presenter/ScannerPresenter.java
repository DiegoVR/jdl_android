package com.jdl.distribution.ui.activity.barcode.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.activity.barcode.view.ScannerView;
import com.jdl.distribution.ui.dialog_fragment.dialog_fragment.model.BuscarProductoResponse;
import com.jdl.distribution.ui.dialog_fragment.dialog_fragment.model.SearchProductService;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScannerPresenter extends MvpBasePresenter<ScannerView> {
    private static final String LOG_TAG = ScannerPresenter.class.getSimpleName();
    private final Context context;

    public ScannerPresenter(Context context) {
        this.context = context;
    }

    public void doSearchProduct(JSONObject jsonObject){

        Log.d(LOG_TAG, "enviando: "+jsonObject.toString());

        ifViewAttached(view -> view.showLoadingDialog(true));

        SearchProductService service = RetrofitClient.getRetrofitInstance().create(SearchProductService.class);
        Call<BuscarProductoResponse> call = service.searchProductScanner(jsonObject.toString());
        call.enqueue(new Callback<BuscarProductoResponse>() {
            @Override
            public void onResponse(Call<BuscarProductoResponse> call, Response<BuscarProductoResponse> response) {

                BuscarProductoResponse searchProductResponse = response.body();

                if (searchProductResponse==null){
                    Log.d(LOG_TAG, "searchProductResponse==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.showLoadingDialog(false);
                            view.onScannerFail("searchProductResponse==null");
                        } else {
                            view.showLoadingDialog(false);
                            view.onScannerFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, searchProductResponse.toString());

                switch (searchProductResponse.getWithStringNotNull().getStatusEnum()){

                    case SUCCESS:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onScannerSuccess(searchProductResponse);});

                        break;

                    case FAIL:
                        ifViewAttached(view ->{
                            view.showLoadingDialog(false);
                            view.onScannerFail(searchProductResponse.getMsg());});

                        break;

                }

            }

            @Override
            public void onFailure(Call<BuscarProductoResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: "+t==null?"":t.toString());
                ifViewAttached(view -> {

                    if (Constants.DEBUG) {
                        view.showLoadingDialog(false);
                        view.onScannerFail("onFailure: "+t==null?"":t.toString());
                    } else {
                        view.showLoadingDialog(false);
                        view.onScannerFail(context.getResources().getString(R.string.error));
                    }

                });
            }
        });

    }
}
