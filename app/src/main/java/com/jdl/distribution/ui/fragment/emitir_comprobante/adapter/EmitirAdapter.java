package com.jdl.distribution.ui.fragment.emitir_comprobante.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmitirAdapter extends RecyclerView.Adapter<EmitirAdapter.ViewHolder> {
    Context context;
    List<EmitirComprobanteResponse.DetallePreventa> listadoPedido;

    public EmitirAdapter(Context context, List<EmitirComprobanteResponse.DetallePreventa> listadoPedido) {
        this.context = context;
        this.listadoPedido = listadoPedido;
    }

    @NonNull
    @Override
    public EmitirAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pedido_imprimir,viewGroup,false);
        return new EmitirAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmitirAdapter.ViewHolder viewHolder, int i) {
        EmitirComprobanteResponse.DetallePreventa model = listadoPedido.get(i);

        viewHolder.txt_nombre_item.setText(model.getNombre());
        viewHolder.txt_cantidad_item.setText(String.valueOf(model.getCantidad()));
        viewHolder.txt_codigo_item.setText(String.valueOf(model.getCodigo()));
        viewHolder.txt_precio_unitario_item.setText(String.valueOf(model.getCostos()));
        viewHolder.txt_sub_total_item.setText(String.valueOf(model.getSubttotal()));

    }

    @Override
    public int getItemCount() {
        return listadoPedido == null ? 0 : listadoPedido.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_sub_total_item)
        TextView txt_sub_total_item;
        @BindView(R.id.txt_nombre_item)
        TextView txt_nombre_item;
        @BindView(R.id.txt_codigo_item)
        TextView txt_codigo_item;
        @BindView(R.id.txt_cantidad_item)
        TextView txt_cantidad_item;
        @BindView(R.id.txt_precio_unitario_item)
        TextView txt_precio_unitario_item;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            txt_sub_total_item = itemView.findViewById(R.id.txt_sub_total_item);
            txt_nombre_item = itemView.findViewById(R.id.txt_nombre_item);
            txt_codigo_item = itemView.findViewById(R.id.txt_codigo_item);
            txt_cantidad_item = itemView.findViewById(R.id.txt_cantidad_item);
            txt_precio_unitario_item = itemView.findViewById(R.id.txt_precio_unitario_item);

        }
    }
}
