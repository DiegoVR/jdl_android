package com.jdl.distribution.ui.fragment.agregar_cliente.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.ui.fragment.agregar_cliente.model.MigoDocumentoResponse;

public interface AgregarClienteView extends MvpView {
    void onAgregarClienteSuccess(StatusMsgResponse response);
    void onAgregarClienteFail(String mensaje);

    void onAgregarImagenSuccess(StatusMsgResponse response);
    void onAgregarImagenFail(String mensaje);

    void onConsultarDocumentoSuccess(MigoDocumentoResponse response);
    void onConsultarDocumentoFail(String mensaje);
}
