package com.jdl.distribution.ui.activity.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.developer.appsupport.ui.activity.BaseActivity;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.activity.login.LoginActivity;
import com.jdl.distribution.ui.activity.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity {

    private final int SPLASH_DURATION = 4000; //milliseconds
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    private void init(){
        createTimer();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelTimer();
    }


    private void createTimer(){

        countDownTimer = new CountDownTimer(SPLASH_DURATION, SPLASH_DURATION) {

            public void onTick(long paramAnonymousLong) {}

            public void onFinish() {

                if (new LoginPref(SplashActivity.this).getUid().length() == 0)
                {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    SplashActivity.this.overridePendingTransition(17432576, 17432577);
                    SplashActivity.this.finish();
                    return;
                }
                SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainActivity.class));
                SplashActivity.this.overridePendingTransition(17432576, 17432577);
                SplashActivity.this.finish();
            }

        };

        countDownTimer.start();
    }

    private void cancelTimer(){
        try {
            countDownTimer.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
