package com.jdl.distribution.ui.fragment.mapa;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Destroyer;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.developer.appsupport.utils.JSONGenerator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.AppPref;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.mvp.model.ClienteResponse;
import com.jdl.distribution.ui.fragment.base.BaseMvpFragment;
import com.jdl.distribution.ui.fragment.mapa.dialog.DialogLeyenda;
import com.jdl.distribution.ui.fragment.mapa.marker_custom.MarkerAdapter;
import com.jdl.distribution.ui.fragment.mapa.marker_custom.OnInfoWindowElemTouchListener;
import com.jdl.distribution.ui.fragment.mapa.presenter.MapaPresenter;
import com.jdl.distribution.ui.fragment.mapa.view.MapaView;
import com.jdl.distribution.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapaClienteFragment extends BaseMvpFragment<MapaView, MapaPresenter>
        implements OnMapReadyCallback, View.OnClickListener, MapaView {

    private static final String LOG_TAG = MapaClienteFragment.class.getSimpleName();

    private GoogleMap mMap;
    private SupportMapFragment supportMapFragment;
    private LoginPref loginPref;

    @BindView(R.id.btn_programados)
    ProgressButton btnProgramados;
    @BindView(R.id.btn_todos)
    ProgressButton btnTodos;
    @BindView(R.id.ll_leyenda)
    LinearLayout ll_leyenda;
    @BindView(R.id.txt_nombre_top)
    TextView txt_nombre_top;
    @Destroy(type = Type.PROGRESS_BAR)
    @BindView(R.id.pb_maps)
    ProgressBar pb_maps;
    @BindView(R.id.ll_colaboradores)
    LinearLayout ll_colaboradores;

    private DialogFragment dialogLeyenda;

    private List<Cliente> clients;
    private ClienteResponse clienteResponse;
    private String selectBoton = "";

    @Override
    public MapaPresenter createPresenter() {
        return new MapaPresenter(getContext());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initGoogleMap();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mapa_cliente, container, false);
        ButterKnife.bind(this, view);
        init(savedInstanceState);
        return view;
    }

    private void init(Bundle savedInstanceState) {

        loginPref = new LoginPref(getContext());

        initDialog(savedInstanceState);
        disableButton();
        setListeners();
        setData();
    }

    private void initDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null) {

            dialogLeyenda = (DialogLeyenda) getChildFragmentManager().findFragmentByTag(DialogLeyenda.class.getCanonicalName());

            if (dialogLeyenda == null)
                dialogLeyenda = new DialogLeyenda();

        } else {
            dialogLeyenda = new DialogLeyenda();
        }
    }

    private void setData() {
        ll_leyenda.setVisibility(View.VISIBLE);
        if (loginPref.getIdProfile().equals(Constants.PROFILE_SUPERVISOR) ||
                loginPref.getIdProfile().equals(Constants.PROFILE_ADMINISTRADOR)) {
            ll_colaboradores.setVisibility(View.VISIBLE);
            txt_nombre_top.setText("Mapa");
        }
    }

    private void setListeners() {

        btnTodos.setOnClickListener(this);
        btnProgramados.setOnClickListener(this);
        ll_leyenda.setOnClickListener(this);


    }

    private void initGoogleMap() {

        new Thread(() -> {
            try {
                supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_ubicacion_cliente);
                if (supportMapFragment == null) {
                    supportMapFragment = SupportMapFragment.newInstance();
                    getChildFragmentManager().beginTransaction().replace(R.id.map_ubicacion_cliente, supportMapFragment).commit();
                }
                getActivity().runOnUiThread(() -> supportMapFragment.getMapAsync(this));
            } catch (Exception ignored) {
                Log.w("d", ignored);
            }
        }).start();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setCompassEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        try {
            //https://developers.google.com/maps/documentation/android-sdk/styling

            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.mapstyle));

            if (!success) {
                Log.e(LOG_TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(LOG_TAG, "Can't find style. Error: ", e);
        }


        mMap.setOnMapLoadedCallback(() -> {
            mMap.getUiSettings().setRotateGesturesEnabled(false);
            ConsultClients();
        });

        if (getContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && getContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

    }

    private void ConsultClients() {
        JSONGenerator.getNewInstance(getActivity())
                .put("id_perfil", new LoginPref(getContext()).getIdProfile())
                .put("id_usuario", new LoginPref(getContext()).getUid())
                .put("mapa_val", "")
                .put("lat", new AppPref(getContext()).getLatitude())
                .put("lon", new AppPref(getContext()).getLongitude())
                .operate(jsonObject -> {
                    getPresenter().doClientsListMap(jsonObject);
                });
    }

    private void mostrarLima() {
        try {
            /*CUANDO EL MAPA ESTE LISTO ENTONCES ENFOCARSE EN EL MAPA DE PERU*/
            LatLngBounds.Builder bound = new LatLngBounds.Builder();
            bound.include(new LatLng(-12.043979, -77.045429))
                    .include(new LatLng(-12.043937, -77.0401080))
                    .include(new LatLng(-12.049110, -77.040172))
                    .include(new LatLng(-12.049173, -77.045107));

            LatLngBounds limites = bound.build();
            this.mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(limites, 500, 500, 0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() == null)
            return;
        switch (v.getId()) {
            case R.id.btn_todos:

                if (clients == null || clients.size() == 0) {
                    return;
                }
                selectBoton = "";

                marcarTodos();

                break;

            case R.id.btn_programados:
                selectBoton = "1";

                if (clients == null || clients.size() == 0) {
                    return;
                }

                marcarTodos();

                break;
            case R.id.ll_leyenda:

                if (dialogLeyenda.isResumed())
                    return;

                dialogLeyenda.showNow(getChildFragmentManager(), DialogLeyenda.class.getSimpleName());

                break;
        }
    }

    private void enableButton() {
        btnProgramados.setEnabled(true);
        btnTodos.setEnabled(true);
    }

    private void disableButton() {

        btnProgramados.setEnabled(false);
        btnTodos.setEnabled(false);


    }

    @Override
    public void showLoadingDialog(boolean status) {
        if (status) {
            Destroyer.subscribe(this);
            pb_maps.setVisibility(View.VISIBLE);
        } else {
            Destroyer.unsubscribe(this);
            pb_maps.setVisibility(View.GONE);
        }
    }

    @Override
    public void onListarClientesMapaSuccess(ClienteResponse response) {
        if (getActivity() == null)
            return;

        enableButton();
        clienteResponse = response;
        clients = clienteResponse.getListC();

        if (response.getListC() != null && response.getListC().size() > 0) {

            marcarTodos();

        } else {

            mostrarLima();

        }

    }

    private void marcarTodos() {

        mMap.clear();

        int count = 0;

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (Cliente client : clienteResponse.getListC()) {

            if (!client.getLatitud().isEmpty() && !client.getLongitud().isEmpty()) {
                MarkerOptions Marker1;
                mMap.setInfoWindowAdapter(new MarkerAdapter(getContext(), mMap, cli -> {
                    Toast.makeText(getContext(), "Nombre: " + cli.getNombre(), Toast.LENGTH_SHORT).show();
                }));

                if (selectBoton.isEmpty()) {
                    count++;

                    LatLng latLng = new LatLng(
                            Double.parseDouble(client.getLatitud()),
                            Double.parseDouble(client.getLongitud()));

                    builder.include(latLng);

                    Marker1 = new MarkerOptions().position(latLng).title(client.getNombre());
                    asignarColorMarkr(Marker1, client);
                } else {
                    if (client.getProg() == Integer.parseInt(selectBoton)) {
                        count++;

                        LatLng latLng = new LatLng(
                                Double.parseDouble(client.getLatitud()),
                                Double.parseDouble(client.getLongitud()));
                        builder.include(latLng);

                        Marker1 = new MarkerOptions().position(latLng).title(client.getNombre());
                        asignarColorMarkr(Marker1, client);
                    }
                }

            }
        }

        if (count > 0)
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(),
                    (int) com.developer.appsupport.utils.Constants.convertDpiToPx(getActivity(), 50)));
        else
            mostrarLima();
    }

    private void asignarColorMarkr(MarkerOptions Marker1, Cliente client) {

        if (client.getColorVisita().equals(String.valueOf(Constants.VISIT_DEFAULT))) {
//                MarkerOptions Marker1 = new MarkerOptions().position(latLng).title(client.getNombre());
            Marker1.icon(Constants.bitmapImagenVector(getContext(), R.drawable.ic_location_gris));
            //mMap.addMarker(Marker1);
            Marker marker = mMap.addMarker(Marker1);
            marker.setTag(client);
//            marker.showInfoWindow();
        }

        if (client.getProg() == 1) {
            if (client.getColorVisita().equals(String.valueOf(Constants.VISIT_COMPLETED))) {

                Marker1.icon(Constants.bitmapImagenVector(getContext(), R.drawable.ic_location_green));
                //mMap.addMarker(Marker1);
                Marker marker = mMap.addMarker(Marker1);
                marker.setTag(client);
//                marker.showInfoWindow();

            } else if (client.getColorVisita().equals(String.valueOf(Constants.VISIT_SIN_VENTA))) {

                Marker1.icon(Constants.bitmapImagenVector(getContext(), R.drawable.ic_location_red));
                //mMap.addMarker(Marker1);
                Marker marker = mMap.addMarker(Marker1);
                marker.setTag(client);
//                marker.showInfoWindow();

            }
        } else {
            if (client.getColorVisita().equals(String.valueOf(Constants.VISIT_COMPLETED))) {

                Marker1.icon(Constants.bitmapImagenVector(getContext(), R.drawable.ic_location_azul));
                //mMap.addMarker(Marker1);
                Marker marker = mMap.addMarker(Marker1);
                marker.setTag(client);
//                marker.showInfoWindow();

            } else if (client.getColorVisita().equals(String.valueOf(Constants.VISIT_SIN_VENTA))) {

                Marker1.icon(Constants.bitmapImagenVector(getContext(), R.drawable.ic_location_orange));
                //mMap.addMarker(Marker1);
                Marker marker = mMap.addMarker(Marker1);
                marker.setTag(client);
//                marker.showInfoWindow();

            }
        }
    }

    @Override
    public void onListarClientesMapaFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    private Bitmap getMarkerBitmapFromView() {

        View customMarkerView = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_custom, null);
//        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
//        markerImageView.setImageResource(resId);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

}
