package com.jdl.distribution.ui.fragment.editar_cliente.model;

import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface CrudClienteService {
    @POST(Constants.WS_ACTUALIZAR_CLIENTE)
    Call<StatusMsgResponse> doActualizarCliente(@Body String data);

}
