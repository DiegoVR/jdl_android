package com.jdl.distribution.ui.fragment.mapa.dialog;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.jdl.distribution.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogLeyenda extends BaseDialogFragment {

    @BindView(R.id.img_close)
    ImageCardView img_close;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_leyenda, container, false);
        setCancelable(false);

        ButterKnife.bind(this,view);

        img_close.setOnClickListener(v -> dismiss());

        return view;
    }

}
