package com.jdl.distribution.ui.fragment.productos.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.ui.fragment.productos.model.ProductoResponse;

public interface ProductosView extends MvpView {
    void showLoadingDialog(boolean status);
    void onListarProductoSuccess(ProductoResponse response);
    void onListarProductoFail(String mensaje);
}
