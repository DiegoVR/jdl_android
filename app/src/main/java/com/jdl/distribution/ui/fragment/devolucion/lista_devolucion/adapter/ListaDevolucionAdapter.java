package com.jdl.distribution.ui.fragment.devolucion.lista_devolucion.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.utils.Constants;
import com.developer.appsupport.utils.MyOutlineProvider;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.devolucion.lista_devolucion.model.ListaDevolucionResponse;
import com.jdl.distribution.utils.adapter.AdapterBinder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListaDevolucionAdapter extends RecyclerView.Adapter<ListaDevolucionAdapter.ViewHolder> {

    Context context;
    ArrayList<ListaDevolucionResponse.Documento> listadoDevolucion;
    DevolucionListener listener;

    public ListaDevolucionAdapter(Context context, ArrayList<ListaDevolucionResponse.Documento> listadoDevolucion,DevolucionListener listener) {
        this.context = context;
        this.listadoDevolucion = listadoDevolucion;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pedido_devolucion,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ListaDevolucionResponse.Documento model = listadoDevolucion.get(i);
        viewHolder.bindTo(model);
    }

    @Override
    public int getItemCount() {
        return listadoDevolucion.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterBinder<ListaDevolucionResponse.Documento>, View.OnClickListener {

        @BindView(R.id.card_devolucion)
        CardView card_devolucion;
        @BindView(R.id.txt_nombre_doc_item)
        TextView txt_nombre_doc_item;
        @BindView(R.id.txt_fecha)
        TextView txt_fecha;
        @BindView(R.id.txt_total)
        TextView txt_total;
        @BindView(R.id.img_more)
        ImageCardView img_more;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                card_devolucion.setOutlineProvider(new MyOutlineProvider((int) Constants.convertDpiToPx(context, 10), 0.3f));
            }

            img_more.setOnClickListener(this);
        }

        @Override
        public void bindTo(@Nullable ListaDevolucionResponse.Documento documento) {
            if (documento!=null){
                txt_total.setText(documento.getTotal());
                txt_nombre_doc_item.setText(documento.getNombre());
                txt_fecha.setText(documento.getFecha());
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.img_more:
                    PopupMenu popup = new PopupMenu(context, img_more);
                    //inflating menu from xml resource
//                    popup.inflate(R.menu.menu_devolucion);
                    popup.getMenuInflater().inflate(R.menu.menu_devolucion, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.btn_dev_total:
                                    if (getAdapterPosition()!=-1){
                                        listener.onItemClickListenerTotal(listadoDevolucion.get(getAdapterPosition()).getIdDocumento());
                                    }
                                    return true;
                                case R.id.btn_dev_parcial:
                                    if (getAdapterPosition()!=-1){
                                        listener.onItemClickListenerParcial(listadoDevolucion.get(getAdapterPosition()));
                                    }
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();

                    break;
            }
        }
    }

    public interface DevolucionListener {
        void onItemClickListenerParcial(ListaDevolucionResponse.Documento documento);
        void onItemClickListenerTotal(String codigo);
    }

}
