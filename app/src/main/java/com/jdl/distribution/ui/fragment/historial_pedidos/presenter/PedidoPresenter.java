package com.jdl.distribution.ui.fragment.historial_pedidos.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.ui.fragment.historial_pedidos.model.PedidoHistorialResponse;
import com.jdl.distribution.ui.fragment.historial_pedidos.model.PedidoService;
import com.jdl.distribution.ui.fragment.historial_pedidos.view.PedidosView;
import com.jdl.distribution.ui.fragment.productos_frecuentes.model.FrecuentesService;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PedidoPresenter extends MvpBasePresenter<PedidosView> {
    private static final String LOG_TAG = PedidoPresenter.class.getSimpleName();
    private final Context context;

    public PedidoPresenter(Context context) {
        this.context = context;
    }

    public void doPedidoRequest(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        ifViewAttached(view -> view.showLoadingDialog(true));


        PedidoService service = RetrofitClient.getRetrofitInstance().create(PedidoService.class);
        Call<PedidoHistorialResponse> call = service.doPedido(jsonObject.toString());


        call.enqueue(new Callback<PedidoHistorialResponse>() {
            @Override
            public void onResponse(Call<PedidoHistorialResponse> call, Response<PedidoHistorialResponse> response) {
                PedidoHistorialResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> view.showLoadingDialog(false));
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onPedidosFail("clientListResponse==null");
                        } else {
                            view.onPedidosFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onPedidosSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onPedidosFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<PedidoHistorialResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.showLoadingDialog(false);
                        view.onPedidosFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.showLoadingDialog(false);
                        view.onPedidosFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });
    }

    public void doEmitirComprobantePreventaRegister(JSONObject jsonObject){

        Log.d(LOG_TAG," enviando: "+jsonObject.toString());

        FrecuentesService service = RetrofitClient.getRetrofitInstance().create(FrecuentesService.class);
        Call<EmitirComprobanteResponse> call = service.emitirComprobante(jsonObject.toString());
        call.enqueue(new Callback<EmitirComprobanteResponse>() {
            @Override
            public void onResponse(Call<EmitirComprobanteResponse> call, Response<EmitirComprobanteResponse> response) {
                EmitirComprobanteResponse emitirComprobanteResponse = response.body();

                if (emitirComprobanteResponse ==null){
                    Log.d(LOG_TAG,"doEmitirComprobantePreventaRegister==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onEmitirComprobanteSFail("doEmitirComprobantePreventaRegister==null");
                        } else {
                            view.onEmitirComprobanteSFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG,emitirComprobanteResponse.toString());

                switch (emitirComprobanteResponse.getStatusEnum()){

                    case SUCCESS:

                        ifViewAttached(view -> view.onEmitirComprobanteSuccess(emitirComprobanteResponse));

                        break;

                    case FAIL:

                        ifViewAttached(view -> view.onEmitirComprobanteSFail(emitirComprobanteResponse.getMsg()));

                        break;

                }
            }

            @Override
            public void onFailure(Call<EmitirComprobanteResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: "+t==null?"":t.toString());
                ifViewAttached(view -> {

                    if (Constants.DEBUG) {
                        view.onEmitirComprobanteSFail("onFailure: "+t==null?"":t.toString());
                    } else {
                        view.onEmitirComprobanteSFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });
    }
}