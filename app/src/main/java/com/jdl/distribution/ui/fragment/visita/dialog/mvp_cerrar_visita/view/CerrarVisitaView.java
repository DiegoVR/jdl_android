package com.jdl.distribution.ui.fragment.visita.dialog.mvp_cerrar_visita.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.mvp.StatusMsgResponse;

public interface CerrarVisitaView extends MvpView {
    void onCloseVisitSuccess(StatusMsgResponse response);
    void onCloseVisitFail(String message);
}
