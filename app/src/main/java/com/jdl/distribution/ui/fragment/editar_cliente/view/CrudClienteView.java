package com.jdl.distribution.ui.fragment.editar_cliente.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.mvp.StatusMsgResponse;

public interface CrudClienteView extends MvpView {
    void onActualizarSuccess(StatusMsgResponse response);
    void onActualizarFail(String mensaje);
}
