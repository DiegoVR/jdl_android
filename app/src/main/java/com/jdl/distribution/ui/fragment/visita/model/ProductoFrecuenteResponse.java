package com.jdl.distribution.ui.fragment.visita.model;

import android.os.Parcelable;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;
import com.jdl.distribution.mvp.model.Moneda;
import com.jdl.distribution.ui.fragment.programados.model.InitVisitResponse;

import org.parceler.Parcel;

import java.util.List;

public class ProductoFrecuenteResponse implements IStringNotNull<ProductoFrecuenteResponse>, StatusConverter {

    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("moneda")
    private Moneda moneda;
    @Expose
    @SerializedName("listP")
    private List<Product> listP;
    @Expose
    @SerializedName("status")
    private int status;

    public String getMsg() {
        return msg;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public List<Product> getListP() {
        return listP;
    }

    public int getStatus() {
        return status;
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Product implements IStringNotNull<Product> {

        @StringNotNull
        @Expose
        @SerializedName("idPro")
        private String idPro;
        @StringNotNull
        @Expose
        @SerializedName("cod")
        private String codigo;
        @StringNotNull
        @Expose
        @SerializedName("nom")
        private String nombre;
        @StringNotNull
        @Expose
        @SerializedName("stock")
        private String stock;
        @StringNotNull
        @Expose
        @SerializedName("sigla")
        private String sigla;
        @StringNotNull
        @Expose
        @SerializedName("cost")
        private double costoSinDescuentos;
        @StringNotNull
        @Expose                 //
        @SerializedName("cant") // EDITAR
        private String cant;   //

        private int cantidad;

        private boolean hasFocus=false;

        @StringNotNull
        private String valorDescuento;

        @StringNotNull
        private String total = "0";

        @StringNotNull
        private String valorDescuentoFormateado;

        @StringNotNull
        private String tipoDescuento;

        public boolean isHasFocus() {
            return hasFocus;
        }

        public String getIdPro() {
            return idPro;
        }

        public void setIdPro(String idPro) {
            this.idPro = idPro;
        }

        public void setHasFocus(boolean hasFocus) {
            this.hasFocus = hasFocus;
        }

        @Override
        public Product getWithStringNotNull() {
            return getWithStringNotNull(this);
        }

        public String getCodigo() {
            return codigo;
        }

        public void setCodigo(String codigo) {
            this.codigo = codigo;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getSigla() {
            return sigla;
        }

        public void setSigla(String sigla) {
            this.sigla = sigla;
        }

        public double getCostoSinDescuentos() {
            return costoSinDescuentos;
        }

        public void setCostoSinDescuentos(double costoSinDescuentos) {
            this.costoSinDescuentos = costoSinDescuentos;
        }

        public String getCant() {
            return cant;
        }

        public void setCant(String cantidad) {
            this.cant = cantidad;
        }

        public int getCantidad() {
            return cantidad;
        }

        public void setCantidad(int cantidadView) {
            this.cantidad = cantidadView;
        }

        public String getValorDescuento() {
            return valorDescuento;
        }

        public void setValorDescuento(String valorDescuento) {
            this.valorDescuento = valorDescuento;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getValorDescuentoFormateado() {
            return valorDescuentoFormateado;
        }

        public void setValorDescuentoFormateado(String valorDescuentoFormateado) {
            this.valorDescuentoFormateado = valorDescuentoFormateado;
        }

        public String getTipoDescuento() {
            return tipoDescuento;
        }

        public void setTipoDescuento(String tipoDescuento) {
            this.tipoDescuento = tipoDescuento;
        }

        @Override
        public String toString() {
            return "Product{" +
                    "codigo='" + codigo + '\'' +
                    ", nombre='" + nombre + '\'' +
                    ", stock='" + stock + '\'' +
                    ", sigla='" + sigla + '\'' +
                    ", costoSinDescuentos=" + costoSinDescuentos +
                    ", cant='" + cant + '\'' +
                    ", cantidad=" + cantidad +
                    ", valorDescuento='" + valorDescuento + '\'' +
                    ", total='" + total + '\'' +
                    ", valorDescuentoFormateado='" + valorDescuentoFormateado + '\'' +
                    ", tipoDescuento='" + tipoDescuento + '\'' +
                    '}';
        }
    }

    @Override
    public ProductoFrecuenteResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    @Override
    public String toString() {
        return "ProductoFrecuenteResponse{" +
                "msg='" + msg + '\'' +
                ", moneda=" + moneda +
                ", listP=" + listP +
                ", status=" + status +
                '}';
    }
}

