package com.jdl.distribution.ui.fragment.historial_pedidos;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Destroyer;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.mvp.model.Message;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.developer.appsupport.utils.JSONGenerator;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.fragment.base.BaseMvpFragment;
import com.jdl.distribution.ui.fragment.emitir_comprobante.EmitirComprobanteFragment;
import com.jdl.distribution.ui.fragment.emitir_comprobante.EmitirComprobanteFragmentBuilder;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.ui.fragment.historial_pedidos.adapter.PedidoHistorialAdapter;
import com.jdl.distribution.ui.fragment.historial_pedidos.model.PedidoHistorialResponse;
import com.jdl.distribution.ui.fragment.historial_pedidos.presenter.PedidoPresenter;
import com.jdl.distribution.ui.fragment.historial_pedidos.view.PedidosView;
import com.jdl.distribution.utils.Constants;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistorialPedidoFragment extends BaseMvpFragment<PedidosView, PedidoPresenter>
        implements SwipeRefreshLayout.OnRefreshListener, PedidosView {

    @BindView(R.id.rv_pedido_historial)
    RecyclerView rv_pedido_historial;
    @Destroy(type = Type.SWIPE_REFRESH)
    @BindView(R.id.sw_pedido_historial)
    public SwipeRefreshLayout sw_pedido_historial;
    @BindView(R.id.txt_cantidad)
    TextView txt_cantidad;
    @BindView(R.id.edt_search_pedido)
    EditText edt_search_pedido;

    private PedidoHistorialAdapter pedidoHistorialAdapter;
    private ArrayList<PedidoHistorialResponse.DetallePedido> listPedido = new ArrayList<>();

    private String fecha = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_historial_pedido, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        search();
        setAdapters();
        setListeners();
    }

    private void setListeners() {
        sw_pedido_historial.setOnRefreshListener(this);
    }

    private void setAdapters() {

        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        rv_pedido_historial.setLayoutManager(layoutManager);
        rv_pedido_historial.setItemAnimator(new DefaultItemAnimator());

        pedidoHistorialAdapter = new PedidoHistorialAdapter(getContext(), listPedido, new PedidoHistorialAdapter.PedidoHistorialListener() {
            @Override
            public void onItemClickListener(PedidoHistorialResponse.DetallePedido detallePedido) {

                JSONGenerator.getNewInstance(getActivity())
                        .requireInternet(true)
                        .put("id_ped", detallePedido.getIdPed())
                        .put("id_pf", new LoginPref(getContext()).getIdProfile())
                        .put("id_fv", new LoginPref(getContext()).getUid())
                        .operate(getPresenter()::doEmitirComprobantePreventaRegister);

            }

            @Override
            public void onItemClickListenerEditar(PedidoHistorialResponse.DetallePedido detallePedido) {

            }

            @Override
            public void onItemClickListenerImprimir(PedidoHistorialResponse.DetallePedido detallePedido) {

            }
        });

        rv_pedido_historial.setAdapter(pedidoHistorialAdapter);
        txt_cantidad.setText(String.valueOf(listPedido.size()));
    }

    private void search() {
        edt_search_pedido.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filtrar(s.toString());
            }


        });
    }

    private void filtrar(String text) {

        ArrayList<PedidoHistorialResponse.DetallePedido> listFiltroPedido = new ArrayList<>();

        for (PedidoHistorialResponse.DetallePedido local : listPedido) {
            if (local.getNom().toLowerCase().contains(text.toLowerCase()) ||
                    local.getRs().toLowerCase().contains(text.toLowerCase()) ||
                    local.getRuc().toLowerCase().contains(text.toLowerCase())) {
                listFiltroPedido.add(local);
            }
        }

        pedidoHistorialAdapter.filterList(listFiltroPedido);

    }

    @Override
    public void onRefresh() {
        if (Constants.verifyInternetAndMqtt(getContext())) {
            sw_pedido_historial.setRefreshing(false);
            return;
        }
        loadData(fecha);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMesage(Message message) {
        if (message.getOpStr().equals(HistorialPedidoFragment.class.getSimpleName())) {
            loadData(message.getMessage());
            fecha = message.getMessage();
        }

    }

    private void loadData(String fecha) {
        if (getActivity() == null)
            return;

        JSONGenerator.getNewInstance(getActivity())
                .requireInternet(true)
                .put("id_pf", new LoginPref(getContext()).getIdProfile())
                .put("id_fv", new LoginPref(getContext()).getUid())
                .put("fecha", fecha)
                .operate(new JSONGenerator.OnOperateListener() {
                    @Override
                    public void onOperateError() {
//                            showContent();
                    }

                    @Override
                    public void onOperateSuccess(JSONObject jsonObject) {
                        getPresenter().doPedidoRequest(jsonObject);
                    }
                });

    }

    @Override
    public PedidoPresenter createPresenter() {
        return new PedidoPresenter(getContext());
    }

    @Override
    public void showLoadingDialog(boolean status) {
        if (status) {
            try {
                Destroyer.subscribe(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            sw_pedido_historial.setRefreshing(true);
        } else {
            try {
                Destroyer.unsubscribe(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            sw_pedido_historial.setRefreshing(false);
        }
    }

    @Override
    public void onPedidosSuccess(PedidoHistorialResponse response) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
        if (response.getListP() != null) {
            listPedido.clear();
            listPedido.addAll(response.getListP());
            pedidoHistorialAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPedidosFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEmitirComprobanteSuccess(EmitirComprobanteResponse response) {
        if (getActivity() == null)
            return;

        EmitirComprobanteFragment.OPEN = true;

        Fragment signupFragment = new EmitirComprobanteFragmentBuilder(response.getData()).build();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(EmitirComprobanteFragment.class.getSimpleName());
        transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
        transaction.replace(R.id.main_content, signupFragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onEmitirComprobanteSFail(String mensaje) {
        if (getActivity() == null)
            return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }
}
