package com.jdl.distribution.ui.dialog_fragment.dialog_fragment.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import java.util.List;

public class BuscarProductoResponse implements IStringNotNull<BuscarProductoResponse>, StatusConverter {
    @StringNotNull
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("listP")
    private List<Product> listP;
    @Expose
    @SerializedName("status")
    private int status;

    public String getMsg() {
        return msg;
    }

    public List<Product> getListP() {
        return listP;
    }

    public int getStatus() {
        return status;
    }

    public static class Product implements IStringNotNull<Product> {
        @Expose
        @SerializedName("stock")
        private String stock;
        //        @Expose
//        @SerializedName("costD")
//        private double costoConDescuento;
        @Expose
        @SerializedName("cost")
        private double costoSinDescuentos;
        @StringNotNull
        @Expose
        @SerializedName("sigla")
        private String simboloMonedad;
        @StringNotNull
        @Expose
        @SerializedName("nom")
        private String nombre;
        @StringNotNull
        @Expose
        @SerializedName("cod")
        private String codigo;
        @StringNotNull
        @Expose
        @SerializedName("idPro")
        private String idPro;

        private boolean checked = false;

        public String getStock() {
            return stock;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public double getCostoSinDescuentos() {
            return costoSinDescuentos;
        }

        public String getSimboloMonedad() {
            return simboloMonedad;
        }

        public String getNombre() {
            return nombre;
        }

        public String getCodigo() {
            return codigo;
        }

        public String getIdPro() {
            return idPro;
        }

        public boolean isChecked() {
            return checked;
        }

        @Override
        public String toString() {
            return "Product{" +
                    "stock='" + stock + '\'' +
                    ", costoSinDescuentos=" + costoSinDescuentos +
                    ", simboloMonedad='" + simboloMonedad + '\'' +
                    ", nombre='" + nombre + '\'' +
                    ", codigo='" + codigo + '\'' +
                    ", idPro='" + idPro + '\'' +
                    ", checked=" + checked +
                    '}';
        }

        @Override
        public Product getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Override
    public BuscarProductoResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }
}
