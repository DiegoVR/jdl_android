package com.jdl.distribution.ui.fragment.historial_despacho.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.ui.fragment.historial_despacho.model.EntregaHistorialService;
import com.jdl.distribution.ui.fragment.historial_despacho.model.HistorialDespachoResponse;
import com.jdl.distribution.ui.fragment.historial_despacho.view.EntregaHistorialView;
import com.jdl.distribution.ui.fragment.productos_frecuentes.model.FrecuentesService;
import com.jdl.distribution.ui.fragment.reparto_entrega.view.EntregaView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EntregaHistorialPresenter extends MvpBasePresenter<EntregaHistorialView> {
    private static final String LOG_TAG = EntregaHistorialPresenter.class.getSimpleName();
    private final Context context;

    public EntregaHistorialPresenter(Context context) {
        this.context = context;
    }

    public void doPedidoRequest(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        ifViewAttached(view -> view.showLoadingDialog(true));


        EntregaHistorialService service = RetrofitClient.getRetrofitInstance().create(EntregaHistorialService.class);
        Call<HistorialDespachoResponse> call = service.doEntrega(jsonObject.toString());


        call.enqueue(new Callback<HistorialDespachoResponse>() {
            @Override
            public void onResponse(Call<HistorialDespachoResponse> call, Response<HistorialDespachoResponse> response) {
                HistorialDespachoResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> view.showLoadingDialog(false));
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onEntregaFail("clientListResponse==null");
                        } else {
                            view.onEntregaFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onEntregaSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onEntregaFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<HistorialDespachoResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.showLoadingDialog(false);
                        view.onEntregaFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.showLoadingDialog(false);
                        view.onEntregaFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });
    }

    public void doEmitirComprobanteEntregaRegister(JSONObject jsonObject){

        Log.d(LOG_TAG," enviando: "+jsonObject.toString());

        FrecuentesService service = RetrofitClient.getRetrofitInstance().create(FrecuentesService.class);
        Call<EmitirComprobanteResponse> call = service.emitirComprobante(jsonObject.toString());
        call.enqueue(new Callback<EmitirComprobanteResponse>() {
            @Override
            public void onResponse(Call<EmitirComprobanteResponse> call, Response<EmitirComprobanteResponse> response) {
                EmitirComprobanteResponse emitirComprobanteResponse = response.body();

                if (emitirComprobanteResponse ==null){
                    Log.d(LOG_TAG,"doEmitirComprobantePreventaRegister==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onEmitirComprobanteSFail("doEmitirComprobantePreventaRegister==null");
                        } else {
                            view.onEmitirComprobanteSFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG,emitirComprobanteResponse.toString());

                switch (emitirComprobanteResponse.getStatusEnum()){

                    case SUCCESS:

                        ifViewAttached(view -> view.onEmitirComprobanteSuccess(emitirComprobanteResponse));

                        break;

                    case FAIL:

                        ifViewAttached(view -> view.onEmitirComprobanteSFail(emitirComprobanteResponse.getMsg()));

                        break;

                }
            }

            @Override
            public void onFailure(Call<EmitirComprobanteResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: "+t==null?"":t.toString());
                ifViewAttached(view -> {

                    if (Constants.DEBUG) {
                        view.onEmitirComprobanteSFail("onFailure: "+t==null?"":t.toString());
                    } else {
                        view.onEmitirComprobanteSFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });
    }
}
