package com.jdl.distribution.ui.fragment.vendedor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.vendedor.model.VendedorResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VendedorAdapter extends RecyclerView.Adapter<VendedorAdapter.ViewHolder> {

    Context context;
    ArrayList<VendedorResponse.Vendedor> listVendedor;

    public VendedorAdapter(Context context, ArrayList<VendedorResponse.Vendedor> listVendedor) {
        this.context = context;
        this.listVendedor = listVendedor;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vendedor,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VendedorResponse.Vendedor vendedor = listVendedor.get(position);

        holder.txt_nombre.setText(vendedor.getNombre());
        holder.txt_celular.setText(vendedor.getCelular());
        holder.txt_correo.setText(vendedor.getCorreo());
        holder.txt_perfil.setText(vendedor.getPerfil());
        holder.txt_almacen.setText(vendedor.getAlmacen());
    }

    @Override
    public int getItemCount() {
        return listVendedor.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_nombre)
        TextView txt_nombre;
        @BindView(R.id.txt_celular)
        TextView txt_celular;
        @BindView(R.id.txt_correo)
        TextView txt_correo;
        @BindView(R.id.txt_perfil)
        TextView txt_perfil;
        @BindView(R.id.txt_almacen)
        TextView txt_almacen;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
