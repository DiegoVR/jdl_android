package com.jdl.distribution.ui.fragment.visita;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.appsupport.annotation.onetime.OneTime;
import com.developer.appsupport.annotation.onetime.OneTimer;
import com.developer.appsupport.annotation.stringnotnull.StringNotNullWorker;
import com.developer.appsupport.mvp.model.Message;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.developer.appsupport.utils.JSONGenerator;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.fragmentargs.bundler.ParcelerArgsBundler;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.AppPref;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.mvp.model.Moneda;
import com.jdl.distribution.ui.fragment.base.BaseMvpFragment;
import com.jdl.distribution.ui.fragment.clientes.model.ComboFrecuenciaResponse;
import com.jdl.distribution.ui.fragment.clientes.model.SeleccioneClienteResponse;
import com.jdl.distribution.ui.fragment.devolucion.lista_devolucion.ListaDevolucionFragment;
import com.jdl.distribution.ui.fragment.devolucion.lista_devolucion.ListaDevolucionFragmentBuilder;
import com.jdl.distribution.ui.fragment.editar_cliente.EditarClienteFragment;
import com.jdl.distribution.ui.fragment.editar_cliente.EditarClienteFragmentBuilder;
import com.jdl.distribution.ui.fragment.incidencias.IncidenciasFragment;
import com.jdl.distribution.ui.fragment.incidencias.IncidenciasFragmentBuilder;
import com.jdl.distribution.ui.fragment.productos_frecuentes.ProductosFrecuentesFragment;
import com.jdl.distribution.ui.fragment.productos_frecuentes.ProductosFrecuentesFragmentBuilder;
import com.jdl.distribution.ui.fragment.programados.model.InitVisitResponse;
import com.jdl.distribution.ui.fragment.reparto_entrega.EntregaRepartoFragment;
import com.jdl.distribution.ui.fragment.reparto_entrega.EntregaRepartoFragmentBuilder;
import com.jdl.distribution.ui.fragment.visita.dialog.DialogCerrarVisita;
import com.jdl.distribution.ui.fragment.visita.dialog.dialog_factura.DialogFactura;
import com.jdl.distribution.ui.fragment.visita.model.EntregaTotalResponse;
import com.jdl.distribution.ui.fragment.visita.model.EntregaValidarResponse;
import com.jdl.distribution.ui.fragment.visita.model.EventoResponse;
import com.jdl.distribution.ui.fragment.visita.model.ProductoFrecuenteResponse;
import com.jdl.distribution.ui.fragment.visita.presenter.MenuVisitaPresenter;
import com.jdl.distribution.ui.fragment.visita.view.MenuVisitaView;
import com.jdl.distribution.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@FragmentWithArgs
public class VisitaFragment extends BaseMvpFragment<MenuVisitaView, MenuVisitaPresenter> implements View.OnClickListener, View.OnTouchListener, MenuVisitaView {

    public static boolean OPEN = true;
    public static boolean OPEN_EDIT = false;

    public static String CLICK_BUTTON_VISITA = "";

    @Arg(bundler = ParcelerArgsBundler.class)
    Cliente cliente;
    @Arg(bundler = ParcelerArgsBundler.class)
    Moneda moneda;
    @Arg(bundler = ParcelerArgsBundler.class)
    InitVisitResponse visitResponse;

    private DialogFragment dialogCerrarVisita;
    private DialogFragment dialogFactura;

    @OneTime
    @BindView(R.id.btn_cerrar_vista)
    ProgressButton btn_cerrar_vista;
    @BindView(R.id.cv_waze)
    CardView cv_waze;
    @BindView(R.id.cv_maps)
    CardView cv_maps;

    @BindView(R.id.txt_nombre_local)
    TextView txtNombreLocal;
    @BindView(R.id.txt_nombre_cliente)
    TextView txtNombreCliente;
    @BindView(R.id.txt_distancia_local)
    TextView txtDistanciaLocal;
    @BindView(R.id.txt_direccion_local)
    TextView txtDireccionLocal;
    @BindView(R.id.txt_rs)
    TextView txt_rs;
    @BindView(R.id.ll_frecuencia)
    LinearLayout ll_frecuencia;
    @BindView(R.id.txt_frecuencia)
    TextView txt_frecuencia;
    @BindView(R.id.txt_horario_local)
    TextView txt_horario_local;

    //PREVENTA
    @BindView(R.id.ll_preventa)
    LinearLayout ll_preventa;
    @OneTime
    @BindView(R.id.btn_pedido_preventa)
    ProgressButton btn_pedido_preventa;
    @OneTime
    @BindView(R.id.btn_editar_preventa)
    ProgressButton btn_editar_preventa;
    @OneTime
    @BindView(R.id.btn_incidencias_preventa)
    ProgressButton btn_incidencias_preventa;
    @OneTime
    @BindView(R.id.btn_inventario_preventa)
    ProgressButton btn_inventario_preventa;

    //PREVENTA

    //REPARTO
    @BindView(R.id.ll_reparto)
    LinearLayout ll_reparto;
    @OneTime
    @BindView(R.id.btn_devolucion_reparto)
    ProgressButton btn_devolucion_reparto;
    @OneTime
    @BindView(R.id.btn_incidencias_reparto)
    ProgressButton btn_incidencias_reparto;
    @OneTime
    @BindView(R.id.btn_inventario_reparto)
    ProgressButton btn_inventario_reparto;
    @OneTime
    @BindView(R.id.btn_entrega_reparto)
    ProgressButton btn_entrega_reparto;
    //REPARTO

    private LoginPref loginPref;
    private AppPref appPref;
    private SeleccioneClienteResponse selClienteResponse;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visita, container, false);
        ButterKnife.bind(this, view);
        FragmentArgs.inject(this);

        init(savedInstanceState);

        return view;
    }

    private void init(Bundle savedInstanceState) {

        initDialogs(savedInstanceState);

        loginPref = new LoginPref(getContext());
        appPref = new AppPref(getContext());

        setListener();
        configUi();
        setData();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void configUi() {
        if (getActivity() == null)
            return;

        switch (loginPref.getIdProfile()) {
            case Constants.PROFILE_PRE_VENTA:
                ll_preventa.setVisibility(View.VISIBLE);
                break;
            case Constants.PROFILE_AUTO_VENTA:
//                ll_autoventa.setVisibility(View.VISIBLE);
                break;
            case Constants.PROFILE_REPARTO:
                ll_reparto.setVisibility(View.VISIBLE);
                break;

            case Constants.PROFILE_SUPERVISOR:
//                ll_supervisor.setVisibility(View.VISIBLE);
                break;
            case Constants.PROFILE_ADMINISTRADOR:
//                ll_administrador.setVisibility(View.VISIBLE);
                break;

        }
    }

    private void initDialogs(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            dialogCerrarVisita = (DialogCerrarVisita) getChildFragmentManager().findFragmentByTag(DialogCerrarVisita.class.getSimpleName());
            dialogFactura = (DialogFactura) getChildFragmentManager().findFragmentByTag(DialogFactura.class.getSimpleName());

            if (dialogCerrarVisita == null)
                dialogCerrarVisita = new DialogCerrarVisita();

            if (dialogFactura == null)
                dialogFactura = new DialogFactura();

        } else {
            dialogCerrarVisita = new DialogCerrarVisita();
            dialogFactura = new DialogFactura();
        }
    }

    private void setListener() {

        btn_cerrar_vista.setOnClickListener(this);
        cv_waze.setOnClickListener(this);
        cv_maps.setOnClickListener(this);

        btn_pedido_preventa.setOnClickListener(this);
        btn_editar_preventa.setOnClickListener(this);
        btn_incidencias_preventa.setOnClickListener(this);
        btn_inventario_preventa.setOnClickListener(this);

        btn_devolucion_reparto.setOnClickListener(this);
        btn_inventario_reparto.setOnClickListener(this);
        btn_incidencias_reparto.setOnClickListener(this);
        btn_entrega_reparto.setOnClickListener(this);

        btn_pedido_preventa.setOneTimerSupport(true);
        btn_editar_preventa.setOneTimerSupport(true);
        btn_incidencias_preventa.setOneTimerSupport(true);
        btn_entrega_reparto.setOneTimerSupport(true);

        btn_cerrar_vista.setOnTouchListener(this);
        cv_waze.setOnTouchListener(this);
        cv_maps.setOnTouchListener(this);

        btn_pedido_preventa.setOnTouchListener(this);
        btn_editar_preventa.setOnTouchListener(this);
        btn_incidencias_preventa.setOnTouchListener(this);
        btn_inventario_preventa.setOnTouchListener(this);

        btn_devolucion_reparto.setOnTouchListener(this);
        btn_inventario_reparto.setOnTouchListener(this);
        btn_incidencias_reparto.setOnTouchListener(this);
        btn_entrega_reparto.setOnTouchListener(this);

        ((DialogFactura) dialogFactura).onSetListenerSuccess((response) -> {
            Fragment signupFragment4 = new EntregaRepartoFragmentBuilder(cliente, null, response).build();
            FragmentTransaction transaction4 = getFragmentManager().beginTransaction();
            transaction4.addToBackStack(null);
            transaction4.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
            transaction4.add(R.id.main_content, signupFragment4, EntregaRepartoFragment.class.getSimpleName());
            transaction4.commit();
        });

    }

    private void setData() {

        if (cliente == null)
            return;

        txtNombreLocal.setText(cliente.getNombre());
        txtNombreCliente.setText(cliente.getContacto());
        txtDistanciaLocal.setText(cliente.getDistancia());
        txtDireccionLocal.setText(cliente.getDireccion());
        txt_rs.setText(cliente.getRazonSocial());

        txt_horario_local.setText(
                cliente.getHoraInicio().isEmpty() ? "" : cliente.getHoraInicio().concat(":00")
                        .concat(cliente.getHoraInicio().isEmpty() || cliente.getHoraFin().isEmpty() ? "" : "-")
                        .concat(cliente.getHoraFin().isEmpty() ? "" : cliente.getHoraFin().concat(":00")));

//        txt_frecuencia.setText(cliente.getFrecuencia().isEmpty() ? "" : cliente.getFrecuencia()+"("+cliente.getDias()+")");

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (v.getId()) {
            case R.id.cv_maps:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(cv_maps);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(cv_maps);
                        break;
                }
                break;
            case R.id.cv_waze:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(cv_waze);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(cv_waze);
                        break;
                }
                break;
            case R.id.btn_pedido_preventa:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(btn_pedido_preventa);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(btn_pedido_preventa);
                        break;
                }
                break;
            case R.id.btn_editar_preventa:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(btn_editar_preventa);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(btn_editar_preventa);
                        break;
                }
                break;
            case R.id.btn_incidencias_preventa:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(btn_incidencias_preventa);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(btn_incidencias_preventa);
                        break;
                }
                break;
            case R.id.btn_inventario_preventa:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(btn_inventario_preventa);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(btn_inventario_preventa);
                        break;
                }
                break;
            case R.id.btn_devolucion_reparto:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(btn_devolucion_reparto);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(btn_devolucion_reparto);
                        break;
                }
                break;
            case R.id.btn_inventario_reparto:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(btn_inventario_reparto);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(btn_inventario_reparto);
                        break;
                }
                break;
            case R.id.btn_incidencias_reparto:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(btn_incidencias_reparto);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(btn_incidencias_reparto);
                        break;
                }
                break;
            case R.id.btn_entrega_reparto:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(btn_entrega_reparto);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(btn_entrega_reparto);
                        break;
                }
                break;
            case R.id.btn_cerrar_vista:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(btn_cerrar_vista);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(btn_cerrar_vista);
                        break;
                }
                break;
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        if (getActivity() == null)
            return;
        switch (v.getId()) {
            case R.id.cv_waze:
                com.developer.appsupport.utils.Constants.openWaze(getContext(), cliente.getLatitud(), cliente.getLongitud());
                break;
            case R.id.cv_maps:
                Log.d("VISITA", cliente.getLatitud() + " / " + cliente.getLongitud());
                com.developer.appsupport.utils.Constants.openGoogleMaps(getContext(), cliente.getLatitud(), cliente.getLongitud(), "");
                break;
            case R.id.btn_cerrar_vista:
                OneTimer.subscribe(this, btn_cerrar_vista);
                new Handler().postDelayed(() -> {

                    if (dialogCerrarVisita.isResumed())
                        return;

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(DialogCerrarVisita.CLIENTE_RESPONSE, Parcels.wrap(cliente));
                    bundle.putParcelable(DialogCerrarVisita.VISITA_RESPONSE, Parcels.wrap(visitResponse));
                    dialogCerrarVisita.setArguments(bundle);
                    dialogCerrarVisita.showNow(getChildFragmentManager(), DialogCerrarVisita.class.getSimpleName());

                    OneTimer.unsubscribe(VisitaFragment.this, btn_cerrar_vista);
                }, 100);

                break;
            case R.id.btn_pedido_preventa:
                CLICK_BUTTON_VISITA = Constants.PEDIDO;

                JSONGenerator.getNewInstance(false, getActivity())
                        .requireInternet(true)
                        .put("id_cliente", cliente.getIdcli())
                        .put("id_perfil", loginPref.getIdProfile())
                        .put("id_usuario", loginPref.getUid())
//                        .put("tipo", com.spry.sales.utils.Constants.TIPO_OPERACION_PEDIDO)
//                        .put("mon", moneda.getCodigo())
                        .operate(jsonObject -> {

                            OneTimer.subscribe(this, btn_pedido_preventa);

                            if (loginPref.getIdProfile().equals(Constants.PROFILE_PRE_VENTA)) {
                                getPresenter().doProductosFrecuentes(jsonObject);
                            }

                        });


                break;
            case R.id.btn_editar_preventa:
                JSONGenerator.getNewInstance(getActivity())
                        .requireInternet(true)
                        .put("id_perfil", loginPref.getIdProfile())
                        .put("id_usuario", loginPref.getUid())
                        .put("id_cliente", cliente.getIdcli())
                        .operate(jsonObject -> {
                            OneTimer.subscribe(this, btn_editar_preventa);
                            getPresenter().doSeleccionaCliente(jsonObject);
                        });
                break;
            case R.id.btn_incidencias_preventa:
            case R.id.btn_incidencias_reparto:
                incidencias();
                break;
            case R.id.btn_devolucion_reparto:
                devolucion();
                break;
            case R.id.btn_entrega_reparto:
                JSONGenerator.getNewInstance(false, getActivity())
                        .requireInternet(true)
                        .put("id_cliente", cliente.getIdcli())
                        .put("id_pf", new LoginPref(getContext()).getIdProfile())
                        .put("id_fv", new LoginPref(getContext()).getUid())
                        .operate(jsonObject -> {

                            OneTimer.subscribe(this, btn_entrega_reparto);

                            getPresenter().doRepartoValidarEntregaRequest(jsonObject);

                        });
                break;

        }
    }

    private void devolucion() {
        Fragment signupFragment = new ListaDevolucionFragmentBuilder(cliente, "").build();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
        transaction.add(R.id.main_content, signupFragment, ListaDevolucionFragment.class.getSimpleName());
        transaction.commit();

    }

    private void incidencias() {
        Fragment signupFragment = new IncidenciasFragmentBuilder(cliente, "", moneda).build();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
        transaction.add(R.id.main_content, signupFragment, IncidenciasFragment.class.getSimpleName());
        transaction.commit();

    }

    @Override
    public MenuVisitaPresenter createPresenter() {
        return new MenuVisitaPresenter(getActivity());
    }

    @Override
    public void onProductosFrecuentesSuccess(ProductoFrecuenteResponse response) {
        if (getActivity() == null) return;

        Fragment signupFragment = new ProductosFrecuentesFragmentBuilder(
                cliente,
                visitResponse.getIdvis(),
                moneda,
                response.getListP()).build();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(VisitaFragment.class.getSimpleName());
        transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
        transaction.add(R.id.main_content, signupFragment, ProductosFrecuentesFragment.class.getSimpleName());
        transaction.commit();
        OneTimer.unsubscribe(this, btn_pedido_preventa);
    }

    @Override
    public void onProductosFrecuentesFail(String message) {
        if (getActivity() == null) return;
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_pedido_preventa);
    }

    @Override
    public void onCloseVisitSuccess(StatusMsgResponse response) {
        if (getActivity() == null)
            return;

        VisitaFragment.OPEN = false;

//        viewModel.deleteVisit();

        Toast.makeText(getActivity(), response.getMsg(), Toast.LENGTH_SHORT).show();
        EventBus.getDefault().post(new Message<>(Constants.OP_NOTIFY_LOCALES_CERRAR_VISITA, ""));
        getActivity().onBackPressed();
        OneTimer.unsubscribe(this, btn_cerrar_vista);
    }

    @Override
    public void onCloseVisitFail(String message) {
        if (getActivity() == null)
            return;

        VisitaFragment.OPEN = false;
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_cerrar_vista);
    }

    @Override
    public void onSeleccionaClienteSuccess(SeleccioneClienteResponse response) {
        if (getActivity() == null) return;
        selClienteResponse = response;
        JSONGenerator.getNewInstance(getActivity())
                .requireInternet(true)
                .put("id_perfil", loginPref.getIdProfile())
                .put("id_usuario", loginPref.getUid())
                .operate(jsonObject -> {
                    getPresenter().doListarFrecuencia(jsonObject);
                });

    }

    @Override
    public void onSeleccionaClienteFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_editar_preventa);
    }

    @Override
    public void onListarComboFrecuenciaSuccess(ComboFrecuenciaResponse response) {
        if (getActivity() == null) return;
        Fragment signupFragment = new EditarClienteFragmentBuilder(selClienteResponse.getCliente(), response.getListFrecu(), response.getListMP(), response.getListZona()).build();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(VisitaFragment.class.getSimpleName());
        transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
        transaction.add(R.id.main_content, signupFragment, EditarClienteFragment.class.getSimpleName());
        transaction.commit();
        OneTimer.unsubscribe(this, btn_editar_preventa);
    }

    @Override
    public void onListarComboFrecuenciaFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_editar_preventa);
    }

    @Override
    public void onEntregaValidarSuccess(EntregaValidarResponse response) {
        if (getContext() == null) return;
        //Toast.makeText(getContext(), response.getMsg(), Toast.LENGTH_SHORT).show();

        JSONGenerator.getNewInstance(false, getActivity())
                .requireInternet(true)
                .put("id_cliente", cliente.getIdcli())
                .put("id_pf", new LoginPref(getContext()).getIdProfile())
                .put("id_fv", new LoginPref(getContext()).getUid())
                .put("id_asignacion", response.getListFactura().get(0).getIdAsignacion())
                .operate(jsonObject -> {

                    if (loginPref.getIdProfile().equals(Constants.PROFILE_REPARTO))
                        presenter.doEntregaTotalRequest(jsonObject);
                });


    }

    @Override
    public void onEntregaValidarFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_entrega_reparto);
    }

    @Override
    public void onEntregaTotalSuccess(EntregaTotalResponse response) {
        if (getActivity() == null) return;
        Fragment signupFragment4 = new EntregaRepartoFragmentBuilder(cliente, visitResponse.getIdvis(), response.getListP()).build();
        FragmentTransaction transaction4 = getFragmentManager().beginTransaction();
        transaction4.addToBackStack(null);
        transaction4.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
        transaction4.add(R.id.main_content, signupFragment4, EntregaRepartoFragment.class.getSimpleName());
        transaction4.commit();
        OneTimer.unsubscribe(this, btn_entrega_reparto);
    }

    @Override
    public void onEntregaTotalFail(String message) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_entrega_reparto);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message message) {

        if (message.getOpStr().equals(Constants.OP_NOTIFY_CLIENT_UPDATE_DATA)) {
            if (message.getObject() instanceof Cliente) {

                cliente.setDireccion(((Cliente) message.getObject()).getDireccion());
                cliente.setContacto(((Cliente) message.getObject()).getContacto());
                cliente.setNombre(((Cliente) message.getObject()).getNombre());
                cliente.setLatitud(((Cliente) message.getObject()).getLatitud());
                cliente.setLongitud(((Cliente) message.getObject()).getLongitud());

                cliente.setIdFrecuencia(((Cliente) message.getObject()).getIdFrecuencia());
                cliente.setListEntrega(((Cliente) message.getObject()).getListEntrega());
                cliente.setListVisita(((Cliente) message.getObject()).getListVisita());
//                cliente = ((Cliente) message.getObject());

                setData();
            }
        }
    }

}
