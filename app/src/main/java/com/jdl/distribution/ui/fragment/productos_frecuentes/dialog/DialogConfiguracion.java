package com.jdl.distribution.ui.fragment.productos_frecuentes.dialog;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.jdl.distribution.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogConfiguracion extends BaseDialogFragment implements View.OnClickListener {

    @BindView(R.id.btn_cancelar)
    Button btn_cancelar;
    @BindView(R.id.btn_aceptar)
    Button btn_aceptar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_configuracion, container, false);
        setCancelable(false);
        ButterKnife.bind(this,view);
        init();
        return view;
    }

    private void init(){
        setListener();
    }

    private void setListener() {
        btn_aceptar.setOnClickListener(this);
        btn_cancelar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_aceptar:
                dismiss();
                break;
            case R.id.btn_cancelar:
                dismiss();
                break;
        }
    }
}
