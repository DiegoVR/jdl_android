package com.jdl.distribution.ui.fragment.productos_frecuentes;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.appsupport.annotation.onetime.OneTime;
import com.developer.appsupport.annotation.onetime.OneTimer;
import com.developer.appsupport.mvp.model.Message;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.developer.appsupport.utils.JSONGenerator;
import com.google.android.material.internal.NavigationMenu;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.fragmentargs.bundler.ParcelerArgsBundler;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.AppPref;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.fragmentargs.ParcelerArrayListBundler;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.mvp.model.Moneda;
import com.jdl.distribution.ui.activity.barcode.SimpleScannerActivity;
import com.jdl.distribution.ui.adapter.productos_frecuentes.ProductoFrecuentePreVentaAdapter;
import com.jdl.distribution.ui.dialog_fragment.dialog_fragment.DialogBuscar;
import com.jdl.distribution.ui.dialog_fragment.dialog_fragment.model.BuscarProductoResponse;
import com.jdl.distribution.ui.fragment.base.BaseMvpFragment;
import com.jdl.distribution.ui.fragment.emitir_comprobante.EmitirComprobanteFragment;
import com.jdl.distribution.ui.fragment.emitir_comprobante.EmitirComprobanteFragmentBuilder;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.ui.fragment.productos_frecuentes.dialog.DialogCalendario;
import com.jdl.distribution.ui.fragment.productos_frecuentes.dialog.DialogConfiguracion;
import com.jdl.distribution.ui.fragment.productos_frecuentes.dialog.DialogDescuentoPrecio;
import com.jdl.distribution.ui.fragment.productos_frecuentes.model.RegistrarPedidoResponse;
import com.jdl.distribution.ui.fragment.productos_frecuentes.presenter.FrecuentesPresenter;
import com.jdl.distribution.ui.fragment.productos_frecuentes.view.FrecuentesView;
import com.jdl.distribution.ui.fragment.programados.model.InitVisitResponse;
import com.jdl.distribution.ui.fragment.visita.VisitaFragment;
import com.jdl.distribution.ui.fragment.visita.model.ProductoFrecuenteResponse;
import com.jdl.distribution.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

@FragmentWithArgs
public class ProductosFrecuentesFragment extends BaseMvpFragment<FrecuentesView, FrecuentesPresenter>
        implements DialogBuscar.OnSelectItemListener, View.OnTouchListener, View.OnClickListener, FrecuentesView {

    private LoginPref loginPref;
    private AppPref appPref;
    private RecyclerView.Adapter adapter;
    private GridLayoutManager layoutManager;

    private DialogBuscar dialogBuscar;
    private DialogConfiguracion dialogConfiguracion;
    private DialogDescuentoPrecio dialogDescuentoPrecio;
    private DialogCalendario dialogCalendario;

    String codmoneda = "", comprobante = "", ruc = "", descuento = "0.0", ordencompra = "", observaciones = "", tipoDoc = "", dirCli = "", nomCli = "", tipoDescuento, tipoPago = "", diasCredito = "", medioPago = "", tipoVenta = "";

    @BindView(R.id.btn_menu)
    FabSpeedDial btn_menu;
    @BindView(R.id.rv_detaller_pedido)
    RecyclerView rv_detaller_pedido;
    @BindView(R.id.txt_total_servicio)
    TextView txt_total_servicio;
    @OneTime
    @BindView(R.id.btn_generar_pedido)
    ProgressButton btn_generar_pedido;
    @BindView(R.id.txt_fecha_entrega)
    TextView txt_fecha_entrega;
    @BindView(R.id.icv_fecha)
    ImageCardView icv_fecha;
    @BindView(R.id.txt_nombre_local)
    TextView txt_nombre_local;
    @BindView(R.id.txt_sigla)
    TextView txt_sigla;
    @BindView(R.id.img_back)
    ImageCardView img_back;

    @Arg(bundler = ParcelerArrayListBundler.class)
    List<ProductoFrecuenteResponse.Product> products;
    @Arg(bundler = ParcelerArgsBundler.class)
    Cliente cliente;
    @Arg(bundler = ParcelerArgsBundler.class)
    Moneda moneda;
    @Arg
    String idVisit;

    private String fechaSeleccionada = "";
    List<ProductoFrecuenteResponse.Product> productsSeleccionados;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_productos_frecuentes, container, false);
        FragmentArgs.inject(this);
        ButterKnife.bind(this, view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        init(savedInstanceState);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void init(Bundle savedInstanceState) {
        loginPref = new LoginPref(getContext());
        appPref = new AppPref(getContext());

        initDialog(savedInstanceState);
        setArcMenu();
        setAdapter();
        setListener();
        setData();
    }

    private void setData() {
        txt_nombre_local.setText(cliente.getNombre());
        txt_sigla.setText("S/ ");
    }

    private void setListener() {
        dialogBuscar.setOnSelectItemListener(this);
        btn_generar_pedido.setOnClickListener(this);
        icv_fecha.setOnClickListener(this);
        img_back.setOnClickListener(this);

        btn_generar_pedido.setOneTimerSupport(true);

        btn_generar_pedido.setOnTouchListener(this);

        dialogCalendario.setOnSuccessListenerFecha((fecha, fechaShort) -> {
            fechaSeleccionada = fecha;
            txt_fecha_entrega.setText(fechaShort);
        });

    }

    private void initDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            dialogBuscar = (DialogBuscar) getChildFragmentManager().findFragmentByTag(DialogBuscar.class.getSimpleName());
            dialogConfiguracion = (DialogConfiguracion) getChildFragmentManager().findFragmentByTag(DialogConfiguracion.class.getSimpleName());
            dialogDescuentoPrecio = (DialogDescuentoPrecio) getChildFragmentManager().findFragmentByTag(DialogDescuentoPrecio.class.getSimpleName());
            dialogCalendario = (DialogCalendario) getChildFragmentManager().findFragmentByTag(DialogCalendario.class.getSimpleName());

            if (dialogBuscar == null)
                dialogBuscar = new DialogBuscar();
            if (dialogConfiguracion == null)
                dialogConfiguracion = new DialogConfiguracion();
            if (dialogDescuentoPrecio == null)
                dialogDescuentoPrecio = new DialogDescuentoPrecio();
            if (dialogCalendario == null)
                dialogCalendario = new DialogCalendario();

        } else {
            dialogBuscar = new DialogBuscar();
            dialogConfiguracion = new DialogConfiguracion();
            dialogDescuentoPrecio = new DialogDescuentoPrecio();
            dialogCalendario = new DialogCalendario();
        }
    }

    private void setAdapter() {

        layoutManager = new GridLayoutManager(getActivity(), 1);
        rv_detaller_pedido.setLayoutManager(layoutManager);
        rv_detaller_pedido.setItemAnimator(new DefaultItemAnimator());

        if (loginPref.getIdProfile().equals(Constants.PROFILE_PRE_VENTA) && (VisitaFragment.CLICK_BUTTON_VISITA.equals(Constants.PEDIDO) || cliente == null)) {
            adapter = new ProductoFrecuentePreVentaAdapter(getActivity(), products, new ProductoFrecuentePreVentaAdapter.Listener() {
                @Override
                public void onEyeClickListener(int i) {

                    if (i < 0 || products.size() == 0 || getActivity() == null || dialogDescuentoPrecio.isResumed())
                        return;

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(DialogDescuentoPrecio.PRODUCTO_RESPONSE, Parcels.wrap(products.get(i)));
                    bundle.putParcelable(DialogDescuentoPrecio.CLIENTE_RESPONSE, Parcels.wrap(cliente));
//                    bundle.putInt(DialogProductDiscount.KEY_POSITION, i);
//                    if (emitirComprobanteSeleccionadoResponse != null) {
//                        bundle.putString("CLIENTE", emitirComprobanteSeleccionadoResponse.getPedido().getIdcli());
//                        bundle.putString("CANTIDAD", emitirComprobanteSeleccionadoResponse.getPedido().getDetP().get(i).getCant());
//                        bundle.putString("DSCT", emitirComprobanteSeleccionadoResponse.getPedido().getDetP().get(i).getDesct());
//                        bundle.putString("MONEDA", emitirComprobanteSeleccionadoResponse.getPedido().getMon());
//                        bundle.putString("DSCV", emitirComprobanteSeleccionadoResponse.getPedido().getDetP().get(i).getDescv());
//                    } else if (cliente != null) {
//                        bundle.putString("CLIENTE", cliente.getIdcli());
//                        bundle.putString("MONEDA", moneda.getCodigo());
//                        bundle.putString("CANTIDAD", String.valueOf(products.get(i).getCantidad()).isEmpty() ? "0" : String.valueOf(products.get(i).getCantidad()));
//                    }
//
                    dialogDescuentoPrecio.setArguments(bundle);
                    dialogDescuentoPrecio.showNow(getChildFragmentManager(), DialogDescuentoPrecio.class.getSimpleName());

                }

                @Override
                public void onTextChangeListener(int i, EditText editText) {

                    double TotalPreventa = 0.0;
                    if (tipoDescuento != null) {
                        if (tipoDescuento.equals(Constants.DISCOUNT_NETO)) {
                            if (Double.parseDouble(descuento == null ? "0.0" : descuento) > 0) {
                                Toast.makeText(getContext(), "Configure nuevamente el descuento global", Toast.LENGTH_SHORT).show();
                                descuento = null;
                            }
                        }
                    }

                    TotalPreventa = CalcularTotal(Double.parseDouble(descuento == null ? "0.0" : descuento));
                    TotalPreventa = (TotalPreventa > 0) ? Constants.toDecimal(TotalPreventa, 2) : TotalPreventa;
                    txt_total_servicio.setText(String.valueOf(TotalPreventa));
                }
            });
        }
        rv_detaller_pedido.setAdapter(adapter);

    }

    private double CalcularTotal(double descuento) {

        double totalDescuento = 0.0;
        double totalSinDescuento = 0.0;
        double TotalPreventa = 0.0;

//        if(i > -1)
//        cantidad = String.valueOf(products.get(i).getCantidad());

        for (ProductoFrecuenteResponse.Product product : products) {
            totalSinDescuento += product.getCostoSinDescuentos() * product.getCantidad();

            if (product.getTipoDescuento() != null) {
                if (product.getTipoDescuento().equals(Constants.DISCOUNT_NETO)) {
                    totalDescuento += Double.parseDouble(product.getValorDescuento());
                } else if (product.getTipoDescuento().equals(Constants.DISCOUNT_PORCENTAJE)) {
                    totalDescuento += (product.getCostoSinDescuentos() * (Double.parseDouble(product.getValorDescuento())) / 100) * product.getCantidad();
                }
            }
        }

        if (tipoDescuento != null) {
            if (tipoDescuento.equals(Constants.DISCOUNT_PORCENTAJE)) {
                TotalPreventa = (totalSinDescuento - totalDescuento) * (100 - descuento) / 100;
            } else if (tipoDescuento.equals(Constants.DISCOUNT_NETO)) {
                TotalPreventa = totalSinDescuento - totalDescuento - descuento;
            }
        } else {
            TotalPreventa = totalSinDescuento - totalDescuento;
        }

        return TotalPreventa;

    }

    private void setArcMenu() {
        btn_menu.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                return super.onPrepareMenu(navigationMenu);
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.id_config:
                        dialogConfiguracion.showNow(getChildFragmentManager(), DialogConfiguracion.class.getSimpleName());
//                        loadDialogCombo();

                        break;
                    case R.id.id_scan:

                        checkPermission();

                        startActivityForResult(new Intent(getActivity(), SimpleScannerActivity.class), Constants.REQUEST_CODE_SCAN);
                        break;
                    case R.id.id_search:
//                        FrequentProductsFragment.this.onClick(btn_buscar);
                        Bundle bundle = new Bundle();
                        if (cliente == null) {
//                            bundle.putString("CLIENTE", emitirComprobanteSeleccionadoResponse.getPedido().getIdcli());
                        } else {
                            bundle.putParcelable(DialogBuscar.CLIENTE, Parcels.wrap(cliente));
                        }
                        dialogBuscar.setArguments(bundle);
                        dialogBuscar.showNow(getChildFragmentManager(), DialogBuscar.class.getSimpleName());

                        break;
                }
                return super.onMenuItemSelected(menuItem);

            }
        });
    }

    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Constants.permissionsCamera[0])) {

                ActivityCompat.requestPermissions(getActivity(), Constants.permissionsCamera, Constants.REQUEST_CODE_PERMISION_CAMERA);

            } else {

                ActivityCompat.requestPermissions(getActivity(), Constants.permissionsCamera, Constants.REQUEST_CODE_PERMISION_CAMERA);

            }

            return;
        }
    }

    @Override
    public void onSelectItem(List<ProductoFrecuenteResponse.Product> product) {
        posibleAddProductList(product);
    }

    private void posibleAddProductList(List<ProductoFrecuenteResponse.Product> listProduct) {

        if (listProduct.size() == 0) {
            Toast.makeText(getActivity(), "No ah seleccionado un item", Toast.LENGTH_SHORT).show();
            return;
        }

        boolean alreadyInList = false;

        for (int i = 0; i < products.size(); i++) {
            for (int j = 0; j < listProduct.size(); j++) {
                if (products.get(i).getIdPro().equals(listProduct.get(j).getIdPro())) {
                    listProduct.remove(j);
                }
            }
        }

        if (listProduct.size() < 1) {
            alreadyInList = true;
        }

        if (!alreadyInList) {
            products.addAll(listProduct);
//            pedidoAdapter.notifyItemInserted(0);
            adapter.notifyDataSetChanged();
            Toast.makeText(getActivity(), "Agregado(s)", Toast.LENGTH_SHORT).show();
            rv_detaller_pedido.postDelayed(() -> layoutManager.scrollToPosition(0), 100);
            dialogBuscar.dismiss();
        } else {
            Toast.makeText(getActivity(), "Los productos seleccionados ya se encuentran en la lista", Toast.LENGTH_SHORT).show();
        }

    }

    private void posibleAddProduct(ProductoFrecuenteResponse.Product product) {

        boolean alreadyInList = false;

        for (ProductoFrecuenteResponse.Product item : products) {

            if (item.getIdPro().equals(product.getIdPro()))
                alreadyInList = true;

        }

        if (!alreadyInList) {
            products.add(0, product);
            adapter.notifyItemInserted(0);
            Toast.makeText(getActivity(), "Agregado", Toast.LENGTH_SHORT).show();
            rv_detaller_pedido.postDelayed(() -> layoutManager.scrollToPosition(0), 100);

        } else {
            Toast.makeText(getActivity(), "El producto ya se encuentra en la lista", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (v.getId()) {
            case R.id.btn_generar_pedido:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Constants.ActionDown(btn_generar_pedido);
                        break;
                    case MotionEvent.ACTION_UP:
                        Constants.ActionUp(btn_generar_pedido);
                        break;
                }
                break;
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_generar_pedido:

                productsSeleccionados = new ArrayList<>();

                for (ProductoFrecuenteResponse.Product product : products) {

                    if (product.getCantidad() > 0) {
                        if (product.getCantidad() == 0)
                            product.setCantidad(0);
                        productsSeleccionados.add(product);
                    }

                }

                if (productsSeleccionados.size() == 0) {
                    Toast.makeText(getActivity(), "No hay productos seleccionados", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (loginPref.getIdProfile().equals(Constants.PROFILE_PRE_VENTA)) {

                    List<DetalleProducto> detalleProductos = new ArrayList<>();
                    for (ProductoFrecuenteResponse.Product product : productsSeleccionados) {
                        detalleProductos.add(new DetalleProducto(product.getCantidad(), product.getIdPro(), String.valueOf(product.getCostoSinDescuentos()), product.getNombre()));
                    }

                    JSONGenerator.getNewInstance(false, getActivity())
                            .requireInternet(true)
                            .put("pe_cli_id", cliente.getIdcli())
                            .put("pe_fv_id", loginPref.getUid())
                            .put("pe_latitud", appPref.getLatitude())
                            .put("pe_longitud", appPref.getLongitude())
                            .put("pe_obser", observaciones)
                            .put("pe_vis_id", idVisit)
                            .put("pe_fecha_prog", fechaSeleccionada)
                            .put("pe_moneda", moneda.getId())
                            .put("pe_impor_total", txt_total_servicio.getText().toString())

                            .put("pe_tip_doc", ruc.isEmpty() ? cliente.getTipoDocumento() : tipoDoc)
                            .put("pe_cli_num_doc", ruc.isEmpty() ? cliente.getRuc() : ruc)
                            .put("pe_cli_nom", ruc.isEmpty() ? cliente.getRazonSocial() : nomCli)
                            .put("pe_cli_direc", ruc.isEmpty() ? cliente.getDireccion() : dirCli)

                            .put("pe_tip_compro", comprobante.isEmpty() ? cliente.getTipoComprobante() : comprobante)
                            .put("pe_est", 0)
                            .put("pe_tip_pago", tipoPago.isEmpty() ? cliente.getTipoPago() : tipoPago)
                            .put("pe_tiempo_credito", diasCredito.isEmpty() ? cliente.getDiasCredito() : diasCredito)
                            .put("pe_medio_pago", medioPago.isEmpty() ? cliente.getMedioPago() : medioPago)
                            .put("pedidoDetalleBean", detalleProductos)
                            .operate(jsonObject -> {

                                OneTimer.subscribe(this, btn_generar_pedido);

                                if (loginPref.getIdProfile().equals(Constants.PROFILE_PRE_VENTA)) {
                                    getPresenter().doRegistrarPedido(jsonObject);
                                }

                            });

                }

                break;
            case R.id.icv_fecha:
                dialogCalendario.showNow(getChildFragmentManager(), DialogCalendario.class.getSimpleName());
                break;
            case R.id.img_back:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public FrecuentesPresenter createPresenter() {
        return new FrecuentesPresenter(getContext());
    }

    @Override
    public void onRegistrarPedidoSuccess(RegistrarPedidoResponse response) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
        EmitirComprobanteFragment.OPEN = false;

        JSONGenerator.getNewInstance(getActivity())
                .requireInternet(true)
                .put("id_ped", response.getIdPed())
                .put("id_pf", new LoginPref(getContext()).getIdProfile())
                .put("id_fv", new LoginPref(getContext()).getUid())
                .operate(getPresenter()::doEmitirComprobantePreventaRegister);
    }

    @Override
    public void onRegistrarPedidoFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_generar_pedido);
    }

    @Override
    public void onEmitirComprobanteSuccess(EmitirComprobanteResponse response) {
        if (getActivity() == null)
            return;

        OneTimer.unsubscribe(ProductosFrecuentesFragment.this, btn_generar_pedido);

        Fragment signupFragment = new EmitirComprobanteFragmentBuilder(response.getData()).build();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(EmitirComprobanteFragment.class.getSimpleName());
        transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
        transaction.replace(R.id.main_content, signupFragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onEmitirComprobanteSFail(String mensaje) {
        if (getActivity() == null)
            return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_generar_pedido);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMensaje(Message message) {
        if (message.getOpStr().equals(Constants.OP_NOTIFY_SCANNER)) {

            if (message.getObject() instanceof BuscarProductoResponse) {
                BuscarProductoResponse.Product producto = ((BuscarProductoResponse) message.getObject()).getListP().get(0);
                ProductoFrecuenteResponse.Product product = new ProductoFrecuenteResponse.Product();

                product.setIdPro(producto.getIdPro());
                product.setCodigo(producto.getCodigo());
                product.setCostoSinDescuentos(producto.getCostoSinDescuentos());
                product.setNombre(producto.getNombre());
                product.setStock(producto.getStock());
                product.setSigla(producto.getSimboloMonedad());

                posibleAddProduct(product);
            }
        }
    }

    //    @Parcel
    public static class DetalleProducto {

        private int pre_pro_cantidad = 0;
        private String ped_pro_id = "";
        private String pre_pro_cost_uni = "";
        private String ped_pro_nombre = "";

        public DetalleProducto(int pre_pro_cantidad, String ped_pro_id, String pre_pro_cost_uni, String ped_pro_nombre) {
            this.pre_pro_cantidad = pre_pro_cantidad;
            this.ped_pro_id = ped_pro_id;
            this.pre_pro_cost_uni = pre_pro_cost_uni;
            this.ped_pro_nombre = ped_pro_nombre;
        }
    }

}
