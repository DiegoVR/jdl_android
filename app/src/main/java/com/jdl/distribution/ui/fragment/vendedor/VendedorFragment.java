package com.jdl.distribution.ui.fragment.vendedor;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.annotation.onetime.OneTime;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.vendedor.adapter.VendedorAdapter;
import com.jdl.distribution.ui.fragment.vendedor.model.VendedorResponse;
import com.jdl.distribution.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VendedorFragment extends BaseFragment implements View.OnClickListener,SwipeRefreshLayout.OnRefreshListener {

    @Destroy(type = Type.SWIPE_REFRESH)
    @BindView(R.id.sw_vendedor)
    SwipeRefreshLayout sw_vendedor;
    @BindView(R.id.rv_vendedor)
    RecyclerView rv_vendedor;
    @BindView(R.id.edt_search_vendedor)
    EditText edt_search_vendedor;
    @OneTime
    @BindView(R.id.btn_agregar_vendedores)
    ProgressButton btn_agregar_vendedores;

    private String search = "";
    private VendedorAdapter vendedorAdapter;
    private static int INIT_SECONDS = 1000;
    private CountDownTimer countDownTimer=null;

    private ArrayList<VendedorResponse.Vendedor> listVendedor = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vendedor, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        if (getActivity() == null)
            return;

        setListener();
        setAdapters();
//        loadData(search);
        searchEditText();

    }

    private void setAdapters() {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(),1);
        rv_vendedor.setLayoutManager(layoutManager);
        rv_vendedor.setItemAnimator(new DefaultItemAnimator());

        String nombre [] = {"Eduardo Huamani","Luis Bernedo","Jean Piere","Edgar Gil","Nick Pasco","Angel Chavez"};
        String celular [] = {"985652125","985474152","963203365","965210214","984512256","987410247"};
        String correo [] = {"eduardo@gmail.com","luis@gmail.com","jean@gmail.com","edgar@gmail.com","nick@gmail.com","angel@gmail.com"};
        String perfil [] = {"ADMINISTRADOR","ADMINISTRADOR","PREVENTA","REPARTO","AUTOVENTA","SUPERVISOR"};
        String almacen [] = {"Central","Central","Central","Central","Central","Central"};
        listVendedor.clear();
        for (int i = 0; i < nombre.length; i++){
            VendedorResponse.Vendedor vendedor = new VendedorResponse.Vendedor();
            vendedor.setNombre(nombre[i]);
            vendedor.setCelular(celular[i]);
            vendedor.setCorreo(correo[i]);
            vendedor.setPerfil(perfil[i]);
            vendedor.setAlmacen(almacen[i]);
            listVendedor.add(vendedor);
        }

        vendedorAdapter = new VendedorAdapter(getContext(),listVendedor);
        rv_vendedor.setAdapter(vendedorAdapter);
        vendedorAdapter.notifyDataSetChanged();
        sw_vendedor.setRefreshing(false);

    }

    private void setListener() {
        btn_agregar_vendedores.setOnClickListener(this);
        btn_agregar_vendedores.setOneTimerSupport(true);
        sw_vendedor.setOnRefreshListener(this);
    }

    private void destroyCountDownTimerTextChanged() {

        try {
            countDownTimer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            countDownTimer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        try {
//            countDownTimer = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    private void searchEditText() {
        edt_search_vendedor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(countDownTimer != null) {
                    destroyCountDownTimerTextChanged();
                }else{
                    countDownTimer = new CountDownTimer(Constants.TIME_MAX_SEARCH,INIT_SECONDS) {
                        @Override
                        public void onTick(long millisUntilFinished) {
//                            Destroyer.subscribe(ProductsFragment.this);
//                            pb_search_products.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onFinish() {
//                            Destroyer.unsubscribe(ProductsFragment.this);
//                            pb_search_products.setVisibility(View.GONE);
//                        if (TextUtils.isEmpty(s))
//                            return;

                            search = s.toString();
//                            loadData(s.toString());

                        }
                    }.start();
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_agregar_vendedores:

//                loadDataCombo();

                break;
        }
    }

    @Override
    public void onRefresh() {
        if (Constants.verifyInternetAndMqtt(getContext())){
            sw_vendedor.setRefreshing(false);
            return;
        }
        setAdapters();
//        loadData(search);
    }
}
