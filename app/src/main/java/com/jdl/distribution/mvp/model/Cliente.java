package com.jdl.distribution.mvp.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcel;

import java.util.List;

@Parcel(Parcel.Serialization.BEAN)
@Entity
public class Cliente implements IStringNotNull<Cliente> {

    @PrimaryKey
    @NotNull
    @StringNotNull
    @Expose
    @SerializedName("idCli")
    private String idcli;

    // PROGRAMADOS Y TODOS LOS CLIENTES
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("nomLocal")
    private String nombre;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("encargado")
    private String contacto;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("dist")
    private String distancia;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("dir")
    private String direccion;

    // PRORGAMADO
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("color")
    private String colorVisita;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("progr")
    private int prog;

    //TODOS LOS CLIENTE
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("rs")
    private String razonSocial;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("ruc")
    private String ruc;

    // DATOS DE INCIO DE VISITA
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("telLocal")
    private String telefonoLocal;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("emailLocal")
    private String correoLocal;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("lat")
    private String latitud;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("lng")
    private String longitud;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("horaIni")
    private String horaInicio;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("horaFin")
    private String horaFin;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("tdoc")
    private String tipoDocumento;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("tpago")
    private String tipoPago;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("diasC")
    private String diasCredito;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("medioP")
    private String medioPago;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("telEmp")
    private String telefonoEmpresa;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("correoEmp")
    private String correoEmpresa;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("dirEmp")
    private String direccionEmpresa;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("tipCom")
    private String tipoComprobante;
    @ColumnInfo
    @StringNotNull
    @Expose
    @SerializedName("codFrecu")
    private String idFrecuencia;
    @ColumnInfo
    @StringNotNull
    @Ignore
    @Expose
    @SerializedName("entregaA")
    private List<EntregaA> listEntrega;
    @ColumnInfo
    @StringNotNull
    @Ignore
    @Expose
    @SerializedName("visitaA")
    private List<VisitaA> listVisita;

    public String getIdFrecuencia() {
        return idFrecuencia;
    }

    public void setIdFrecuencia(String idFrecuencia) {
        this.idFrecuencia = idFrecuencia;
    }

    public List<EntregaA> getListEntrega() {
        return listEntrega;
    }

    public void setListEntrega(List<EntregaA> listEntrega) {
        this.listEntrega = listEntrega;
    }

    public List<VisitaA> getListVisita() {
        return listVisita;
    }

    public void setListVisita(List<VisitaA> listVisita) {
        this.listVisita = listVisita;
    }

    @NotNull
    public String getIdcli() {
        return idcli;
    }

    public String getTipoComprobante() {
        return tipoComprobante;
    }

    public String getDiasCredito() {
        return diasCredito;
    }

    public void setDiasCredito(String diasCredito) {
        this.diasCredito = diasCredito;
    }

    public void setTipoComprobante(String tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public int getProg() {
        return prog;
    }

    public void setProg(int prog) {
        this.prog = prog;
    }

    public String getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public String getTelefonoLocal() {
        return telefonoLocal;
    }

    public void setTelefonoLocal(String telefonoLocal) {
        this.telefonoLocal = telefonoLocal;
    }

    public String getCorreoLocal() {
        return correoLocal;
    }

    public void setCorreoLocal(String correoLocal) {
        this.correoLocal = correoLocal;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getTelefonoEmpresa() {
        return telefonoEmpresa;
    }

    public void setTelefonoEmpresa(String telefonoEmpresa) {
        this.telefonoEmpresa = telefonoEmpresa;
    }

    public String getCorreoEmpresa() {
        return correoEmpresa;
    }

    public void setCorreoEmpresa(String correoEmpresa) {
        this.correoEmpresa = correoEmpresa;
    }

    public String getDireccionEmpresa() {
        return direccionEmpresa;
    }

    public void setDireccionEmpresa(String direccionEmpresa) {
        this.direccionEmpresa = direccionEmpresa;
    }

    public void setIdcli(@NotNull String idcli) {
        this.idcli = idcli;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getColorVisita() {
        return colorVisita;
    }

    public void setColorVisita(String colorVisita) {
        this.colorVisita = colorVisita;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public LatLng getLatLng(){
        try {
            return new LatLng(Double.parseDouble(latitud), Double.parseDouble(longitud));
        }catch (Exception e){
            e.printStackTrace();
            return new LatLng(-12.0465667,-77.0448037);
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class EntregaA implements IStringNotNull<EntregaA> {

        @Expose
        @SerializedName("dia")
        private String idDia;

        public String getIdDia() {
            return idDia;
        }

        public void setIdDia(@NotNull String idDia) {
            this.idDia = idDia;
        }

        @Override
        public String toString() {
            return "EntregaA{" +
                    "idDia='" + idDia + '\'' +
                    '}';
        }

        @Override
        public EntregaA getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class VisitaA implements IStringNotNull<VisitaA> {

        @Expose
        @SerializedName("dia")
        private String idDia;

        public String getIdDia() {
            return idDia;
        }

        public void setIdDia(@NotNull String idDia) {
            this.idDia = idDia;
        }

        @Override
        public String toString() {
            return "VisitaA{" +
                    "idDia='" + idDia + '\'' +
                    '}';
        }

        @Override
        public VisitaA getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }


    @Override
    public String toString() {
        return "Cliente{" +
                "idcli='" + idcli + '\'' +
                ", nombre='" + nombre + '\'' +
                ", contacto='" + contacto + '\'' +
                ", distancia='" + distancia + '\'' +
                ", direccion='" + direccion + '\'' +
                ", colorVisita='" + colorVisita + '\'' +
                ", razonSocial='" + razonSocial + '\'' +
                ", ruc='" + ruc + '\'' +
                ", telefonoLocal='" + telefonoLocal + '\'' +
                ", correoLocal='" + correoLocal + '\'' +
                ", latitud='" + latitud + '\'' +
                ", longitud='" + longitud + '\'' +
                ", horaInicio='" + horaInicio + '\'' +
                ", horaFin='" + horaFin + '\'' +
                ", tipoDocumento='" + tipoDocumento + '\'' +
                ", tipoPago='" + tipoPago + '\'' +
                ", telefonoEmpresa='" + telefonoEmpresa + '\'' +
                ", correoEmpresa='" + correoEmpresa + '\'' +
                ", direccionEmpresa='" + direccionEmpresa + '\'' +
                ", tipoComprobante='" + tipoComprobante + '\'' +
                ", diasCredito='" + diasCredito + '\'' +
                ", medioPago='" + medioPago + '\'' +
                '}';
    }

    @Override
    public Cliente getWithStringNotNull() {
        return getWithStringNotNull(this);
    }
}
