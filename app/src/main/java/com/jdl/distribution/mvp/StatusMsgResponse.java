package com.jdl.distribution.mvp;

import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusMsgResponse implements StatusConverter {
    @StringNotNull
    @Expose
    @SerializedName("msg")
    private String msg;
    @StringNotNull
    @Expose
    @SerializedName("status")
    private int status;

    public String getMsg() {
        return msg;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    @Override
    public String toString() {
        return "StatusMsgResponse{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                '}';
    }
}
