package com.jdl.distribution.mvp.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import java.util.List;

public class ClienteResponse implements IStringNotNull<ClienteResponse>, StatusConverter {

    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("listCliente")
    private List<Cliente> listC;
    @Expose
    @SerializedName("idVisita")
    private String idvis;
    @Expose
    @SerializedName("status")
    private int status;

    public List<Cliente> getListC() {
        return listC;
    }

    public void setListC(List<Cliente> listC) {
        this.listC = listC;
    }

    public String getMsg() {
        return msg;
    }

    public String getIdvis() {
        return idvis;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public ClienteResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    @Override
    public String toString() {
        return "ClienteResponse{" +
                "msg='" + msg + '\'' +
                ", listC=" + listC +
                ", idvis='" + idvis + '\'' +
                ", status=" + status +
                '}';
    }
}
