package com.developer.appsupport.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;

import com.developer.appsupport.utils.Constants;
import com.mijael.appsupport.R;

public class ProgressButton extends RelativeLayout {

    private static int DEFAULT_PROGRESS_COLOR = Color.WHITE;
    private final String LOG_TAG = ProgressButton.class.getSimpleName();
    private static int id = 1234;
    private static int idImg = 1334;
    private final int DEFAULT_TEXT_COLOR = Color.BLACK;
    private final int DEFAULT_TEXT_BACKGROUND_COLOR = Color.TRANSPARENT;
    private final int DEFAULT_TEXT_SIZE = -1;
    private final int DEFAULT_WIDTH = ViewGroup.LayoutParams.WRAP_CONTENT;
    private final int DEFAULT_HEIGHT = ViewGroup.LayoutParams.WRAP_CONTENT;
    private final int DEFAULT_IMG = R.drawable.ic_not_img;

    private boolean loading = false;
    private boolean oneTimerSupport = false;
    private int textColor = DEFAULT_TEXT_COLOR;
    private int progressColor = DEFAULT_PROGRESS_COLOR;
    private int textBackgroundColor = DEFAULT_TEXT_BACKGROUND_COLOR;
    private float textSize = DEFAULT_TEXT_SIZE;
    private float progressWidth = DEFAULT_WIDTH;
    private float progressHeight = DEFAULT_HEIGHT;
    private float imgWidth = DEFAULT_HEIGHT;
    private float imgHeight = DEFAULT_HEIGHT;
    private String text = "";
    private String font = "";
    private boolean textAllCaps = false;
    private int imgTop = -1;
    private int imgRight = -1;
    private int imgBottom = -1;
    private int imgLeft = -1;
    private float imgMargin = 0;
    private float imgPadding = 0;
    private int imgBackgroundColor = DEFAULT_TEXT_BACKGROUND_COLOR;
    private int imgTint = -1;

    //widgets
    public TextView textView;
    private ProgressBar progressBar;
    private ImageView imageView;

    public ProgressButton(Context context) {
        super(context);

        init(context);

    }

    public ProgressButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgressButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ProgressButton, defStyleAttr, 0);

        text = a.getString(R.styleable.ProgressButton_progress_text);
        textColor = a.getColor(R.styleable.ProgressButton_progress_textColor, DEFAULT_TEXT_COLOR);
        progressColor = a.getColor(R.styleable.ProgressButton_progress_textColor, DEFAULT_PROGRESS_COLOR);
        textBackgroundColor = a.getColor(R.styleable.ProgressButton_progress_textBackgroundColor, DEFAULT_TEXT_BACKGROUND_COLOR);
        progressWidth = a.getDimension(R.styleable.ProgressButton_progress_width, DEFAULT_WIDTH);
        progressHeight = a.getDimension(R.styleable.ProgressButton_progress_height, DEFAULT_HEIGHT);
        textSize = a.getDimension(R.styleable.ProgressButton_progress_textSize, DEFAULT_TEXT_SIZE);
        textAllCaps = a.getBoolean(R.styleable.ProgressButton_progress_textAllCaps, false);
        font = a.getString(R.styleable.ProgressButton_progress_fontFamily);
        imgTop = a.getResourceId(R.styleable.ProgressButton_progress_imgTop, DEFAULT_IMG);
        imgRight = a.getResourceId(R.styleable.ProgressButton_progress_imgRight, DEFAULT_IMG);
        imgBottom = a.getResourceId(R.styleable.ProgressButton_progress_imgBottom, DEFAULT_IMG);
        imgLeft = a.getResourceId(R.styleable.ProgressButton_progress_imgLeft, DEFAULT_IMG);
        imgWidth = a.getDimension(R.styleable.ProgressButton_progress_imgWidth, DEFAULT_WIDTH);
        imgHeight = a.getDimension(R.styleable.ProgressButton_progress_imgHeight, DEFAULT_HEIGHT);
        imgBackgroundColor = a.getColor(R.styleable.ProgressButton_progress_imgBackgroundColor, DEFAULT_TEXT_BACKGROUND_COLOR);
        imgMargin = a.getDimension(R.styleable.ProgressButton_progress_imgMargin, 0f);
        imgPadding = a.getDimension(R.styleable.ProgressButton_progress_imgPadding, 0f);
        imgTint = a.getColor(R.styleable.ProgressButton_progress_imgTint, -1);
        a.recycle();

        init(context);


    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (oneTimerSupport) {

            if (enabled) {
                setLoading(false);
            } else {
                setLoading(true);
            }

        }

    }



    private void init(Context context){

        //progress config
        ViewGroup.LayoutParams layoutParamsProgress = new LayoutParams((int)progressWidth, (int)progressHeight);
        ((LayoutParams) layoutParamsProgress).addRule(RelativeLayout.CENTER_IN_PARENT, TRUE);
        //ProgressBar
        progressBar = new ProgressBar(context);
        progressBar.getIndeterminateDrawable().setColorFilter(progressColor,android.graphics.PorterDuff.Mode.MULTIPLY);
        //progressBar.setId(R.id.progress_progress_button);
        progressBar.setLayoutParams(layoutParamsProgress);
        progressBar.setVisibility(GONE);


        //RelativeLayout
        LayoutParams layoutParamsRelative = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ((LayoutParams) layoutParamsRelative).addRule(RelativeLayout.CENTER_IN_PARENT, TRUE);
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(layoutParamsRelative);


        //Textview
        ViewGroup.LayoutParams layoutParamsTextView = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textView = new TextView(context);
        textView.setId(id++);
        textView.setText(text);
        textView.setTextColor(textColor);
        textView.setBackgroundColor(textBackgroundColor);
        textView.setLayoutParams(layoutParamsTextView);
        textView.setAllCaps(textAllCaps);
        if (textSize!=DEFAULT_TEXT_SIZE)
            textView.setTextSize(textSize);
        setCustomFont(context, font);


        //ImageView
        LayoutParams layoutParamsImage = new LayoutParams((int)imgWidth, (int)imgHeight);
        imageView = new ImageView(context);
        imageView.setLayoutParams(layoutParamsImage);
        imageView.setId(idImg++);
        imageView.setPadding((int)imgPadding, (int)imgPadding, (int)imgPadding, (int)imgPadding);
        imageView.setBackgroundColor(imgBackgroundColor);

        if (imgTint!=-1){
            imageView.setColorFilter(imgTint);
        }

        if (imgLeft!=DEFAULT_IMG && imgLeft!=-1){
            Log.d(LOG_TAG, "imgLeft");

            ((LayoutParams) layoutParamsTextView).leftMargin = (int)(imgWidth + imgMargin);
            ((LayoutParams) layoutParamsTextView).addRule(CENTER_VERTICAL, TRUE);
            textView.setLayoutParams(layoutParamsTextView);

            layoutParamsImage.addRule(CENTER_VERTICAL, TRUE);
            imageView.setLayoutParams(layoutParamsImage);

            imageView.setImageResource(imgLeft);
            relativeLayout.addView(imageView);
            relativeLayout.addView(textView);
            addView(relativeLayout);
        }else if (imgTop!=DEFAULT_IMG && imgTop!=-1){
            Log.d(LOG_TAG, "imgTop");

//            ((LayoutParams) layoutParamsTextView).topMargin = (int)(imgHeight + imgMargin);
            ((LayoutParams) layoutParamsTextView).addRule(CENTER_HORIZONTAL, TRUE);
            ((LayoutParams) layoutParamsTextView).addRule(BELOW, imageView.getId());
            textView.setLayoutParams(layoutParamsTextView);

            layoutParamsImage.addRule(CENTER_HORIZONTAL, TRUE);
//            ((LayoutParams) layoutParamsImage).addRule(ABOVE, textView.getId());
            imageView.setLayoutParams(layoutParamsImage);

            imageView.setImageResource(imgTop);
            relativeLayout.addView(imageView);
            relativeLayout.addView(textView);
            addView(relativeLayout);
        }else if (imgRight!=DEFAULT_IMG && imgRight!=-1){
            Log.d(LOG_TAG, "imgRight");

            //textView.setLayoutParams(layoutParamsTextView);
            ((LayoutParams) layoutParamsTextView).addRule(CENTER_VERTICAL, TRUE);
            textView.setLayoutParams(layoutParamsTextView);

            layoutParamsImage.addRule(CENTER_VERTICAL, TRUE);
            layoutParamsImage.addRule(RIGHT_OF, textView.getId());
            imageView.setLayoutParams(layoutParamsImage);

            imageView.setImageResource(imgRight);
            relativeLayout.addView(imageView);
            relativeLayout.addView(textView);
            addView(relativeLayout);
        }else if (imgBottom!=DEFAULT_IMG && imgBottom!=-1){
            Log.d(LOG_TAG, "imgBottom");

            //((LayoutParams) layoutParamsTextView).bottomMargin = (int)(imgHeight + imgMargin);
            //((LayoutParams) layoutParamsTextView).addRule(CENTER_HORIZONTAL, TRUE);
            //textView.setLayoutParams(layoutParamsTextView);
//
            ((LayoutParams) layoutParamsTextView).addRule(CENTER_HORIZONTAL, TRUE);
            //layoutParamsImage.topMargin = (int) (Constants.convertDpiToPx(context, 20) + imgMargin);
            //layoutParamsImage.addRule(ABOVE, textView.getId());
            textView.setLayoutParams(layoutParamsTextView);

            ((LayoutParams) layoutParamsImage).addRule(CENTER_HORIZONTAL, TRUE);
            ((LayoutParams) layoutParamsImage).addRule(BELOW, textView.getId());
            imageView.setLayoutParams(layoutParamsImage);


            imageView.setImageResource(imgBottom);
            relativeLayout.addView(imageView);
            relativeLayout.addView(textView);
            addView(relativeLayout);
        }else {
            Log.d(LOG_TAG, "img all default ");
            ((LayoutParams) layoutParamsTextView).addRule(RelativeLayout.CENTER_IN_PARENT, TRUE);
            textView.setLayoutParams(layoutParamsTextView);
            addView(textView);
        }

        addView(progressBar);


    }

    public boolean isLoading(){
        return loading;
    }

    public void setLoading(boolean loading){
        this.loading = loading;

        if (this.loading){
            textView.setVisibility(GONE);
            imageView.setVisibility(GONE);
            progressBar.setVisibility(VISIBLE);
        }else {
            textView.setVisibility(VISIBLE);
            imageView.setVisibility(VISIBLE);
            progressBar.setVisibility(GONE);
        }

    }

    public boolean isOneTimerSupport() {
        return oneTimerSupport;
    }

    public void setOneTimerSupport(boolean oneTimerSupport) {
        this.oneTimerSupport = oneTimerSupport;
    }

    public void setCustomFont(Context ctx, String asset) {

        if (TextUtils.isEmpty(asset))
            return;

        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), asset);
        } catch (Exception e) {
            Log.e(LOG_TAG, "Could not get typeface: "+e.getMessage());
            return;
        }

        textView.setTypeface(tf);

    }


}

