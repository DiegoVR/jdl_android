package com.developer.appsupport.ui.dialog;

import android.content.DialogInterface;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import java.util.concurrent.atomic.AtomicBoolean;

public class MyDialogFragment extends DialogFragment {
    private final AtomicBoolean isShowing = new AtomicBoolean(false);

    @Override
    public void show(FragmentManager manager, String tag) {
        isShowing.set(true); // ensure we update our "show" flag
        super.show(manager, tag);
    }

    @Override
    public void onResume() {
        super.onResume();
        isShowing.set(true);
    }
    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        isShowing.set(false);
    }
    /**
     * reader for the boolean flag, if show was already called
     */
    public boolean getIsShowing() {
        return isShowing.get();
    }
}
