package com.developer.appsupport.ui.listener;

public interface MyLifecycleObserver {

    void onCreate();

    void onPause();

    void onResume();

    void onStart();

    void onStop();

    void onDestroy();

}
