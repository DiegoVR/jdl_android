package com.developer.appsupport.utils.files;

import android.graphics.Bitmap;

import java.io.File;

public interface ImageListener {

    void onImageLoading(boolean loading, String message);

    void onImageSuccess(File file);

    void onImageViewSuccess(Bitmap bitmap);

    void onImageFail(String message);

}
