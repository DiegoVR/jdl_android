package com.developer.appsupport.mvp.model;

public class Message<T> {

    public static final int OP_DEFAULT_VALUE = -100;
    public static final String OP_STR_DEFAULT_VALUE = "";

    private int op;
    private String opStr;
    private Status status;
    private T object;
    private String message;
    private String message2;
    private String message3;

    public Message(int op, String opStr, Status status, T object) {
        this.op = OP_DEFAULT_VALUE;
        this.op = op;
        this.opStr = opStr;
        this.status = status;
        this.object = object;
    }

    public String getMessage2() {
        return message2;
    }

    public Message(int op, Status status, T object) {
        this(op, OP_STR_DEFAULT_VALUE, status, object);
    }

    public Message(String opStr, Status status, T object) {
        this(OP_DEFAULT_VALUE, opStr, status, object);
    }

    public Message(int op, T object){
        this(op, OP_STR_DEFAULT_VALUE, Status.SUCCESS, object);
    }

    public Message(String opStr, T object) {
        this.opStr = opStr;
        this.object = object;
    }

    public Message(String opStr, String message) {
        this.opStr = opStr;
        this.message = message;
    }

    public Message(String opStr, String message, String message2) {
        this.opStr = opStr;
        this.message = message;
        this.message2 = message2;
    }

    public Message(String opStr, String message, String message2,String message3) {
        this.opStr = opStr;
        this.message = message;
        this.message2 = message2;
        this.message3 = message3;
    }

    public String getMessage3() {
        return message3;
    }

    public String getMessage() {
        return message;
    }

    public int getOp() {
        return op;
    }

    public String getOpStr() {
        return opStr;
    }

    public Status getStatus() {
        return status;
    }

    public T getObject() {
        return object;
    }
}
